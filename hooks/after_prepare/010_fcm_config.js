#!/usr/bin/env node
'use strict';

var fs = require('fs');

var getValue = function(config, name) {
    var value = config.match(new RegExp('<' + name + '>(.*?)</' + name + '>', "i"));
    if(value && value[1]) {
        return value[1];
    } else {
        return null;
    }
}

function fileExists(path) {
  try  {
    return fs.statSync(path).isFile();
  }
  catch (e) {
    return false;
  }
}

function directoryExists(path) {
  try  {
    return fs.statSync(path).isDirectory();
  }
  catch (e) {
    return false;
  }
}

var config = fs.readFileSync("config.xml").toString();
var name = getValue(config, "name");

if (directoryExists("platforms/ios")) {
  /*var paths = ["GoogleService-Info.plist", "platforms/ios/www/GoogleService-Info.plist"];

  for (var i = 0; i < paths.length; i++) {
    if (fileExists(paths[i])) {
      try {
        var contents = fs.readFileSync(paths[i]).toString();
        fs.writeFileSync("platforms/ios/GoogleService-Info.plist", contents)
      } catch(err) {
        //process.stdout.write(err);
      }

      break;
    }
  }*/
    var path = require('path');
    var xcode = require('xcode');

    var rootdir = path.resolve(__dirname + "/../.."),
        projectPath = rootdir + "/platforms/ios/drift.xcodeproj/project.pbxproj",
        proj = new xcode.project(projectPath);
    proj.parse(function(err) {
        if (err) {
            console.log("ERROR: XCODE project failed to parse:");
            console.log(err);
        } else {
            var targetFile = rootdir + "/platforms/ios/GoogleService-Info.plist";
            var sourceFile = rootdir + "/GoogleService-Info.plist";
            fs.writeFileSync(targetFile, fs.readFileSync(sourceFile));
            var pbxGroupKey = proj.findPBXGroupKey({ name : "CustomTemplate" });
            proj.addResourceFile('GoogleService-Info.plist', {} , pbxGroupKey);
            fs.writeFileSync(projectPath, proj.writeSync());
            console.log("Updated XCODE project with references");
        }
    });
}

if (directoryExists("platforms/android")) {
  var paths = ["google-services.json", "platforms/android/assets/www/google-services.json"];

  for (var i = 0; i < paths.length; i++) {
    if (fileExists(paths[i])) {
      try {
        var contents = fs.readFileSync(paths[i]).toString();
        fs.writeFileSync("platforms/android/google-services.json", contents);

        var json = JSON.parse(contents);
        var strings = fs.readFileSync("platforms/android/res/values/strings.xml").toString();

        // strip non-default value
        strings = strings.replace(new RegExp('<string name="google_app_id">([^\@<]+?)</string>', "i"), '');

        // strip non-default value
        strings = strings.replace(new RegExp('<string name="google_api_key">([^\@<]+?)</string>', "i"), '');

        // strip empty lines
        strings = strings.replace(new RegExp('(\r\n|\n|\r)[ \t]*(\r\n|\n|\r)', "gm"), '$1');

        // replace the default value
        strings = strings.replace(new RegExp('<string name="google_app_id">([^<]+?)</string>', "i"), '<string name="google_app_id">' + json.client[0].client_info.mobilesdk_app_id + '</string>');

        // replace the default value
        strings = strings.replace(new RegExp('<string name="google_api_key">([^<]+?)</string>', "i"), '<string name="google_api_key">' + json.client[0].api_key[0].current_key + '</string>');

        fs.writeFileSync("platforms/android/res/values/strings.xml", strings);
      } catch(err) {
        process.stdout.write(err);
      }

      break;
    }
  }
}