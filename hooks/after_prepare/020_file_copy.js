#!/usr/bin/env node
'use strict';

var fs = require('fs');
var path = require('path');

/*************************************************
    ==========   Copy Gradle file   ==========
**************************************************/
//var rootdir = path.resolve(__dirname + "/../..");
var rootdir = "";

function directoryExists(path) {
  try  {
    return fs.statSync(path).isDirectory();
  }
  catch (e) {
    return false;
  }
}

/*************************************************
    ===   Copy Local and Push notification icon file   ===
**************************************************/

var srcPushNotificationIcon = path.join(rootdir, "src/assets/img/fcm_push_icon.png");
var pushNoticiationIconHdpi = path.join(rootdir, "platforms/android/res/drawable-port-hdpi/fcm_push_icon.png");
var pushNoticiationIconLdpi = path.join(rootdir, "platforms/android/res/drawable-port-ldpi/fcm_push_icon.png");
var pushNoticiationIconMdpi = path.join(rootdir, "platforms/android/res/drawable-port-mdpi/fcm_push_icon.png");
var pushNoticiationIconXHdpi = path.join(rootdir, "platforms/android/res/drawable-port-xhdpi/fcm_push_icon.png");
var pushNoticiationIconXXHdpi = path.join(rootdir, "platforms/android/res/drawable-port-xxhdpi/fcm_push_icon.png");
var pushNoticiationIconXXXHdpi = path.join(rootdir, "platforms/android/res/drawable-port-xxxhdpi/fcm_push_icon.png");
var srcLocalNotificationIcon = path.join(rootdir, "src/assets/img/notification.png");
var localNoticiationIconHdpi = path.join(rootdir, "platforms/android/res/drawable-port-hdpi/notification.png");
var localNoticiationIconLdpi = path.join(rootdir, "platforms/android/res/drawable-port-ldpi/notification.png");
var localNoticiationIconMdpi = path.join(rootdir, "platforms/android/res/drawable-port-mdpi/notification.png");
var localNoticiationIconXHdpi = path.join(rootdir, "platforms/android/res/drawable-port-xhdpi/notification.png");
var localNoticiationIconXXHdpi = path.join(rootdir, "platforms/android/res/drawable-port-xxhdpi/notification.png");
var localNoticiationIconXXXHdpi = path.join(rootdir, "platforms/android/res/drawable-port-xxxhdpi/notification.png");

if (directoryExists("platforms/android")) {
  setTimeout(function() {
    fs.createReadStream(srcPushNotificationIcon).pipe(fs.createWriteStream(pushNoticiationIconHdpi));
    fs.createReadStream(srcPushNotificationIcon).pipe(fs.createWriteStream(pushNoticiationIconLdpi));
    fs.createReadStream(srcPushNotificationIcon).pipe(fs.createWriteStream(pushNoticiationIconMdpi));
    fs.createReadStream(srcPushNotificationIcon).pipe(fs.createWriteStream(pushNoticiationIconXHdpi));
    fs.createReadStream(srcPushNotificationIcon).pipe(fs.createWriteStream(pushNoticiationIconXXHdpi));
    fs.createReadStream(srcPushNotificationIcon).pipe(fs.createWriteStream(pushNoticiationIconXXXHdpi));
    fs.createReadStream(srcLocalNotificationIcon).pipe(fs.createWriteStream(localNoticiationIconHdpi));
    fs.createReadStream(srcLocalNotificationIcon).pipe(fs.createWriteStream(localNoticiationIconLdpi));
    fs.createReadStream(srcLocalNotificationIcon).pipe(fs.createWriteStream(localNoticiationIconMdpi));
    fs.createReadStream(srcLocalNotificationIcon).pipe(fs.createWriteStream(localNoticiationIconXHdpi));
    fs.createReadStream(srcLocalNotificationIcon).pipe(fs.createWriteStream(localNoticiationIconXXHdpi));
    fs.createReadStream(srcLocalNotificationIcon).pipe(fs.createWriteStream(localNoticiationIconXXXHdpi));
  }, 0);
}