
export class Perks {
  static default:Array<any> = [
    {
      id: '1',
      name: 'Label.Music',
      icon: 'assets/img/perk-music.png',
      selected: false
    },
    {
      id: '2',
      name: 'Label.Food',
      icon: 'assets/img/perk-food.png',
      selected: false
    },
    {
      id: '3',
      name: 'Label.Drinks',
      icon: 'assets/img/perk-drinks.png',
      selected: false
    },
    {
      id: '4',
      name: 'Label.Conversation',
      icon: 'assets/img/perk-conversation.png',
      selected: false
    },
    {
      id: '5',
      name: 'Label.GasMoney',
      icon: 'assets/img/perk-gas.png',
      selected: false
    },
    {
      id: '6',
      name: 'Label.CoDriving',
      icon: 'assets/img/perk-driving.png',
      selected: false
    }
  ];
  /*
    get perks list
    @parameter
      mode: true-get all list, false-get only selected list
      selected: selected perks string separated ','
  */
  static getList(mode: boolean, selected?: string) {
    if(selected && selected != null){
      this.default.forEach((_item) =>{
        _item.selected = false;
      });
      var list = this.default;
      var selectedList = selected.split(',');
      selectedList.forEach((_id) => {
        list.forEach((_perk) => {
          if(_perk.id == _id){
            _perk.selected = true;
          }
        });
      });
      if(mode == false){
        list = list.filter((_item) =>{
          return _item.selected == true;
        });
        return list;
      }else{
        return list;
      }
    }else {
      if(mode == false){
        return [];
      }else{
        this.default.forEach((_item) =>{
          _item.selected = false;
        });
        return this.default;
      }
    }
  };
  /*
    get perks string
    returns selected string that separated ','
    @parameter
      perkList: perks list
  */
  static getString(perkList: Array<any>){
    var selectedList = [];
    perkList.forEach((perk) => {
      if(perk.selected == true){
        selectedList.push(perk.id);
      }
    });
    let selectedString = selectedList.join(',');
    return selectedString;
  };
}