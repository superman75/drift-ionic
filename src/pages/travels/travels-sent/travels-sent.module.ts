import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { TravelsSentPage } from './travels-sent';

@NgModule({
  declarations: [
    TravelsSentPage,
  ],
  imports: [
    IonicPageModule.forChild(TravelsSentPage),
    TranslateModule.forChild()
  ],
})
export class TravelsSentPageModule {}
