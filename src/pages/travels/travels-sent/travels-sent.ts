import { Component, trigger, state, style, transition, animate } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
import { UserService } from '../../../providers/userService';
import { UtilService } from '../../../providers/utilService';
/**
 * Generated class for the TravelsSentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-travels-sent',
  templateUrl: 'travels-sent.html',
  animations: [
    trigger('listItemState', [
      state('in',
        style({
          opacity: 1,
          height: '*',
          minHeight: '*'
        })),
      transition('* => void', [
        animate(250, style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class TravelsSentPage {
  loading: Loading;
  requests: Array<any> = [];
  noRequest: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private loadingCtrl: LoadingController,
    private translateService: TranslateService,
    private auth: AuthService,
    private userService: UserService,
    private util: UtilService) {
      
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    runs when page will enter
  */
  ionViewWillEnter() {
    this.showLoading();
    this.requests = [];
    this.noRequest = false;
    this.getRequests();
  };
  /*
    get requests data
  */
  getRequests() {
    this.auth.getSentDriftRequests().then((res: Array<any>) => {
      this.loading.dismiss();
      if(res.length == 0){
        this.noRequest = true;
      }else{
        this.noRequest = false;
      }
      res.forEach((_request)=> {
        this.userService.getUserInfo(_request.otheruser_id).then((_user: any)=> {
          if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
            _request.pictureurl = 'assets/img/malethumb.png';
          }else{
            _request.pictureurl = _user.pictureurl;
          }
          _request.nickname = _user.nickname;
          _request.transparam = {
            username: _request.nickname
          };
          this.requests.push(_request);
        });
      });
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };

  /*
    Called when user click 'drift' tab
  */
  goDrift() {
    this.navCtrl.push('TravelsDriftPage', {}, {animate: true, animation: 'wp-transition', easing: 'ease-in-out', direction: 'back'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };
  /*
    Called when user click 'deny' button
  */
  cancelRequest(index, request) {
    this.requests.splice(index, 1);
    this.util.cancelDriftRequest(request.id).then((res)=> {
      console.log(res);
    });
  };
  /*
    Called when user click 'requests' tab
  */
  goRequests() {
    this.navCtrl.push('TravelsRequestsPage', {}, {animate: true, animation: 'wp-transition', easing: 'ease-in-out', direction: 'back'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };

}
