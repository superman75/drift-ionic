import { Component, trigger, state, style, transition, animate } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { mapboxDistance, mapboxStyle, mapboxZoom, intervalTime} from '../../../app/app.config';
import { AuthService } from '../../../providers/authService';
import { UserService } from '../../../providers/userService';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
/**
 * Generated class for the TravelsDriftPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-travels-drift',
  templateUrl: 'travels-drift.html',
  animations: [
    trigger('listItemState', [
      state('in',
        style({
          opacity: 1,
          height: '*',
          minHeight: '*'
        })),
      transition('* => void', [
        animate(250, style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class TravelsDriftPage {
  loading: Loading;//loading instance
  runningDrifts: Array<any> = [];//current user's running drifts
  agreedDrifts: Array<any> = [];//current user's agreed drifts
  completedDrifts: Array<any> = [];//current user's completed drifts
  userInfo: any = null;//current user's info
  noRunning: boolean = false;//shows whether current user has running drift or not
  noAgreed: boolean = false;//shows whether current user has agreed drift or not.
  mapContainer = {
    map: null,
    currentMarker: null,
    destinationMarker: null,
    userMarkerArray:[],
    driftpointMarkerArray:[],
    idRoute: 0
  };
  updateUserInterval: any;
  updateOthersInterval: any;
  destination: any;
  mapId: number;//current page mapid
  showingAlert: boolean = false;
  constructor(
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService,
    private auth: AuthService,
    private userService: UserService,
    private mapbox: MapboxService,
    private location: LocationService,
    private alertCtrl: AlertController) {
      this.userInfo = this.auth.getUserInfo();
      this.destination = {
        text: this.userInfo.destinationcaption,
        lon: this.userInfo.destinationlon,
        lat: this.userInfo.destinationlat
      };
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('travels_drift_map');
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    show end drift alert
  */
  showEndDriftAlert() {
    let alert = this.alertCtrl.create();
    let buttonOk = this.translateService.instant('Button.Ok');
    let buttonCancel = this.translateService.instant('Button.Cancel');
    let title = this.translateService.instant('Alert.Message.WhichDrift');
    alert.setTitle(title);
    this.runningDrifts.forEach((_drift) => {
      alert.addInput({
        type: 'radio',
        label: _drift.nickname,
        value: _drift.id
      });
    });
    alert.addButton(buttonCancel);
    alert.addButton({
      text: buttonOk,
      handler: data => {
        if(data){
          this.runningDrifts.forEach((_drift) => {
            if(_drift.id == data){
              this.navCtrl.push('EndDriftPage', {drift: _drift}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
            }
          });
        }
      }
    });
    alert.present();
  };
  /*
    runs when the page loaded
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      destinationMarker: null,
      userMarkerArray:[],
      driftpointMarkerArray:[],
      idRoute: 0
    };
    this.runningDrifts = [];//current user's running drifts
    this.agreedDrifts = [];//current user's agreed drifts
    this.completedDrifts = [];//current user's completed drifts
    this.getRunningDrifts();
    this.getCompletedDrifts();
  };
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    clearInterval(this.updateUserInterval);
    clearInterval(this.updateOthersInterval);
    this.mapContainer.map.remove();
  };
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('travels_drift_map');
  };
  /*
    get running drifts data
  */
  getRunningDrifts() {
    this.noRunning = false;
    this.noAgreed = false;
    this.runningDrifts = [];
    this.agreedDrifts = [];
    this.auth.getDrifts().then((res: any) => {
      if(res.running.length == 0){
        this.noRunning = true;
      }
      if(res.agreed.length == 0){
        this.noAgreed = true;
      }
      this.showMap(false, false, false);
      res.running.forEach((_drift)=> {
        if(_drift.otheruser_id != null){
          this.userService.getUserInfo(_drift.otheruser_id).then((_user: any)=> {
            if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
              _drift.pictureurl = 'assets/img/malethumb.png';
            }else{
              _drift.pictureurl = _user.pictureurl;
            }
            _drift.nickname = _user.nickname;
            _drift.transparam = {
              username: _drift.nickname
            };
            this.runningDrifts.push(_drift);
          });
        }else{//hitchhike mode
          _drift.pictureurl = 'assets/img/malethumb.png';
          _drift.nickname = 'hitchhike mode';
          this.runningDrifts.push(_drift);
        }
      });
      res.agreed.forEach((_drift)=> {
        this.userService.getUserInfo(_drift.otheruser_id).then((_user: any)=> {
          if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
            _drift.pictureurl = 'assets/img/malethumb.png';
          }else{
            _drift.pictureurl = _user.pictureurl;
          }
          _drift.nickname = _user.nickname;
          _drift.transparam = {
            username: _drift.nickname
          };
          this.agreedDrifts.push(_drift);
        });
      });
    }, (err) => {
      console.log(err);
    });
  };
  /*
    get completed drifts data
  */
  getCompletedDrifts() {
    this.completedDrifts = [];
    this.userService.getDrifts(this.userInfo.id).then((res: Array<any>) => {
      res.forEach((_drift)=> {
        if(_drift.status == 'completed'){
          if(_drift.drifter_id == this.userInfo.id){
            _drift.other_id = _drift.driver_id;
          }else{
            _drift.other_id = _drift.drifter_id;
          }
          if(_drift.end_date != null){
            _drift.end_date = _drift.end_date.replace(/-/g, "/");
          }
          if(_drift.other_id != null){
            this.userService.getUserInfo(_drift.other_id).then((_user: any)=> {
              if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
                _drift.pictureurl = 'assets/img/malethumb.png';
              }else{
                _drift.pictureurl = _user.pictureurl;
              }
              _drift.nickname = _user.nickname;
              _drift.transparam = {
                username: _drift.nickname
              };
              this.completedDrifts.push(_drift);
            });
          }else{
            _drift.pictureurl = 'assets/img/malethumb.png';
            this.completedDrifts.push(_drift);
          }
        }
      });
    }, (err) => {
      console.log(err);
    });
  };
  
  /*
    show map in the page
  */
  showMap(_mapUpdate, _userUpdate, _otherUpdate) {
    let mapOptions = {
      container: 'travels_drift_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: mapboxZoom,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      destinationlat: this.destination.lat,
      destinationlon: this.destination.lon,
      users: null,
      driftpoints: null,
      mode: 'Driver',
      routeColor: '#8ca8a9'
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null) {
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false, false, false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showDestinationMap(this.mapContainer, false, false, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //add other users to the map
            this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers)=> {
              dataOptions.users = resusers;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {});
            });
            //add driftpoints to the map
            this.location.getDriftPoints(cpos.latitude, cpos.longitude, mapboxDistance).then((resdrifts) => {
              dataOptions.driftpoints = resdrifts;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, false, null, dataOptions).then((res) => {});
            });
            //update intervals
            this.updateUserInterval = setInterval(() => {
              this.showMap(true, true, false);
            }, 5000);
            this.updateOthersInterval = setInterval(() => {
              this.showMap(true, false, true);
            }, intervalTime);
          }, (err) => {
            this.loading.dismiss();
            console.log(err);
          });
        }else if(_userUpdate == true){
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showDestinationMap(this.mapContainer, true, true, false, null, dataOptions).then((res) => {
            console.log('update current user...');
          });
        }else if(_otherUpdate == true){
          this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers) => {
            dataOptions.users = resusers;
            this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {
              console.log('update other users...');
            });          
          }, (err) => {
            console.log(err);
          });
        }
      }
    });
  };
  /*
    Called when user click 'cancel drift' button
  */
  cancelDrift(index, drift){
    this.navCtrl.push('CancelDriftPage', {drift: drift}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
  };
  /*
    Called when user click 'navigation icon'
  */
  goNavigation() {
    if(this.userInfo.mode == 'Driver'){
      this.navCtrl.push('DriverGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
        let index = this.navCtrl.getActive().index;
        this.navCtrl.remove(index-1);
      });
    }else{
      this.navCtrl.push('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
        let index = this.navCtrl.getActive().index;
        this.navCtrl.remove(index-1);
      });
    }
  };
  /*
    Called when user click 'end drift' button
  */
  endDrift() {
    if(this.runningDrifts.length > 1){
      this.showEndDriftAlert();
    }else{
      this.navCtrl.push('EndDriftPage', {drift: this.runningDrifts[0]}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click 'requests' tab
  */
  goRequests() {
    this.navCtrl.push('TravelsRequestsPage', {}, {animate: true, animation: 'wp-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };
  /*
    Called when user click 'sent' tab
  */
  goSent() {
    this.navCtrl.push('TravelsSentPage', {}, {animate: true, animation: 'wp-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };
}
