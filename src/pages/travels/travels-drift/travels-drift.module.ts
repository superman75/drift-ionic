import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { TravelsDriftPage } from './travels-drift';

@NgModule({
  declarations: [
    TravelsDriftPage,
  ],
  imports: [
    IonicPageModule.forChild(TravelsDriftPage),
    TranslateModule.forChild()
  ],
})
export class TravelsDriftPageModule {}
