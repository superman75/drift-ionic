import { Component, trigger, state, style, transition, animate } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
import { UtilService } from '../../../providers/utilService';
import { UserService } from '../../../providers/userService';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
/**
 * Generated class for the TravelsRequestsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-travels-requests',
  templateUrl: 'travels-requests.html',
  animations: [
    trigger('listItemState', [
      state('in',
        style({
          opacity: 1,
          height: '*',
          minHeight: '*'
        })),
      transition('* => void', [
        animate(250, style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class TravelsRequestsPage {
  loading: Loading;
  requests: Array<any> = [];
  noRequest: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private auth: AuthService,
    private util: UtilService,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService,
    private userService: UserService,
    private location: LocationService,
    private mapbox: MapboxService,
    private alertCtrl: AlertController) {
      
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    show Alert
  */
  showAlert(content){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {}  
        }
      ]
    });
    alert.present();
  };
  /*
    runs when view will enter
  */
  ionViewWillEnter() {
    this.showLoading();
    this.noRequest = false;
    this.requests = [];
    this.checkGeoLocation(); 
  };
  /*
    check geo location
  */
  checkGeoLocation() {
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null) {
        let alert = this.alertCtrl.create({
          title: `<img src="assets/img/logo.png"/>`,
          subTitle: 'Failed obtaining your GPS location.',
          buttons: [
            {
              text: 'try again',
              handler: () => {
                this.checkGeoLocation();
              }
            }
          ]
        });
        alert.present();
      }else{
        this.getRequests(cpos);
      }
    });
  };
  /*
    get requests data
  */
  getRequests(cpos) {
    this.auth.getDriftRequests().then((res: Array<any>) => {
      this.loading.dismiss();
      if(res.length == 0){
        this.noRequest = true;
      }else{
        this.noRequest = false;
      }
      res.forEach((_request)=> {
        this.userService.getUserInfo(_request.otheruser_id).then((_user: any)=> {
          if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
            _request.pictureurl = 'assets/img/malethumb.png';
          }else{
            _request.pictureurl = _user.pictureurl;
          }
          _request.user = _user;
          _request.nickname = _user.nickname;
          _request.transparam = {
            username: _user.nickname
          };
          _request.distance = (this.mapbox.distanceBetween([cpos.longitude, cpos.latitude], [parseFloat(_user.lon), parseFloat(_user.lat)])).toFixed(1);
          this.requests.push(_request);
        });
      });
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click user image
  */
  goUser(request) {
    this.navCtrl.push('UserProfilePage', {userid: request.otheruser_id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'right arrow'
  */
  goUserDrift(request){
    console.log(request);
    this.navCtrl.push('UserDriftPage', {from:'travels-requests', user: request.user, request: request}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
  };
  /*
    Called when user click 'deny' button
  */
  rejectRequest(index, request) {
    this.showLoading();
    this.util.rejectDriftRequest(request.id).then((res) => {
      this.loading.dismiss();
      this.auth.getUser();
      this.requests.splice(index, 1);
      console.log(res);
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  };
  /*
    Called when user click 'arrow' button
  */
  acceptRequest(index, request) {
    this.showLoading();
    if(this.auth.getUserInfo().mode == 'Driver'){
      this.util.acceptDriftRequest(request.id).then((res) => {
        this.auth.getUser().then((res) => {
          this.loading.dismiss();
          this.requests.splice(index, 1);
          localStorage.setItem('drift_destination_user', request.otheruser_id);
          this.navCtrl.push('DriverUserDestinationPage', {userid: request.otheruser_id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
            let index = this.navCtrl.getActive().index;
            this.navCtrl.remove(index-1);
          });
        }, (err)=> {
          this.loading.dismiss();
          console.log(err);
        });
      },(err) => {
        this.loading.dismiss();
        console.log(err);
      });
    }else{
      this.auth.getDrifts().then((res: any) => {
        this.loading.dismiss();
        if(res.agreed.length > 0 || res.running.length > 0){
          if(res.agreed.length > 0){
            let alertMessage = this.translateService.instant('Alert.Message.CouldNotAcceptHaveAgreed');
            this.showAlert(alertMessage);
          }
          if(res.running.length > 0){
            let alertMessage = this.translateService.instant('Alert.Message.CouldNotAcceptHaveRunning');
            this.showAlert(alertMessage);
          }
        }else{
          this.util.acceptDriftRequest(request.id).then((res) => {
            this.auth.getUser().then((res) => {
              this.navCtrl.push('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
                let index = this.navCtrl.getActive().index;
                this.navCtrl.remove(index-1);
              });
            });
          }, (err) => {
            console.log(err);
          })
        }
      }, (err)=> {
        this.loading.dismiss();
      });
    }
  };
  /*
    Called when user click 'drift' tab
  */
  goDrift() {
    this.navCtrl.push('TravelsDriftPage', {}, {animate: true, animation: 'wp-transition', easing: 'ease-in-out', direction: 'back'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };

  /*
    Called when user click 'sent' tab
  */
  goSent() {
    this.navCtrl.push('TravelsSentPage', {}, {animate: true, animation: 'wp-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };
}
