import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DrifterHitchPage } from './drifter-hitch';

@NgModule({
  declarations: [
    DrifterHitchPage,
  ],
  imports: [
    IonicPageModule.forChild(DrifterHitchPage),
    TranslateModule.forChild()
  ],
})
export class DrifterHitchPageModule {}
