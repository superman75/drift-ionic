import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';

/**
 * Generated class for the DrifterProposeHitchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drifter-propose-hitch',
  templateUrl: 'drifter-propose-hitch.html',
})
export class DrifterProposeHitchPage {

  constructor(
    public navCtrl: NavController) {
  }

  /*
    Called when user click 'sure' button
  */
  goSure() {
    this.navCtrl.push('DrifterHitchMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
      let index = this.navCtrl.getActive().index;
      this.navCtrl.remove(index-1);
    });
  };
  /*
    Called when user click 'no' button
  */
  goNo() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
