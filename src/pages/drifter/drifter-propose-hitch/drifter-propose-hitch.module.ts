import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrifterProposeHitchPage } from './drifter-propose-hitch';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DrifterProposeHitchPage,
  ],
  imports: [
    IonicPageModule.forChild(DrifterProposeHitchPage),
    TranslateModule.forChild()
  ],
})
export class DrifterProposeHitchPageModule {}
