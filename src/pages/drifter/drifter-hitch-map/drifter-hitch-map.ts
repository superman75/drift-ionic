import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { mapboxDistance, mapboxStyle, mapboxZoom, intervalTime} from '../../../app/app.config';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
import { UtilService } from '../../../providers/utilService';
import { BadgeService } from '../../../providers/badgeService';
/**
 * Generated class for the DrifterHitchMapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drifter-hitch-map',
  templateUrl: 'drifter-hitch-map.html',
})
export class DrifterHitchMapPage {
  loading: Loading;
  mapContainer = {
    map: null,
    currentMarker: null,
    userMarkerArray:[],
    driftpointMarkerArray:[],
  };
  updateUserInterval: any ;
  updateOthersInterval: any;
  platenumber: string = '';
  showingAlert: boolean = false;
  mapId: number;//current page mapid
  constructor(
    private navCtrl: NavController,
    private loadingCtrl: LoadingController, 
    private alertCtrl: AlertController, 
    private translateService: TranslateService, 
    private mapbox: MapboxService, 
    private location: LocationService,
    private util: UtilService,
    private badge: BadgeService) { 
    //get map id to avoid duplicate id
    this.mapId = this.mapbox.getMapId('drifter_hitch_map');
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
  */
  showAlert(content: string){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {}  
        }
      ]
    });
    alert.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      userMarkerArray:[],
      driftpointMarkerArray:[],
    };
    //check whether gps is available or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage);
    });
    //show map
    this.showMap(false, false, false);
  }
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    clearInterval(this.updateUserInterval);
    clearInterval(this.updateOthersInterval);
    this.mapContainer.map.remove();
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('drifter_hitch_map');
  }
  /*
    show map in the page
  */
  showMap(_mapUpdate: boolean, _userUpdate: boolean, _otherUpdate: boolean) {
    let mapOptions = {
      container: 'drifter_hitch_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: mapboxZoom,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      destinationlat: null,
      destinationlon: null,
      users: null,
      driftpoints: null,
      mode: 'Drifter'
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null){
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false, false, false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showDestinationMap(this.mapContainer, false, false, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //add other users to the map
            this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers)=> {
              dataOptions.users = resusers;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {});
            });
            //add driftpoints to the map
            this.location.getDriftPoints(cpos.latitude, cpos.longitude, mapboxDistance).then((resdrifts) => {
              dataOptions.driftpoints = resdrifts;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, false, null, dataOptions).then((res) => {});
            });
            //update intervals
            this.updateUserInterval = setInterval(() => {
              this.showMap(true, true, false);
            }, 5000);
            this.updateOthersInterval = setInterval(() => {
              this.showMap(true, false, true);
            }, intervalTime);
          }, (err) => {
            this.loading.dismiss();
            console.log(err);
          });
        }else if(_userUpdate == true){
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showDestinationMap(this.mapContainer, true, true, false, null, dataOptions).then((res) => {
            // update currnt user
          });
        }else if(_otherUpdate == true){
          this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers) => {
            dataOptions.users = resusers;
            this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {
              // update other users
            });          
          });
        }
      } 
    });
  }
  /*
    Called when user click 'done' button
  */
  goDone(){
    if(this.platenumber == ''){
      let alertMessage = this.translateService.instant('Alert.Message.TypePlatenumber');
      this.showAlert(alertMessage);
    }else{
      this.showLoading();
      this.util.startHitchhike(this.platenumber).then((res) => {
        this.loading.dismiss();
        this.navCtrl.push('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }, (err) => {
        this.loading.dismiss();
      });
    }
  }
  /* 
    Called when user click arrow button
  }
  */
  goBack(){
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
}
