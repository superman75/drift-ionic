import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DrifterHitchMapPage } from './drifter-hitch-map';

@NgModule({
  declarations: [
    DrifterHitchMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DrifterHitchMapPage),
    TranslateModule.forChild()
  ],
})
export class DrifterHitchMapPageModule {}
