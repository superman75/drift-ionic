import { Component, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, Platform, Events} from 'ionic-angular';

import { mapboxDistance, mapboxStyle, mapboxZoom, intervalTime, routeColor} from '../../../app/app.config';
import { AuthService } from '../../../providers/authService';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
import { UtilService } from '../../../providers/utilService';
import { SettingService } from '../../../providers/settingService';
import { BadgeService } from '../../../providers/badgeService';
/**
 * Generated class for the DrifterMapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drifter-map',
  templateUrl: 'drifter-map.html',
})
export class DrifterMapPage {
  loading: Loading;
  mapContainer = {
    map: null,
    currentMarker: null,
    destinationMarker: null,
    userMarkerArray:[],
    driftpointMarkerArray:[],
    idRoute: 0
  };
  destination: any;
  updateUserInterval: any ;
  updateOthersInterval: any;
  displayCommunityChat: boolean = true;
  mapId: number;//current page mapid
  rightCommunityMessages = [];
  leftCommunityMessages = [];
  messageSide: string = 'left';
  communityMessage: string = '';//commnitymessage
  showingAlert: boolean = false;
  proposed: boolean = false;
  showSosBadge: boolean = false;
  userInfo: any;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private translateService: TranslateService,
    private loadingCtrl: LoadingController,
    private location: LocationService,
    private mapbox: MapboxService,
    private auth: AuthService,
    private setting: SettingService,
    private platform: Platform,
    private alertCtrl: AlertController,
    private util: UtilService,
    private badge: BadgeService,
    private events: Events,
    private zone: NgZone) {
      this.updateUserInfo();
      this.displayCommunityChat = this.setting.getDisplayCommunityChat();
      let destinationParam = this.navParams.get('destination');
      if(destinationParam != null){
        this.destination = {
          lon: destinationParam.center[0],
          lat: destinationParam.center[1]
        };
      }else{
        this.destination = {
          lon: null,
          lat: null
        }
      }
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('drifter_map');
      /*
        listen for push notification received event
      */
      this.events.subscribe('notificationReceived', (_notification) => {
        this.zone.run(() => {
          let notification = JSON.parse(_notification);
          if(notification.type == 'communitymessage'){
            let fromUser = JSON.parse(notification.fromuser);
            var pictureurl = 'assets/img/malethumb.png';
            if(fromUser.pictureurl !== 'http://test.drift.group/storage'){
              pictureurl = fromUser.pictureurl;
            }
            let messageContent = {
              userid: fromUser.id,
              message: notification.message,
              pictureurl: pictureurl,
              bottom: 90,
              interval: null
            };
            if(this.rightCommunityMessages.length == 0 && this.leftCommunityMessages.length == 0){
              this.leftCommunityMessages.push(messageContent);
              this.animateCommunity(this.leftCommunityMessages[0]);  
            }else{
              if(this.messageSide == 'left'){
                if(fromUser.id == this.leftCommunityMessages[this.leftCommunityMessages.length-1].userid){
                  this.leftCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.leftCommunityMessages.push(messageContent);
                  this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]);  
                }else{
                  this.messageSide = 'right';
                  this.rightCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.rightCommunityMessages.push(messageContent);
                  this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
                }
              }else{
                if(fromUser.id == this.rightCommunityMessages[this.rightCommunityMessages.length-1].userid){
                  this.rightCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.rightCommunityMessages.push(messageContent);
                  this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
                }else{
                  this.messageSide = 'left';
                  this.leftCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.leftCommunityMessages.push(messageContent);
                  this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]); 
                }
              }
            }
          }
        });
      });
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    get open rquests
  */
  getTotalRequests() {
    return this.badge.getTotalRequests();
  }
  /*
    track requests
  */
  trackRequestsFn(index, item) {
    return item.userid;
  }
  /*
    update user info
  */
  updateUserInfo() {
    this.userInfo = this.auth.getUserInfo();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
    @parameter
      mode: 0-when location is not available
            1-when user has not vehicle, when user has not driverlicense
      content: message content
  */
  showAlert(content: string, mode?: number){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {
            switch (mode) {
              case 0:
                this.platform.exitApp();
                break;
              case 1:
                this.showLoading();
                this.util.getCountries().then((res) => {
                  this.loading.dismiss();
                  this.navCtrl.push('ProfileEditPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
                    let index = this.navCtrl.getActive().index;
                    this.navCtrl.remove(1, index-1);
                  });
                }, (err) => {
                  this.loading.dismiss();
                  console.log('error in getting countries data...');
                });
                break;
              default:
                // code...
                break;
            }
          }  
        }
      ]
    });
    alert.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    this.updateUserInfo();
    //show sos badge
    if(localStorage.getItem('drift_sosClicked') !== 'true' && (this.userInfo.emergencycontact1 == null && this.userInfo.emergencycontact2 == null)){
      this.showSosBadge = true;
    }else{
      this.showSosBadge = false;
    }
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      destinationMarker: null,
      userMarkerArray:[],
      driftpointMarkerArray:[],
      idRoute: 0
    };
    //check whether gps is availalbe or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage, 0);
    });
    //show map
    this.showMap(false, false, false);
    //update settings
    this.displayCommunityChat = this.setting.getDisplayCommunityChat();
    this.leftCommunityMessages = [];
    this.rightCommunityMessages = [];
    this.messageSide = 'left';
  }
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    clearInterval(this.updateUserInterval);
    clearInterval(this.updateOthersInterval);
    this.mapContainer.map.remove();
    this.leftCommunityMessages.forEach((_item: any) => {
      clearInterval(_item.interval);
    });
    this.rightCommunityMessages.forEach((_item: any) => {
      clearInterval(_item.interval);
    });
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('drifter_map');
  }
  /*
    show map in the page
  */
  showMap(_mapUpdate: boolean, _userUpdate: boolean, _otherUpdate: boolean) {
    let mapOptions = {
      container: 'drifter_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: mapboxZoom,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      destinationlat: this.destination.lat,
      destinationlon: this.destination.lon,
      users: null,
      driftpoints: null,
      mode: 'Drifter',
      routeColor: routeColor
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null){
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false, false, false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showDestinationMap(this.mapContainer, false, false, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //add other users to the map
            this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers: any)=> {
              dataOptions.users = resusers;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {});
              if(this.proposed == false){
                var driverCount = 0;
                resusers.forEach((_user)=> {//check drivers within distance around user
                  if(_user.mode == 'Driver'){
                    driverCount++;
                  }
                });
                if(driverCount == 0) {
                  this.navCtrl.push('DrifterProposeHitchPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
                }
                this.proposed = true;
              }
            });
            //add driftpoints to the map
            this.location.getDriftPoints(cpos.latitude, cpos.longitude, mapboxDistance).then((resdrifts) => {
              dataOptions.driftpoints = resdrifts;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, false, null, dataOptions).then((res) => {});
            });
            //update intervals
            this.updateUserInterval = setInterval(() => {
              this.showMap(true, true, false);
            }, 5000);
            this.updateOthersInterval = setInterval(() => {
              this.showMap(true, false, true);
            }, intervalTime);
          }, (err) => {
            this.loading.dismiss();
            console.log(err);
          });
        }else if(_userUpdate == true){
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showDestinationMap(this.mapContainer, true, true, false, null, dataOptions).then((res) => {
            // update current user
          });
        }else if(_otherUpdate == true){
          this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers) => {
            dataOptions.users = resusers;
            this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {
              // update other users
            });          
          });
        }
      }
    });
  }
  /*
    Called when user click arrow button
  */
  goBack(){
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
  /*
    Called when user click 'driver' button
  */
  goDriver(){
    let userInfo = this.auth.getUserInfo();
    if(userInfo.hasdriverslicense == 1){
      if(userInfo.hasvehicle == 1){
        this.navCtrl.push('DriverSelectPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
          let index = this.navCtrl.getActive().index;
          this.navCtrl.remove(1, index-1);
        });
      }else{
        let alertMessage = this.translateService.instant('Alert.Message.ShouldHaveVehicle');
        this.showAlert(alertMessage, 1);
      }
    }else{
      let alertMessage = this.translateService.instant('Alert.Message.ShouldHaveDriverLicence');
      this.showAlert(alertMessage, 1);
    }
  }
  /*
    Called when user click 'hitchhiker' icon
  */
  goHitch(){
    this.navCtrl.push('DrifterHitchPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click request badge area
  */
  goRequest(request){
    if(request.type == 'companionrequest'){
      this.goCompanionRequest();
    }else if(request.type == 'driftinvitation'){
      this.goDriftRequest();
    }else {
      this.goChatMessage(request.userid);
    }
  }
  /*
    Called when user click 'companionrequest' badge area
  */
  goCompanionRequest() {
    console.log('goCompanionRequest');
    this.navCtrl.push('ProfileCompanionsPage', {userid: this.userInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'driftrequest' badge area
  */
  goDriftRequest() {
    console.log('goDriftRequest');
    this.navCtrl.push('TravelsRequestsPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
  }
  /*
    Called when user click 'chatmessage' badge area
  */
  goChatMessage(_userid) {
    console.log('goChatMessage');
    this.navCtrl.push('ChatsPage', { friendsId: _userid }, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    show emergency alert
  */
  showEmergencyAlert() {
    let message = this.translateService.instant('Alert.Message.DeclareEmergency');
    let buttonYes = this.translateService.instant('Button.Yes');
    let buttonNo = this.translateService.instant('Button.No');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      message: message,
      buttons: [
        {
          text: buttonYes,
          handler: () => {
            this.declareEmergency();
          }
        },
        {
          text: buttonNo,
          role: 'cancel',
          handler: () => {
            // this.navCtrl.push('EmergencyContactPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          }
        }
      ]
    });
    alert.present();
  }
  /*
    Called when user click 'sos' button
  */
  goSos() {
    localStorage.setItem('drift_sosClicked', 'true');
    if(this.userInfo.emergencycontact1 == null && this.userInfo.emergencycontact2 == null){
      this.navCtrl.push('EmergencyContactPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }else{
      this.showEmergencyAlert();
    }
  }
  /*
    declare emergency
  */
  declareEmergency() {
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos != null){
        this.util.declareEmergency(cpos.latitude, cpos.longitude).then((res)=> {
          console.log(res);
        }, (err) => {
          console.log(err);
        });
      }
    });
  }
  /*
    Called when user keypress in community chat field
  */
  changeMessage(event: any){
    if(event.keyCode == 13){
      this.util.sendCommunityMessage(this.communityMessage).then((res) => {
        //send message successfully
      }, (err) => {
        console.log(err);
      });
      let userInfo = this.auth.getUserInfo();
      var userImg = 'assets/img/malethumb.png';
      if(userInfo.pictureurl)
        userImg = userInfo.pictureurl;
      let messageContent = {
        userid: userInfo.id,
        message: this.communityMessage,
        pictureurl: userImg,
        bottom: 90,
        interval: null
      };
      if(this.rightCommunityMessages.length == 0 && this.leftCommunityMessages.length == 0){
        this.leftCommunityMessages.push(messageContent);
        this.animateCommunity(this.leftCommunityMessages[0]);  
      }else{
        if(this.messageSide == 'left'){
          if(userInfo.id == this.leftCommunityMessages[this.leftCommunityMessages.length-1].userid){
            this.leftCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.leftCommunityMessages.push(messageContent);
            this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]);  
          }else{
            this.messageSide = 'right';
            this.rightCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.rightCommunityMessages.push(messageContent);
            this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
          }
        }else{
          if(userInfo.id == this.rightCommunityMessages[this.rightCommunityMessages.length-1].userid){
            this.rightCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.rightCommunityMessages.push(messageContent);
            this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
          }else{
            this.messageSide = 'left';
            this.leftCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.leftCommunityMessages.push(messageContent);
            this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]); 
          }
        }
      }
      this.communityMessage = '';
    }
  }
  /*
    animate community chat
  */
  animateCommunity(_item: any) {
    _item.interval = setInterval(() => {
      _item.bottom = _item.bottom + (this.platform.height() / 500);
      if(_item.bottom > (this.platform.height() + 10)){
        clearInterval(_item.interval);
      }
    }, 200);
  }
}
