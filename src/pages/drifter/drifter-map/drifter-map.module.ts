import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DrifterMapPage } from './drifter-map';

@NgModule({
  declarations: [
    DrifterMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DrifterMapPage),
    TranslateModule.forChild()
  ],
})
export class DrifterMapPageModule {}
