import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrifterSelectPage } from './drifter-select';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DrifterSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(DrifterSelectPage),
    TranslateModule.forChild()
  ],
})
export class DrifterSelectPageModule {}
