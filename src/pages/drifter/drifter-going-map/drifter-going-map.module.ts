import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { Camera } from '@ionic-native/camera';
import { DrifterGoingMapPage } from './drifter-going-map';

@NgModule({
  declarations: [
    DrifterGoingMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DrifterGoingMapPage),
    TranslateModule.forChild()
  ],
  providers: [
    Camera
  ]
})
export class DrifterGoingMapPageModule {}
