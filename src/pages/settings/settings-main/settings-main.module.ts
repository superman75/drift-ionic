import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SettingsMainPage } from './settings-main';

@NgModule({
  declarations: [
    SettingsMainPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsMainPage),
    TranslateModule.forChild()
  ],
})
export class SettingsMainPageModule {}
