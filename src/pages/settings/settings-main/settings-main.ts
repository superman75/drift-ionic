import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { SettingService } from '../../../providers/settingService';

/**
 * Generated class for the SettingsMainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings-main',
  templateUrl: 'settings-main.html',
})
export class SettingsMainPage {
  settingData:any = null;
  constructor(
    private navCtrl: NavController,
    private setting: SettingService) {
    this.settingData = this.setting.getSettings();
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.setting.setSettings(this.settingData);
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click 'changepassword' button
  */
  goPassword() {
    this.navCtrl.push('SettingsPasswordPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'emergency contacts' button 
  */
  goEmergency() {
    this.navCtrl.push('EmergencyContactPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
}
