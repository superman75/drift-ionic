import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
import { PasswordValidator } from '../../../validators/password.validator';
import { OldPasswordValidator } from '../../../validators/changePassword.validator';

/**
 * Generated class for the SettingsPasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings-password',
  templateUrl: 'settings-password.html',
})
export class SettingsPasswordPage {
  changePasswordGroup: FormGroup;
  matchingPasswordsGroup: FormGroup;
  submitted: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toastCtrl: ToastController,
    private translateService: TranslateService) {
  }
  /*
    show toast message
  */
  showToast(content) {
    let toast = this.toastCtrl.create({
      message: content,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  };
  /*
    triggerd when page will be loaded
  */
  ionViewWillLoad() {
    /*
      Create formgroup that contains password and confirm password control
    */
    this.matchingPasswordsGroup = new FormGroup({
      newPassword: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[^\w\s][ -~]+$')
      ])),
      confirmPassword: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });
    /*
      Create change password form group
    */
    this.changePasswordGroup = this.formBuilder.group({
      oldPassword: new FormControl('', Validators.compose([
        Validators.required,
        OldPasswordValidator.validPassword
      ])),
      matchingPasswords: this.matchingPasswordsGroup
    });
  };

  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click 'done!' button
  */
  onSubmit(){
    this.submitted = true;
    if(this.changePasswordGroup.valid){
      console.log(this.changePasswordGroup.value);
      this.auth.changePassword(this.changePasswordGroup.value.oldPassword, this.changePasswordGroup.value.matchingPasswords.newPassword).then((res)=> {
        let toastMessage = this.translateService.instant('Toast.ChangePassword');
        this.showToast(toastMessage);
      }, (err) => {
        console.log(err);
      });
    }
  };
}
