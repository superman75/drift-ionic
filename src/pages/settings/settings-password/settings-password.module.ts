import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SettingsPasswordPage } from './settings-password';

@NgModule({
  declarations: [
    SettingsPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPasswordPage),
    TranslateModule.forChild()
  ]
})
export class SettingsPasswordPageModule {}
