import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { ChatService } from '../../providers/chatService';

@IonicPage()
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
})
export class ChatsPage {
  insideRoom: boolean;

  @ViewChild('chatheader') chatheader: ElementRef;
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private chatService: ChatService) {

    window.addEventListener('guapp-chat-room-selected', (e) => {
      this.insideRoom = true;
      this.hideHeader();
    }, false);
    window.addEventListener('guapp-chat-room-left', (e) => {
      this.insideRoom = false;
      this.showHeader();
    }, false);
  }

  ionViewDidEnter() {
    this.insideRoom = false;
    var friendsId: number = this.navParams.get("friendsId");
    this.chatService.start_chat()
      .then(() => {
        if (friendsId)
          this.chatService.enter_room(friendsId);          
      });
  }

  hideHeader() {
    this.chatheader.nativeElement.classList.add("inside-room");
    this.content.setElementClass("inside-room", true);
  }
  showHeader() {
    this.chatheader.nativeElement.classList.remove("inside-room");
    this.content.setElementClass("inside-room", false);
  }

  goBack() {
    this.navCtrl.pop({ animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back' });
  };

  goByNativeBackButton() {
    if (this.insideRoom) {
      this.chatService.leave_chat_room();
    }
    else {
      this.goBack();
    }
  }

  ionViewWillLeave() {
    this.chatService.end_chat();
  }

}
