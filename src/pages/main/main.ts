import { Component, NgZone} from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, Loading, Platform, Events} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { mapboxDistance, mapboxStyle, intervalTime } from '../../app/app.config';
import { AuthService } from '../../providers/authService';
import { MapboxService } from '../../providers/mapboxService';
import { LocationService } from '../../providers/locationService';
import { UtilService } from '../../providers/utilService';
import { SettingService } from '../../providers/settingService';
import { BadgeService } from '../../providers/badgeService';
/**
 * Generated class for the MainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  loading: Loading;
  updateUserInterval: any;//interval instance that update user
  updateOthersInterval: any;//interval instance that update others
  communityMessage: string = '';//commnitymessage
  displayCommunityChat: boolean = true;//shows whether to show community message field or not
  mapContainer = {//map container
    map: null,
    currentMarker: null,
    userMarkerArray:[],
    driftpointMarkerArray:[],
  };
  rightCommunityMessages = [];
  leftCommunityMessages = [];
  messageSide: string = 'left';
  mapId: number;//current page mapid
  showingAlert: boolean = false;
  showSosBadge: boolean = false;
  userInfo: any = null;
  constructor(
    private navCtrl: NavController,
    private platform: Platform,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private translateService: TranslateService,
    private mapbox: MapboxService,
    private location: LocationService,
    private auth: AuthService,
    private util: UtilService,
    private badge: BadgeService,
    private setting: SettingService,
    private events: Events,
    private zone: NgZone) {
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('main_map');
      this.showingAlert = false;
      this.userInfo = this.auth.getUserInfo();
      this.displayCommunityChat = this.setting.getDisplayCommunityChat();
      /*
        listen for push notification received event
      */
      this.events.subscribe('notificationReceived', (_notification) => {
        this.zone.run(() => {
          let notification = JSON.parse(_notification);
          if(notification.type == 'communitymessage'){
            let fromUser = JSON.parse(notification.fromuser);
            var pictureurl = 'assets/img/malethumb.png';
            if(fromUser.pictureurl !== 'http://test.drift.group/storage'){
              pictureurl = fromUser.pictureurl;
            }
            let messageContent = {
              userid: fromUser.id,
              message: notification.message,
              pictureurl: pictureurl,
              bottom: 60,
              interval: null
            };
            if(this.rightCommunityMessages.length == 0 && this.leftCommunityMessages.length == 0){
              this.leftCommunityMessages.push(messageContent);
              this.animateCommunity(this.leftCommunityMessages[0]);  
            }else{
              if(this.messageSide == 'left'){
                if(fromUser.id == this.leftCommunityMessages[this.leftCommunityMessages.length-1].userid){
                  this.leftCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.leftCommunityMessages.push(messageContent);
                  this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]);  
                }else{
                  this.messageSide = 'right';
                  this.rightCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.rightCommunityMessages.push(messageContent);
                  this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
                }
              }else{
                if(fromUser.id == this.rightCommunityMessages[this.rightCommunityMessages.length-1].userid){
                  this.rightCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.rightCommunityMessages.push(messageContent);
                  this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
                }else{
                  this.messageSide = 'left';
                  this.leftCommunityMessages.forEach((_item: any) => {
                    _item.bottom = _item.bottom + 40;
                  });
                  this.leftCommunityMessages.push(messageContent);
                  this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]); 
                }
              }
            }
          }
        });
      });
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
    @parameter
      mode: 0-when location is not available
            1-when user has not vehicle, when user has not driverlicense
      content: message content
  */
  showAlert(content: string, mode?: number){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {
            switch (mode) {
              case 0:
                this.platform.exitApp();
                break;
              case 1:
                this.showLoading();
                this.util.getCountries().then((res) => {
                  this.loading.dismiss();
                  this.navCtrl.push('ProfileEditPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
                }, (err) => {
                  this.loading.dismiss();
                  console.log('error in getting countries data...');
                });
                break;
              case 2:
                this.navCtrl.push('TravelsDriftPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
              default:
                // code...
                break;
            }
          }
        }
      ]
    });
    alert.present();
  }
  /*
    Runs when the page has loaded.
    This event only happens once per page being created.
    If a page leaves but is cached, then this event will not fire again on a subsequent viewing.
    This event is good place to put your setup code for the page
  */
  ionViewDidLoad() {

  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    this.userInfo = this.auth.getUserInfo();
    //show sos badge
    if(localStorage.getItem('drift_sosClicked') !== 'true' && (this.userInfo.emergencycontact1 == null && this.userInfo.emergencycontact2 == null)){
      this.showSosBadge = true;
    }else{
      this.showSosBadge = false;
    }
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      userMarkerArray:[],
      driftpointMarkerArray:[],
    };
    //check whether gps is available or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage, 0);
    });
    this.showMap(false, false, false);
    this.displayCommunityChat = this.setting.getDisplayCommunityChat();
    this.leftCommunityMessages = [];
    this.rightCommunityMessages = [];
    this.messageSide = 'left';
  }
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    clearInterval(this.updateUserInterval);
    clearInterval(this.updateOthersInterval);
    this.mapContainer.map.remove();
    this.leftCommunityMessages.forEach((_item: any) => {
      clearInterval(_item.interval);
    });
    this.rightCommunityMessages.forEach((_item: any) => {
      clearInterval(_item.interval);
    });
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('main_map');
  }
  /*
    show map in the page
  */
  showMap(_mapUpdate: boolean, _userUpdate: boolean, _otherUpdate: boolean) {
    let mapOptions = {
      container: 'main_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: 10,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      users: null,
      driftpoints: null,
      mode: 'Drifter'
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null){
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false, false, false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showMainMap(this.mapContainer, false, false, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //update intervals
            this.updateUserInterval = setInterval(() => {
              this.showMap(true, true, false);
            }, 5000);
            this.updateOthersInterval = setInterval(() => {
              this.showMap(true, false, true);
            }, intervalTime);
          }, (err) => {
            console.log(err);
          });
        }else if(_userUpdate == true){
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showMainMap(this.mapContainer, true, true, false, null, dataOptions).then((res) => {
            // update current user
          });
        }else if(_otherUpdate == true){
          this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers) => {
            dataOptions.users = resusers;
            this.mapbox.showMainMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {
              // update other users
            });
          });
        }
      }
    });
  }
  /*
    Called when user click 'drifter' button
  */
  goDrifter(){
    this.showLoading();
    this.auth.getDrifts().then((res: any) => {//check whether user is drifting now or not
      this.loading.dismiss();
      if(res.running.length == 0 && res.agreed.length == 0){
        this.navCtrl.push('DrifterSelectPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }else{
        let alertMessage = this.translateService.instant('Alert.Message.ShouldNotChangeMode');
        this.showAlert(alertMessage, 2);
      }
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  }

  /*
    Called when user click 'driver' button
  */
  goDriver(){
    this.showLoading();
    this.auth.getDrifts().then((res: any) => {//check whether user is drifting now or not
      this.loading.dismiss();
      if(res.running.length == 0 && res.agreed.length == 0){
        if(this.userInfo.hasdriverslicense == 1){
          if(this.userInfo.hasvehicle == 1){
            this.navCtrl.push('DriverSelectPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          }else{
            let alertMessage = this.translateService.instant('Alert.Message.ShouldHaveVehicle');
            this.showAlert(alertMessage, 1);
          }
        }else{
          let alertMessage = this.translateService.instant('Alert.Message.ShouldHaveDriverLicence');
          this.showAlert(alertMessage, 1);
        }
      }else{
        let alertMessage = this.translateService.instant('Alert.Message.ShouldNotChangeMode');
        this.showAlert(alertMessage, 2);
      }
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  }
  /*
    Called when user click image of communitchat message
  */
  goUser(message){
    if(message.userid != null){
      this.navCtrl.push('UserProfilePage', {userid: message.userid}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }
  }
  /*
    show emergency alert
  */
  showEmergencyAlert() {
    let message = this.translateService.instant('Alert.Message.DeclareEmergency');
    let buttonYes = this.translateService.instant('Button.Yes');
    let buttonNo = this.translateService.instant('Button.No');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      message: message,
      buttons: [
        {
          text: buttonYes,
          handler: () => {
            this.declareEmergency();
          }
        },
        {
          text: buttonNo,
          role: 'cancel',
          handler: () => {
            // this.navCtrl.push('EmergencyContactPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          }
        }
      ]
    });
    alert.present();
  }
  /*
    Called when user click 'sos' button
  */
  goSos() {
    localStorage.setItem('drift_sosClicked', 'true');
    if(this.userInfo.emergencycontact1 == null && this.userInfo.emergencycontact2 == null){
      this.navCtrl.push('EmergencyContactPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }else{
      this.showEmergencyAlert();
    }
  }
  /*
    declare emergency
  */
  declareEmergency() {
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos != null){
        this.util.declareEmergency(cpos.latitude, cpos.longitude).then((res)=> {
          console.log(res);
        }, (err) => {
          console.log(err);
        }); 
      }
    });
  }
  /*
    Called when user keypress in community chat field
  */
  changeMessage(event: any){
    if(event.keyCode == 13){
      this.util.sendCommunityMessage(this.communityMessage).then((res) => {
        //send message successfully
      }, (err) => {
        console.log(err);
      });
      let userInfo = this.auth.getUserInfo();
      var userImg = 'assets/img/malethumb.png';
      if(userInfo.pictureurl)
        userImg = userInfo.pictureurl;
      let messageContent = {
        userid: userInfo.id,
        message: this.communityMessage,
        pictureurl: userImg,
        bottom: 60,
        interval: null
      };
      if(this.rightCommunityMessages.length == 0 && this.leftCommunityMessages.length == 0){
        this.leftCommunityMessages.push(messageContent);
        this.animateCommunity(this.leftCommunityMessages[0]);  
      }else{
        if(this.messageSide == 'left'){
          if(userInfo.id == this.leftCommunityMessages[this.leftCommunityMessages.length-1].userid){
            this.leftCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.leftCommunityMessages.push(messageContent);
            this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]);  
          }else{
            this.messageSide = 'right';
            this.rightCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.rightCommunityMessages.push(messageContent);
            this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
          }
        }else{
          if(userInfo.id == this.rightCommunityMessages[this.rightCommunityMessages.length-1].userid){
            this.rightCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.rightCommunityMessages.push(messageContent);
            this.animateCommunity(this.rightCommunityMessages[this.rightCommunityMessages.length-1]); 
          }else{
            this.messageSide = 'left';
            this.leftCommunityMessages.forEach((_item: any) => {
              _item.bottom = _item.bottom + 40;
            });
            this.leftCommunityMessages.push(messageContent);
            this.animateCommunity(this.leftCommunityMessages[this.leftCommunityMessages.length-1]); 
          }
        }
      }
      this.communityMessage = '';
    }
  }
  /*
    animate community chat
  */
  animateCommunity(_item: any) {
    _item.interval = setInterval(() => {
      _item.bottom = _item.bottom + (this.platform.height() / 500);
      if(_item.bottom > (this.platform.height() + 10)){
        clearInterval(_item.interval);
      }
    }, 200);
  }
}
