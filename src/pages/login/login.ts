
import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, ToastController} from 'ionic-angular'
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../providers/authService';
import { UtilService } from '../../providers/utilService';
import { LocationService } from '../../providers/locationService';
import { ChatService } from '../../providers/chatService';
import { BadgeService } from '../../providers/badgeService';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	loading: Loading;
  credentials = { email: '', password: '' };
  userInfo: any;
  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    private badge: BadgeService,
    private chatService: ChatService,
    private loadingCtrl: LoadingController, 
    private toastCtrl: ToastController, 
    private translateService: TranslateService,
    private location: LocationService,
    private util: UtilService) {
  };
  /*
    show toast message
  */
  showToast(content) {
    let toast = this.toastCtrl.create({
      message: content,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    Called when user click 'login' button
  */
  login() {
    if(this.credentials.email == '' || this.credentials.password == ''){
      let toastMessage = this.translateService.instant('Toast.InsertLogin');
      this.showToast(toastMessage);
    }else{
      this.showLoading();
      this.auth.login(this.credentials).then((result) => {
        this.userInfo = this.auth.getUserInfo();
        this.badge.refreshRequests();
        this.chatService.init_chat();
        this.auth.getDrifts().then((res: any) => {//check whether user is drifting now or not
          this.loading.dismiss();
          if(res.running.length > 0){
            if(this.userInfo.mode == 'Driver'){
              this.navCtrl.setRoot('DriverGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
            }else{
              this.navCtrl.setRoot('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
            }
          }else{
            this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          }
          this.location.updateCurrentLocation();
          this.location.findGuys();
        }, (err)=> {
          this.loading.dismiss();
          console.log(err);
        });
      }, (err) => {
        this.loading.dismiss();
        let toastMessage = this.translateService.instant('Toast.InvalidLogin');
        this.showToast(toastMessage);
      });
    }
  };
  /*
    Called when user click 'Register' button
  */
  goRegister() {
    this.showLoading();
    this.util.getCountries().then((res) => {
      this.loading.dismiss();
      this.navCtrl.push('RegisterFormPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    })
  };
}
