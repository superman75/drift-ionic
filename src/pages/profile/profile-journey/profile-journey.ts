import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the ProfileJourneyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-journey',
  templateUrl: 'profile-journey.html',
})
export class ProfileJourneyPage {
  loading:Loading;
  drifts: Array<any> = [];
  noJourney: boolean = false;//show whether current user has journey or not 
  noMoreJourney: boolean = false;
  myjourney: boolean = true;//shows whether this journey is current user's or not
  transparam: any;//translate parameter
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private userSerivce: UserService,
    private auth: AuthService,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService) {
      this.showLoading();
      this.drifts = [];
      this.noJourney = false;
      if(this.auth.getUserInfo().id !== this.navParams.get('userid')){
        this.myjourney = false;
        this.transparam = {
          username: this.navParams.get('username')
        };
      }
      this.getJourney();
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    get journey
  */
  getJourney() {
    this.userSerivce.getDrifts(this.navParams.get('userid')).then((res: Array<any>) => {
      this.loading.dismiss();
      if(res.length == 0){
        this.noJourney = true;
      }else{
        this.noJourney = false;
      }
      res.forEach((_drift) => {
        if(!_drift.coverpictureurl){
          _drift.coverpictureurl = 'assets/img/no-image.png';
        }
        this.drifts.push(_drift);
      });
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click journey image
  */
  goPhoto(drift) {
    this.navCtrl.push('ProfilePhotoPage', {userid: this.navParams.get('userid'), driftid: drift.drift_id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
}
