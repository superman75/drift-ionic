import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileJourneyPage } from './profile-journey';

@NgModule({
  declarations: [
    ProfileJourneyPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileJourneyPage),
    TranslateModule.forChild()
  ],
})
export class ProfileJourneyPageModule {}
