import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileMainPage } from './profile-main';

@NgModule({
  declarations: [
    ProfileMainPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileMainPage),
    TranslateModule.forChild()
  ],
})
export class ProfileMainPageModule {}
