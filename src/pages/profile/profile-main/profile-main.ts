import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from '../../../providers/utilService';
import { UserService } from '../../../providers/userService';
import { AuthService } from '../../../providers/authService';
import { BadgeService } from '../../../providers/badgeService';
import { Perks } from '../../../models/perks.model';
/**
 * Generated class for the ProfileMainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-main',
  templateUrl: 'profile-main.html',
})
export class ProfileMainPage {
  loading: Loading;
  userInfo: any = {};//user profile information
  journeyplaceholderpicture: string = 'assets/img/journey-thumb.jpg';
  userImages = [];//user profile images to show slide
  perks = [];//user perks array
  reviews = []; //user reviews array
  companions = []; //user friends array
  showPerksMore: boolean = false;//show or not more... button
  showReviewsMore: boolean = false;//show or not readmore... button
  constructor(
    private navCtrl: NavController,
    private util: UtilService,
    private auth: AuthService,
    private badge: BadgeService,
    private loadingCtrl: LoadingController, 
    private translateService: TranslateService,
    private userService: UserService) {
      //prepare the user profile data to show page
      this.showLoading();
      this.auth.getUser().then((res) => {
        this.loading.dismiss();
        this.userInfo = res;
        this.loadProfile();
      }, (err) => {
        console.log(err);
      });
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter() {
    if(localStorage.getItem('drift_profileupdated') == 'true'){
      this.userInfo = this.auth.getUserInfo();
      this.loadProfile();
      localStorage.removeItem('drift_profileupdated');
    }
  }
  /*
    load profile
  */
  loadProfile(){
    this.userImages = [];
    this.companions = [];
    this.reviews = [];
    this.perks = Perks.getList(false, this.userInfo.perks);
    if(this.perks.length > 4){//if perks count is more than 4 show 'more...' button
      this.showPerksMore = true;
    }
    //prepare slide images first is user profile image, last is user vehicle image
    if(this.userInfo.pictureurl){
      this.userImages.push({id:'profile', url:this.userInfo.pictureurl});
    }
    if(this.userInfo.pictures.length > 0){
      this.userImages = this.userImages.concat(this.userInfo.pictures);
    }
    if(this.userInfo.vehicle){
      if(this.userInfo.vehicle.pictureurl != null)
        this.userImages.push({id:'vehicle', url:this.userInfo.vehicle.pictureurl});
    }
    //get journey placeholder picture
    if(this.userInfo.journeyplaceholderpicture){
      this.journeyplaceholderpicture = this.userInfo.journeyplaceholderpicture;
    }
    //prepare reviews
    if((this.userInfo.upvotes + this.userInfo.downvotes) > 2){
      this.showReviewsMore = true;
    }
    if(this.userInfo.reviews){
      this.reviews = this.userInfo.reviews;
    }
    //get friends list
    this.userService.getCompanions(this.userInfo.id).then((res: Array<any>) => {
      res.forEach((_companion)=> {
        if(!_companion.pictureurl || _companion.pictureurl == 'http://test.drift.group/storage'){
          _companion.pictureurl = 'assets/img/malethumb.png';
        }
      });
      this.companions = res;
    });
  }
  /*
    called when user click 'more...' button in perks area
  */
  clickPerksMore(){
    this.showPerksMore = false;
  }
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
  /*
    Called when user click 'edit profile' button
  */
  goEdit() {
    this.showLoading();
    this.util.getCountries().then((res) => {
      this.loading.dismiss();
      this.navCtrl.push('ProfileEditPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  }
  /*
    Called when user click 'journeys' image
  */
  goJourneys() {
    this.navCtrl.push('ProfileJourneyPage', {userid: this.userInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'read more...' button
  */
  goReviews() {
    this.navCtrl.push('ProfileReviewPage', {userid: this.userInfo.id, upvotes: this.userInfo.upvotes, downvotes: this.userInfo.downvotes}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'companions' arrow
  */
  goCompanions() {
    this.navCtrl.push('ProfileCompanionsPage', {userid: this.userInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click other user
  */
  goUser(companion){
    this.navCtrl.push('UserProfilePage', {userid: companion.user_id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
}
