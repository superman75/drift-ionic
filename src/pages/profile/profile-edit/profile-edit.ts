import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { PhoneValidator } from '../../../validators/phone.validator';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { Country } from '../../../models/country.model';
import { UtilService } from '../../../providers/utilService';
import { AuthService } from '../../../providers/authService';
import { BadgeService } from '../../../providers/badgeService';
import { Perks } from '../../../models/perks.model';

/**
 * Generated class for the ProfileEditPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html',
})
export class ProfileEditPage {
  loading: Loading;
  userInfo: any = {};//user profile information
  editFormGroup: FormGroup;//edit form group
  countryPhoneGroup: FormGroup;//country validate form group
  submitted: boolean = false;//shows whether user click submit button or not
  countries: Array<Country> = [];//countries data received from server
  selectOptions: object;//country select title to show when user click countries select
  checkNicknameText: string = '';//checking text when user type nickname
  checkEmailText: string = '';//checking text when user type email
  aboutCount = 0;//shows user's about character count
  perks=[];//user's perks array
  profilepicture;//user profile picture
  profileChanged: boolean = false;//shows the profile picture is changed or not
  vehiclepicture;//user vehicle picture
  vehicleChanged: boolean = false;//shows the vehicle picture is changed or not
  pictures = [];//user's picture array;
  changed = [];//changed user picture array;
  constructor(
    private navCtrl: NavController, 
    private formBuilder: FormBuilder, 
    private loadingCtrl: LoadingController,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController, 
    private translateService: TranslateService,
    private camera: Camera,
    private util: UtilService,
    private auth: AuthService,
    private badge: BadgeService) {
      //prepare the user profile data to show page
      this.userInfo = this.auth.getUserInfo();
      this.profilepicture = 'assets/img/malethumb.png';
      this.profileChanged = false;
      this.vehiclepicture = 'assets/img/car-thumb.jpg';
      this.vehicleChanged = false;
      this.pictures= [
        {id: null, url: 'assets/img/placeholder.png', changed: false},
        {id: null, url: 'assets/img/placeholder.png', changed: false},
        {id: null, url: 'assets/img/placeholder.png', changed: false},
        {id: null, url: 'assets/img/placeholder.png', changed: false},
        {id: null, url: 'assets/img/placeholder.png', changed: false}
      ];
      this.perks = Perks.getList(true, this.userInfo.perks);
      //prepare slide images first is user profile image, last is user vehicle image
      if(this.userInfo.pictureurl){
        this.profilepicture = this.userInfo.pictureurl;
      }
      if(this.userInfo.pictures.length > 0){
        for(let i=0; i<this.userInfo.pictures.length; i++){
          this.pictures[i] = this.userInfo.pictures[i];
          this.pictures[i] = {
            id: this.userInfo.pictures[i].id,
            url: this.userInfo.pictures[i].url,
            changed: false
          }
        }
      }
      var numberOfLineBreaks = (this.userInfo.about.match(/\n/g) || []).length;
      this.aboutCount = this.userInfo.about.length + numberOfLineBreaks;
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    Runs when page will be loaded
  */
  ionViewWillLoad() {
    /*
      get countries data
    */
    this.countries = this.util.countries;
    this.translateService.get('Label.Country').subscribe((value) => {
      this.selectOptions = {
        title: value
      };
    });
    /*
      Create formgroup that contains country and phone number control
    */
    var userCountry:Country;
    this.countries.forEach((_country) => {
      if(_country.iso == this.userInfo.countrycode){
        userCountry = _country;
      }
    });
    let country = new FormControl(userCountry, Validators.required);
    let phone = new FormControl(this.userInfo.mobile, Validators.compose([
      Validators.required,
      PhoneValidator.validCountryPhone(country)
    ]));
    this.countryPhoneGroup = new FormGroup({
      country: country,
      phone: phone
    });
    /*
      Create edit form group
    */
    this.editFormGroup = this.formBuilder.group({
      nickname: new FormControl(this.userInfo.nickname),
      birthdate: new FormControl(this.userInfo.birthdate, Validators.required),
      aboutme: new FormControl(this.userInfo.about),
      profession: new FormControl(this.userInfo.profession),
      platenumber: new FormControl(''),
      carmodel: new FormControl(''),
      hasdriverslicense: new FormControl((this.userInfo.hasdriverslicense == 1) ? true : false),
      email: new FormControl(this.userInfo.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      countryPhone: this.countryPhoneGroup,
      city: new FormControl(this.userInfo.city, Validators.required),
      address: new FormControl(this.userInfo.address, Validators.required),
      zip: new FormControl(this.userInfo.zip, Validators.required)
    });
    /*
      if vehicle exist patch values
    */
    if(this.userInfo.vehicle){
      if(this.userInfo.vehicle.pictureurl != null){
        this.vehiclepicture = this.userInfo.vehicle.pictureurl;
      }
      this.editFormGroup.patchValue({
        platenumber: this.userInfo.vehicle.platenumber,
        carmodel: this.userInfo.vehicle.carmodel
      });
    }
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
  Called when user click image or thumb icon
  */
  showActionSheet(_picname: string) {
    let titleChoosePicture = this.translateService.instant('ActionSheet.Title.ChoosePicture');
    let buttonChooseAlbums = this.translateService.instant('ActionSheet.Button.ChooseAlbums');
    let buttonChooseCamera = this.translateService.instant('ActionSheet.Button.ChooseCamera');
    let actionSheet = this.actionSheetCtrl.create({
      title: titleChoosePicture,
      buttons: [
        {
          text: buttonChooseAlbums,
          icon: 'albums',
          handler: () => {
            let options: CameraOptions = {
              quality: 75,
              destinationType: this.camera.DestinationType.DATA_URL,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: true,
              encodingType: this.camera.EncodingType.JPEG,
              targetWidth: 150,
              targetHeight: 150,
              saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then((imageData) => {
              //imageData is either a base64 encoded string or a file URI
              //If it's base64:
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              switch (_picname) {
                case "profile":
                  this.profilepicture = base64Image;
                  this.profileChanged = true;
                  break;
                case "0":
                  this.pictures[0].url = base64Image;
                  this.pictures[0].changed = true;
                  break;
                case "1":
                  this.pictures[1].url = base64Image;
                  this.pictures[1].changed = true;
                  break;
                case "2":
                  this.pictures[2].url = base64Image;
                  this.pictures[2].changed = true;
                  break;
                case "3":
                  this.pictures[3].url = base64Image;
                  this.pictures[3].changed = true;
                  break;
                case "4":
                  this.pictures[4].url = base64Image;
                  this.pictures[4].changed = true;
                  break;
                case "vehicle":
                  this.vehiclepicture = base64Image;
                  this.vehicleChanged = true;
                  break;
              }
            }, (err) => {
              //Handle error
            });
          }
        },
        {
          text: buttonChooseCamera,
          icon: 'camera',
          handler: () => {
            let options: CameraOptions = {
              quality: 75,
              destinationType: this.camera.DestinationType.DATA_URL,
              sourceType: this.camera.PictureSourceType.CAMERA,
              allowEdit: true,
              encodingType: this.camera.EncodingType.JPEG,
              targetWidth: 150,
              targetHeight: 150,
              saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then((imageData) => {
              //imageData is either a base64 encoded string or a file URI
              //If it's base64:
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              switch (_picname) {
                case "profile":
                  this.profilepicture = base64Image;
                  this.profileChanged = true;
                  break;
                case "0":
                  this.pictures[0].url = base64Image;
                  this.pictures[0].changed = true;
                  break;
                case "1":
                  this.pictures[1].url = base64Image;
                  this.pictures[1].changed = true;
                  break;
                case "2":
                  this.pictures[2].url = base64Image;
                  this.pictures[2].changed = true;
                  break;
                case "3":
                  this.pictures[3].url = base64Image;
                  this.pictures[3].changed = true;
                  break;
                case "4":
                  this.pictures[4].url = base64Image;
                  this.pictures[4].changed = true;
                  break;
                case "vehicle":
                  this.vehiclepicture = base64Image;
                  this.vehicleChanged = true;
                  break;
              }
            }, (err) => {
              //Handle error
            });
          }
        }
      ]
    });
    actionSheet.present();
  }
  /*
    Called when user click 'done!' button
  */
  onSubmit(values){
    this.submitted = true;
    if(this.editFormGroup.valid && this.checkNicknameText != 'Label.NicknameNotAvailable' && this.checkEmailText != 'Label.EmailNotAvailable'){
      this.showLoading();
      //submit profile
      this.updateVehicle();
    }else{
      alert('please fill out required fields');
    }
  }
  /*
    update vehicle
  */
  updateVehicle() {
    if(this.editFormGroup.get('platenumber').value == '' || this.editFormGroup.get('carmodel').value == ''){
      let messageUploadPicture = this.translateService.instant('Alert.Message.IfYouHave');
      let buttonBack = this.translateService.instant('Alert.Button.Back');
      let buttonSkip = this.translateService.instant('Alert.Button.Skip');
      let alert = this.alertCtrl.create({
        title: `<img src="assets/img/logo.png"/>`,
        message: messageUploadPicture,
        buttons: [
          {
            text: buttonBack,
            role: 'cancel',
            handler: () => {
              this.loading.dismiss();
            }
          },
          {
            text: buttonSkip,
            handler: () =>{
              this.updateProfilePicture();
            }
          }
        ]
      });
      alert.present();
    }else{
      if(this.vehicleChanged){
        this.auth.updateVehicle(this.editFormGroup.get('platenumber').value, this.editFormGroup.get('carmodel').value, this.vehiclepicture).then((res) => {
          this.updateProfilePicture();
        }, (err) => {
          this.loading.dismiss();
          alert('update vehicle picture error... please try again later!');
        });
      }else{
        this.auth.updateVehicle(this.editFormGroup.get('platenumber').value, this.editFormGroup.get('carmodel').value).then((res) => {
          this.updateProfilePicture();
        }, (err) => {
          this.loading.dismiss();
          alert('update vehicle picture error... please try again later!');
        });
      }
    }
  }
  /*
    update profile picture
  */
  updateProfilePicture() {
    if(this.profileChanged){
      this.auth.updateProfilePicture(this.profilepicture).then((res) => {
        this.updateUserPictures();
      }, (err) => {
        this.loading.dismiss();
        alert('update profile picture error... please try again later!');
      });
    }else{
      this.updateUserPictures();
    }
  }
  /*
    update user pictures
  */
  updateUserPictures() {
    this.changed = [];
    this.pictures.forEach((_picture) => {
      if(_picture.changed == true){
        this.changed.push(_picture);
      }
    });
    if(this.changed.length == 0){
      this.updateAbout();
    }else{
      for(let i=0; i<this.changed.length; i++){
        this.uploadUserPicture(i, this.changed[i]);
      }
    }
  }
  /*
    upload picture
  */
  uploadUserPicture(index, picture){
    if(picture.id !== null){
      this.auth.changeUserPicture(picture).then((res) => {
        if(index == this.changed.length-1){
          this.updateAbout();
        }
      }, (err) => {
        this.loading.dismiss();
        alert('update user picture error... please try again later!');
      });
    }else{
      this.auth.uploadUserPicture(picture).then((res) => {
        if(index == this.changed.length-1){
          this.updateAbout();
        } 
      }, (err) => {
        this.loading.dismiss();
        alert('upload user picture error... please try again later!');
      });
    }
  }
  /*
    update about character
  */
  updateAbout() {
    this.auth.updateAbout(this.editFormGroup.get('aboutme').value).then((res) => {
      this.updatePerks();
    }, (err) => {
      this.loading.dismiss();
      alert('update about characters error... please try again later!');
    });
  }
  /*
    update perks
  */
  updatePerks() {
    let perksString = Perks.getString(this.perks);
    if(perksString !== ''){
      this.auth.updatePerk(perksString).then((res) => {
        this.updateProfile();
      }, (err) => {
        this.loading.dismiss();
        alert('update perks error... please try again later!');
      });
    }else{
      this.updateProfile();
    }
  }
  /*
    update profile
  */
  updateProfile() {
    this.auth.update(this.editFormGroup.value).then((res) => {
      localStorage.setItem('drift_profileupdated', 'true');
      this.loading.dismiss();
      this.goBack();
    }, (err) => {
      this.loading.dismiss();
      alert('update profile error... please try again later!');
    });
  }
  /*
     Called when blue email text field
  */
  checkEmail(){
    if(this.editFormGroup.get('email').valid && this.editFormGroup.get('email').value != this.userInfo.email){
      this.checkEmailText = 'Label.Checking';
      this.auth.checkEmail(this.editFormGroup.get('email').value).then((res) => {
        this.checkEmailText = 'Label.EmailAvailable';
      }, (err) => {
        this.loading.dismiss();
        this.checkEmailText = 'Label.EmailNotAvailable';
      });
    }
  }
  /*
    Called when user change email text field
  */
  changeEmail() {
    this.checkEmailText = '';
  }
  /*
    Called when user change textarea content
  */
  changeAbout() {
    var numberOfLineBreaks = (this.editFormGroup.get('aboutme').value.match(/\n/g) || []).length;
    this.aboutCount = this.editFormGroup.get('aboutme').value.length + numberOfLineBreaks;
  }
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
}
