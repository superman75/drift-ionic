import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { TextMaskModule } from 'angular2-text-mask';
import { ProfileEditPage } from './profile-edit';
import { Camera} from '@ionic-native/camera';

@NgModule({
  declarations: [
    ProfileEditPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileEditPage),
    TranslateModule.forChild(),
    TextMaskModule
  ],
  providers: [
    Camera
  ]
})
export class ProfileEditPageModule {}
