import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileCompanionsPage } from './profile-companions';

@NgModule({
  declarations: [
    ProfileCompanionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileCompanionsPage),
    TranslateModule.forChild()
  ],
})
export class ProfileCompanionsPageModule {}
