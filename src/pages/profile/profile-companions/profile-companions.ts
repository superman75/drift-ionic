import { Component, NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, Events} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { AuthService } from '../../../providers/authService';
import { UtilService } from '../../../providers/utilService';

/**
 * Generated class for the ProfileCompanionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-companions',
  templateUrl: 'profile-companions.html',
})
export class ProfileCompanionsPage {
  loading: Loading;
  companions: Array<any>=[];
  requests: Array<any>=[];
  query: string = '';
  noCompanion: boolean = false;
  mycompanions: boolean = false;//shows whether this journey is current user's or not
  transparam: any;//translate parameter
  initSearch: boolean = false;//user start search or not
  itemcount: number = 20;
  cPagenumber: number = 0;
  sPagenumber: number = 0;
  noMorePeople: boolean = false;
  opencompanionrequestcount: number = 0;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private auth: AuthService,
    private userService: UserService,
    private util: UtilService,
    private translateService:TranslateService,
    private loadingCtrl: LoadingController,
    private events: Events,
    private zone: NgZone) {
      this.showLoading();
      this.companions = [];
      this.requests = [];
      this.noCompanion = false;
      if(this.auth.getUserInfo().id !== this.navParams.get('userid')){
        this.mycompanions = false;
        this.transparam = {
          username: this.navParams.get('username')
        };
      }else{
        this.mycompanions = true;
        this.opencompanionrequestcount = this.auth.getUserInfo().opencompanionrequestcount;
        this.auth.getCompanionRequests().then((res: Array<any>)=> {
          res.forEach((_request)=> {
            this.userService.getUserInfo(_request.otheruser_id).then((_user: any)=> {
              if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
                _request.pictureurl = 'assets/img/malethumb.png';
              }else{
                _request.pictureurl = _user.pictureurl;
              }
              _request.nickname = _user.nickname;
              this.requests.push(_request);
            });
          });
        },(err) => {
          console.log(err);
        });
      }
      this.getCompanions();
      /*
        listen for ther user info updated event
        and update openrequestcount
      */
      this.events.subscribe('getUserInfo', () => {
        this.zone.run(() => {
          this.opencompanionrequestcount = this.auth.getUserInfo().opencompanionrequestcount;
        });
      });
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    search people
  */
  search(query: string, mode: string, infiniteScroll?: any){
    if (mode == 'more') {
      if (this.initSearch == false) {
        this.getCompanions(infiniteScroll);
      } else {
        this.searchPeople(query, infiniteScroll);
      }
    } else {
      this.initSearch = true;
      this.cPagenumber = 0;
      this.sPagenumber = 0;
      this.companions = [];
      this.showLoading();
      this.noMorePeople = false;
      this.searchPeople(query);
    }
  };
  /*
    get Companions
  */
  getCompanions(infiniteScroll?: any){
    this.userService.getCompanions(this.navParams.get('userid'), this.itemcount, this.cPagenumber).then((res: Array<any>) => {
      if(res.length > 0){
        res.forEach((_item) => {
          if(_item.pictureurl== null || _item.pictureurl == 'http://test.drift.group/storage'){
            _item.pictureurl = 'assets/img/malethumb.png';
          }
          this.companions.push(_item);
        });
        this.cPagenumber++;
        if(infiniteScroll)
          infiniteScroll.complete();
      }else{
        this.noMorePeople = true;
        if(this.companions.length == 0){
          this.noCompanion = true;
        }else{
          this.noCompanion = false;
        }
      }
      if(!infiniteScroll){
        this.loading.dismiss();
      }
    }, (err) => {
      if(!infiniteScroll){
        this.loading.dismiss();
      }
      this.noMorePeople = true;
    });
  };
  /*
    search people with query
  */
  searchPeople(query, infiniteScroll?: any) {
    this.userService.searchPeople(query, this.itemcount, this.sPagenumber).then((res: Array<any>) => {
      if(res.length > 0){
        res.forEach((_item) => {
          if(_item.pictureurl== null || _item.pictureurl == 'http://test.drift.group/storage'){
            _item.pictureurl = 'assets/img/malethumb.png';
          }
        });
        res = res.filter((_item) => {
          return this.auth.getUserInfo().id != _item.user_id;
        });
        this.companions = this.companions.concat(res);
        this.sPagenumber++;
        if(infiniteScroll)
          infiniteScroll.complete();
      }else{
        this.noMorePeople = true;
      }
      if(!infiniteScroll){
        this.loading.dismiss();
      }
    }, (err) => {
      this.noMorePeople = true;
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click other user
  */
  goUser(id){
    this.navCtrl.push('UserProfilePage', {userid: id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'accept' button
  */
  goAccept(_request, _index){
    this.showLoading();
    this.util.acceptCompanionRequest(_request.id).then((res)=> {
      this.auth.getUser();
      this.requests.splice(_index, 1);
      this.cPagenumber = 0;
      this.sPagenumber = 0;
      this.companions = [];
      this.noMorePeople = false;
      this.noCompanion = false;
      this.getCompanions();
    }, (err)=> {
      this.loading.dismiss();
      console.log(err);
    });
  };
  /*
    Called when user click 'deny' button
  */
  goDeny(_request, _index){
    this.showLoading();
    this.util.rejectCompanionRequest(_request.id).then((res) => {
      this.auth.getUser().then((res)=> {
        this.loading.dismiss();
      });
      this.requests.splice(_index, 1);
      console.log(res);
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  };
}
