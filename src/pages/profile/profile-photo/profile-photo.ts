import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from '../../../providers/utilService';
import { AuthService } from '../../../providers/authService';

/**
 * Generated class for the ProfilePhotoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-photo',
  templateUrl: 'profile-photo.html',
})
export class ProfilePhotoPage {
  loading:Loading;
  photos: Array<any> = [];
  nophoto: boolean = false;//shows whether this journey has photo or not
  pagenumber: number = 0;
  itemcount: number = 5;
  noMorePhoto: boolean = false;
  myphoto: boolean = true;//shows whether this photos is current user's or not
  transparam: any;//translate parameter
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private util: UtilService,
    private auth: AuthService,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService) {
      this.showLoading();
      this.photos = [];
      this.noMorePhoto = false;
      this.nophoto = false;
      this.pagenumber = 0;
      if(this.auth.getUserInfo().id !== this.navParams.get('userid')){
        this.myphoto = false;
        this.transparam = {
          username: this.navParams.get('username')
        };
      }
      this.getPhotos();
  }
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    get photos
  */
  getPhotos(infiniteScroll?: any) {
    this.util.getDriftPhotos(this.navParams.get('driftid'), this.itemcount, this.pagenumber).then((res: Array<any>) => {
      if(res.length > 0){
        this.photos = this.photos.concat(res);
        this.pagenumber++;
        if(infiniteScroll)
          infiniteScroll.complete();
      }else{
        this.noMorePhoto = true;
        if(this.photos.length == 0){
          this.nophoto = true;
        }else{
          this.nophoto = false;
        }
      }
      if(!infiniteScroll){
        this.loading.dismiss();
      }
    }, (err) => {
      if(!infiniteScroll){
        this.loading.dismiss();
      }
      this.noMorePhoto = true;
      console.log(err);
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
