import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfilePhotoPage } from './profile-photo';

@NgModule({
  declarations: [
    ProfilePhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePhotoPage),
    TranslateModule.forChild()
  ],
})
export class ProfilePhotoPageModule {}
