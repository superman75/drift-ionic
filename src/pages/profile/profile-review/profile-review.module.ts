import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileReviewPage } from './profile-review';

@NgModule({
  declarations: [
    ProfileReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileReviewPage),
    TranslateModule.forChild()
  ],
})
export class ProfileReviewPageModule {}
