import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { AuthService } from '../../../providers/authService';

/**
 * Generated class for the ProfileReviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-review',
  templateUrl: 'profile-review.html',
})
export class ProfileReviewPage {
  loading:Loading;
  reviews = [];
  upvotes: number = 0;
  downvotes: number = 0;
  pagenumber: number = 0;
  itemcount: number = 10;
  myreview: boolean = true;//shows whether this review is current user's or not
  noMoreReview: boolean = false;
  transparam;//translate parameter
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private userSerivce: UserService,
    private auth: AuthService,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService) {
      this.showLoading();
      if(this.auth.getUserInfo().id !== this.navParams.get('userid')){
        this.myreview = false;
        this.transparam = {
          username: this.navParams.get('username')
        };
      }
      this.upvotes = this.navParams.get('upvotes');
      this.downvotes = this.navParams.get('downvotes');
      this.reviews = [];
      this.pagenumber = 0;
      this.getReviews();
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    get reviews
  */
  getReviews(infiniteScroll?: any) {
    this.userSerivce.getReviews(this.navParams.get('userid'), this.itemcount, this.pagenumber).then((res: Array<any>) => {
      if(res.length > 0){
        res.forEach((_review) => {
          var revieTime = _review.datetime.replace(/-/g, "/");
          _review.time = revieTime;
        });
        this.reviews = this.reviews.concat(res);
        this.pagenumber++;
        if(infiniteScroll)
          infiniteScroll.complete();
      }else{
        this.noMoreReview = true;
      }
      if(!infiniteScroll){
        this.loading.dismiss();
      }
    }, (err) => {
      if(!infiniteScroll){
        this.loading.dismiss();
      }
      console.log(err);
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
