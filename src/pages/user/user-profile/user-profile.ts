import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { BadgeService } from '../../../providers/badgeService';
import { Perks } from '../../../models/perks.model';

/**
 * Generated class for the UserProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  loading: Loading;
  journeyplaceholderpicture: string = 'assets/img/journey-thumb.jpg';
  perks = [];//user perks array
  userImages = [];//user profile images to show slide
  commonFriends = [];//user companions array
  reviews=[];//user reviews array
  userInfo: any = {};//user information
  transparam;//translate parameter
  showPerksMore: boolean = false;
  showReviewsMore: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private userService: UserService,
    private loadingCtrl: LoadingController, 
    private translateService: TranslateService,
    private badge: BadgeService,
    private toastCtrl: ToastController) {
      this.showLoading();
      this.userService.getUserInfo(this.navParams.get('userid')).then((res) => {
        this.userInfo = res;
        this.transparam = {
          username: this.userInfo.nickname
        };
        this.perks = Perks.getList(false, this.userInfo.perks);
        if(this.perks.length > 4){//if perks count is more than 4 show 'more...' button
          this.showPerksMore = true;
        }
        //prepare slide images first is user profile image, last is user vehicle image
        if(this.userInfo.pictureurl && this.userInfo.pictureurl != 'http://test.drift.group/storage'){
          this.userImages.push({id: 'profile', url: this.userInfo.pictureurl});
        }
        if(this.userInfo.pictures.length > 0){
          this.userImages = this.userImages.concat(this.userInfo.pictures);
        }
        if(this.userInfo.vehicle){
          if(this.userInfo.pictuerurl != null)
            this.userImages.push({id: 'vehicle', url: this.userInfo.vehicle.pictureurl});
        }
        //get journey placeholder picture
        if(this.userInfo.journeyplaceholderpicture){
          this.journeyplaceholderpicture = this.userInfo.journeyplaceholderpicture;
        }
        //prepare reviews
        if((this.userInfo.upvotes + this.userInfo.downvotes) > 2){
          this.showReviewsMore = true;
        }
        if(this.userInfo.reviews){
          this.reviews = this.userInfo.reviews;
        }
        //get friends list
        this.userService.getCompanions(this.userInfo.id).then((res: Array<any>) => {
          res.forEach((_companion)=> {
            if(!_companion.pictureurl || _companion.pictureurl == 'http://test.drift.group/storage'){
              _companion.pictureurl = 'assets/img/malethumb.png';
            }
            this.commonFriends.push(_companion);
          });
          this.loading.dismiss();
        }, (err)=> {
          this.loading.dismiss();
        });
      }, (err) => {
        this.loading.dismiss();
        console.log(err);
      });
  };
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show toast message
  */
  showToast(content) {
    let toast = this.toastCtrl.create({
      message: content,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter() {
    if(localStorage.getItem('drift_driftrequested') == 'true'){
      this.userInfo.driftRequested = true;
      localStorage.removeItem('drift_driftrequested');
    }
  }
  /*
    called when user click 'more...' button in perks area
  */
  clickPerksMore(){
    this.showPerksMore = false;
  }
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
  /*
    Called when user click 'journeys' image
  */
  goJourneys() {
    this.navCtrl.push('ProfileJourneyPage', {userid: this.userInfo.id, username: this.userInfo.nickname}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'read more...' button
  */
  goReviews() {
    this.navCtrl.push('ProfileReviewPage', {userid: this.userInfo.id, username: this.userInfo.nickname, upvotes: this.userInfo.upvotes, downvotes: this.userInfo.downvotes}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'companions' arrow
  */
  goCompanions() {
    this.navCtrl.push('ProfileCompanionsPage', {userid: this.userInfo.id, username: this.userInfo.nickname}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click other user
  */
  goUser(user_id){
    this.navCtrl.push('UserProfilePage', {userid: user_id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'phone' icon
  */
  callPhone() {

  }
  /*
    Called when user click 'mail' icon
  */
  sendMessage() {
    this.navCtrl.push('UserMessagePage', {user: this.userInfo}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'showDrift' button
  */
  showDrift() {
    this.navCtrl.push('UserDriftPage', { from: 'user-profile', user: this.userInfo}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
  }
  /*
    Called when user click 'user plus' icon
  */
  requestCompanion() {
    if(this.userInfo.isCompanion == false){
      this.userService.sendCompanionRequest(this.userInfo.id).then((res) => {
        let toastMessage = this.translateService.instant('Toast.CompanionRequestSuccess');
        this.showToast(toastMessage);
      }, (err) => {
        this.showToast(err);
      });
    }
  }
  /*
    Called when user click 'user block' icon
  */
  blockCompanion() {
    if(this.userInfo.isBlocked == false){
      this.userService.blockCompanion(this.userInfo.id).then((res) => {
        this.userInfo.isBlocked = true;
        let toastMessage = this.translateService.instant('Toast.BlockCompanionSuccess');
        this.showToast(toastMessage);
      }, (err) => {
        console.log(err);
      });
    }else{
      this.userService.unblockCompanion(this.userInfo.id).then((res) => {
        this.userInfo.isBlocked = false;
        let toastMessage = this.translateService.instant('Toast.UnBlockCompanionSuccess');
        this.showToast(toastMessage);
      }, (err) => {
        console.log(err);
      });
    }
  }
}
