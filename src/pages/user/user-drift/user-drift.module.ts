import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { UserDriftPage } from './user-drift';

@NgModule({
  declarations: [
    UserDriftPage,
  ],
  imports: [
    IonicPageModule.forChild(UserDriftPage),
    TranslateModule.forChild()
  ],
})
export class UserDriftPageModule {}
