import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { mapboxStyle, mapboxZoom, routeColor, otherRouteColor } from '../../../app/app.config';
import { TranslateService } from '@ngx-translate/core';

import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
import { AuthService } from '../../../providers/authService';
import { UtilService } from '../../../providers/utilService';
import { BadgeService } from '../../../providers/badgeService';
/**
 * Generated class for the UserDriftPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-drift',
  templateUrl: 'user-drift.html',
})
export class UserDriftPage {
  loading: Loading;
  userInfo: any = {};//user information
  otherInfo: any = {};//other user information
  from: string = '';
  userImage: string = '';
  sameMode: boolean = false;
  betweenDistance: number = 0;
  journeyDistance: number = 0;
  drifting: boolean = false;
  mapContainer = {
    map: null,
    currentMarker: null,
    destinationMarker: null,
    otherMarker: null,
    otherDestinationMarker: null,
    idCurrentRoute: 0,
    idOtherRoute: 1
  };
  mapId: number;//current page mapid
  showingAlert: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private loadingCtrl: LoadingController, 
    private translateService: TranslateService,
    private mapbox: MapboxService,
    private location: LocationService,
    private auth: AuthService,
    private alertCtrl: AlertController,
    private util: UtilService,
    private badge: BadgeService) {
      this.userInfo = this.auth.getUserInfo();
      this.from = this.navParams.get('from');
      this.otherInfo = this.navParams.get('user');
      if(this.otherInfo.pictureurl && this.otherInfo.pictureurl != 'http://test.drift.group/storage'){
        this.userImage =  this.otherInfo.pictureurl;
      }else{
        this.userImage = 'assets/img/malethumb.png';
      }
      if(this.auth.getUserInfo().mode == this.otherInfo.mode){
        this.sameMode = true;
      }
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('user_drift_map');
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
  */
  showAlert(content){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {}  
        }
      ]
    });
    alert.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      destinationMarker: null,
      otherMarker: null,
      otherDestinationMarker: null,
      idCurrentRoute: 0,
      idOtherRoute: 1
    };
    //check whether gps is available or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage);
    });
    this.showMap();
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('user_drift_map');
  }
  /*
    show map in the page
  */
  showMap() {
    let mapOptions = {
      container: 'user_drift_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: mapboxZoom,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      destinationlat: this.userInfo.destinationlat,
      destinationlon: this.userInfo.destinationlon,
      routeColor: routeColor,
      otherlat: this.otherInfo.lat,
      otherlon: this.otherInfo.lon,
      otherdestinationlat: this.otherInfo.destinationlat,
      otherdestinationlon: this.otherInfo.destinationlon,
      otherRouteColor: otherRouteColor
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null) {
        if(this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap();
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        this.betweenDistance = (this.mapbox.distanceBetween([dataOptions.currentlon, dataOptions.currentlat], [parseFloat(dataOptions.otherlon), parseFloat(dataOptions.otherlat)])).toFixed(1);
        this.mapbox.showDriftersRouteMap(this.mapContainer, mapOptions, dataOptions).then((res: any) =>{
          this.loading.dismiss();
          this.journeyDistance = parseFloat((res.distance/1000).toFixed(1));
        }, (err) => {
          this.loading.dismiss();
          console.log(err);
        });
      }
    });
  }
  /*
    called when user click 'request drift' button
  */
  requestDrift() {
    this.showLoading();
    this.util.sendDriftRequest(this.otherInfo.id).then((res) => {
      this.loading.dismiss();
      localStorage.setItem('drift_driftrequested', 'true');
      this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  }
  /*
    Called when user click 'accept' button
  */
  acceptRequest() {
    this.showLoading();
    let request = this.navParams.get('request');
    if(this.auth.getUserInfo().mode == 'Driver'){
      this.util.acceptDriftRequest(request.id).then((res) => {
        this.auth.getUser().then((res) => {
          this.loading.dismiss();
          this.navCtrl.push('DriverUserDestinationPage', {userid: this.otherInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
            let index = this.navCtrl.getActive().index;
            this.navCtrl.remove(index-1);
          });
        });
      }, (err) => {
        this.loading.dismiss();
        console.log(err);
      });
    }else{
      this.auth.getDrifts().then((res: any) => {
        this.loading.dismiss();
        if(res.agreed.length > 0 || res.running.length > 0){
          if(res.agreed.length > 0){
            let alertMessage = this.translateService.instant('Alert.Message.CouldNotAcceptHaveAgreed');
            this.showAlert(alertMessage);
          }
          if(res.running.length > 0){
            let alertMessage = this.translateService.instant('Alert.Message.CouldNotAcceptHaveRunning');
            this.showAlert(alertMessage);
          }
        }else{
          this.util.acceptDriftRequest(request.id).then((res) => {
            this.auth.getUser().then((res) => {
              this.navCtrl.push('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
                let index = this.navCtrl.getActive().index;
                this.navCtrl.remove(index-1);
              });
            });
          }, (err) => {
            console.log(err);
          })
        }
      }, (err)=> {
        this.loading.dismiss();
        console.log(err);
      });
    }
  }
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
}
