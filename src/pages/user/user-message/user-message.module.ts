import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { UserMessagePage } from './user-message';

@NgModule({
  declarations: [
    UserMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(UserMessagePage),
    TranslateModule.forChild()
  ]
})
export class UserMessagePageModule {}
