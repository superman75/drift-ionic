import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, ToastController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { BadgeService } from '../../../providers/badgeService';

/**
 * Generated class for the UserMessagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-message',
  templateUrl: 'user-message.html',
})
export class UserMessagePage {
  loading: Loading;
  user: any;//user to send message
  message: string = '';//message content
  userImage: string = ''//user's image
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private userService: UserService,
    private toastCtrl: ToastController,
    private translateService: TranslateService,
    private loadingCtrl: LoadingController,
    private badge: BadgeService,
    private alertCtrl: AlertController) {
      this.user = this.navParams.get('user');
      if(this.user.pictureurl && this.user.pictureurl != 'http://test.drift.group/storage'){
        this.userImage =  this.user.pictureurl;
      }else{
        this.userImage = 'assets/img/malethumb.png';
      }
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show toast message
  */
  showToast(content) {
    let toast = this.toastCtrl.create({
      message: content,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
  */
  showAlert(content){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {
          }  
        }
      ]
    });
    alert.present();
  }
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
  /*
    Called when user click 'send' button
  */
  send() {
    if(this.message != ''){
      this.showLoading();
      this.userService.sendMessage(this.user.id, this.message).then((res) => {
        this.loading.dismiss();
        let toastMessage = this.translateService.instant('Toast.SendMessageSuccess');
        this.showToast(toastMessage);
      }, (err) => {
        this.loading.dismiss();
        console.log(err);
      });
    }else{
      let alertMessage = this.translateService.instant('Alert.Message.TypeMessage');
      this.showAlert(alertMessage);
    }
  }
}
