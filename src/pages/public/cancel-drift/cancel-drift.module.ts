import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CancelDriftPage } from './cancel-drift';

@NgModule({
  declarations: [
    CancelDriftPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelDriftPage),
    TranslateModule.forChild()
  ],
})
export class CancelDriftPageModule {}
