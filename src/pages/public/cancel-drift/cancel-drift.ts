import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from '../../../providers/utilService';

/**
 * Generated class for the CancelDriftPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cancel-drift',
  templateUrl: 'cancel-drift.html',
})
export class CancelDriftPage {
  loading: Loading;
  drift: any;
  transparam: any;
  messageCount: number = 0;
  message: string = '';
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private util: UtilService,
    private translateService: TranslateService,
    private loadingCtrl: LoadingController) {
      this.drift = this.navParams.get('drift');
      this.transparam = {
        username: this.drift.nickname
      };
  };
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    Called when user change textarea content
  */
  changeMessage() {
    var numberOfLineBreaks = (this.message.match(/\n/g) || []).length;
    this.messageCount = this.message.length + numberOfLineBreaks;
  };
  /*
    Called when user click 'send' button
  */
  cancelDrift() {
    this.showLoading();
    this.util.cancelDrift(this.drift.id, this.message).then((res) => {
      this.loading.dismiss();
      this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err) => {
      this.loading.dismiss();
      console.log('cancel drift error...');
    });
  };
  /*
    Called when user click 'backarrow'
  */
  goBack() {
    console.log(this.navCtrl.getViews());
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
