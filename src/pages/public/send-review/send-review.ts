import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from '../../../providers/utilService';
/**
 * Generated class for the SendReviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-send-review',
  templateUrl: 'send-review.html',
})
export class SendReviewPage {
  loading: Loading;
  drift: any;
  reviewCount: number = 0;
  reviewText: string = '';
  activeVote: string = 'up';
  upVoteImage: string = 'assets/img/uparrow-active.png';
  downVoteImage: string = "assets/img/downarrow.png";
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private util: UtilService,
    private translateService: TranslateService,
    private loadingCtrl: LoadingController) {
      this.drift = this.navParams.get('drift');
  }
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    Called when user change textarea content
  */
  changeReview() {
    var numberOfLineBreaks = (this.reviewText.match(/\n/g) || []).length;
    this.reviewCount = this.reviewText.length + numberOfLineBreaks;
  };
  /*
    Called when user click 'send' button
  */
  sendReview() {
    this.showLoading();
    this.util.sendReview(this.drift.id, this.activeVote, this.reviewText).then((res) => {
      this.loading.dismiss();
      this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err) => {
      this.loading.dismiss();
      console.log('send review error...');
    });
  };
  /*
    Called when user click uparrow
  */
  upVote() {
    if(this.activeVote == 'down'){
      this.activeVote = 'up';
      this.upVoteImage = 'assets/img/uparrow-active.png';
      this.downVoteImage = 'assets/img/downarrow.png';
    }
  };
  /*
    Called when user click downarrow
  */
  downVote() {
    if(this.activeVote == 'up'){
      this.activeVote = 'down';
      this.upVoteImage = 'assets/img/uparrow.png';
      this.downVoteImage = 'assets/img/downarrow-active.png';
    }
  };
  /*
    Called when user click 'backarrow'
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
