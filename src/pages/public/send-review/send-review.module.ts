import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SendReviewPage } from './send-review';

@NgModule({
  declarations: [
    SendReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(SendReviewPage),
    TranslateModule.forChild()
  ],
})
export class SendReviewPageModule {}
