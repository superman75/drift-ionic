import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EndNavigationPage } from './end-navigation';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EndNavigationPage,
  ],
  imports: [
    IonicPageModule.forChild(EndNavigationPage),
    TranslateModule.forChild()
  ],
})
export class EndNavigationPageModule {}
