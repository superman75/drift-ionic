import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EndNavigationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-end-navigation',
  templateUrl: 'end-navigation.html',
})
export class EndNavigationPage {

  constructor(
    public viewCtrl: ViewController, 
    public navParams: NavParams) {
  }

  /*
    Called when user click 'yes, i do' button
  */
  endNavigation() {
    this.viewCtrl.dismiss({mode: 'end'});
  };
  /*
    Called when user click 'no, continue' button
  */
  goBack() {
    this.viewCtrl.dismiss({mode: 'continue'});
  };

}
