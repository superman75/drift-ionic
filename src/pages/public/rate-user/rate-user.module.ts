import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { RateUserPage } from './rate-user';

@NgModule({
  declarations: [
    RateUserPage,
  ],
  imports: [
    IonicPageModule.forChild(RateUserPage),
    TranslateModule.forChild()
  ],
})
export class RateUserPageModule {}
