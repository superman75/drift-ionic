import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RateUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rate-user',
  templateUrl: 'rate-user.html',
})
export class RateUserPage {
  drift;
  pictureurl: string = 'assets/img/malethumb.png';
  transparam;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams) {
      this.drift = this.navParams.get('drift');
      this.transparam = {
        username: this.drift.nickname
      };
      this.pictureurl = this.drift.pictureurl;
  };

  /*
    Called when user click 'rate now' button
  */
  rateNow() {
    this.navCtrl.push('SendReviewPage', {drift: this.drift}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'rate later' button
  */
  rateLater() {
    this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
}
