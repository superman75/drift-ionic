import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the DriftMatchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drift-match',
  templateUrl: 'drift-match.html',
})
export class DriftMatchPage {
  userInfo: any;
  otherInfo: any;
  timeoutInstance: any;
  pictureurl: string = 'assets/img/malethumb.png';
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private auth: AuthService) {
      this.userInfo = this.auth.getUserInfo();
      this.otherInfo = this.navParams.get('user');
      if(this.otherInfo.pictureurl && this.otherInfo.pictureurl != 'http://test.drift.group/storage'){
        this.pictureurl = this.otherInfo.pictureurl;
      }
  };
  /*
    Runs when the page has fully entered and is now the active page
    This event will fire, whether it was the first load or a chached page.
  */
  ionViewDidEnter() {
    this.timeoutInstance = setTimeout(() => {
      if(this.navCtrl.getActive().name != 'DriverUserDestinationPage' && this.navCtrl.getActive().name != 'DrifterGoingMapPage'){
        if(this.userInfo.mode == 'Driver'){
          this.navCtrl.push('DriverUserDestinationPage', {userid: this.otherInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
            let index = this.navCtrl.getActive().index;
            this.navCtrl.remove(index-1);
          });
        }else{
          this.navCtrl.push('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
            let index = this.navCtrl.getActive().index;
            this.navCtrl.remove(index-1);
          });
        }
      }else{
        this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }
    }, 5000);
  };
  /*
    Called when user click screen
  */
  goBack() {
    clearTimeout(this.timeoutInstance);
    if(this.navCtrl.getActive().name != 'DriverUserDestinationPage' && this.navCtrl.getActive().name != 'DrifterGoingMapPage'){
      if(this.userInfo.mode == 'Driver'){
        this.navCtrl.push('DriverUserDestinationPage', {userid: this.otherInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
          let index = this.navCtrl.getActive().index;
          this.navCtrl.remove(index-1);
        });
      }else{
        this.navCtrl.push('DrifterGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
          let index = this.navCtrl.getActive().index;
          this.navCtrl.remove(index-1);
        });
      }
    }else{
      this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }
  };

}
