import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DriftMatchPage } from './drift-match';

@NgModule({
  declarations: [
    DriftMatchPage,
  ],
  imports: [
    IonicPageModule.forChild(DriftMatchPage),
    TranslateModule.forChild()
  ],
})
export class DriftMatchPageModule {}
