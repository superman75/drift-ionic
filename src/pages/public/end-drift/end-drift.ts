import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from '../../../providers/utilService';
/**
 * Generated class for the EndDriftPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-end-drift',
  templateUrl: 'end-drift.html',
})
export class EndDriftPage {
  drift:any;
  loading: Loading;
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private util: UtilService,
    private translateService: TranslateService,
    private loadingCtrl: LoadingController) {
      this.drift = this.navParams.get('drift');
  };
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    Called when user click 'yes, i do' button
  */
  endDrift() {
    this.showLoading();
    this.util.endDrift(this.drift.id).then((res) => {
      this.loading.dismiss();
      if(this.drift.otheruser_id != null){
        this.navCtrl.push('RateUserPage', {drift: this.drift}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
          let index = this.navCtrl.getActive().index;
          this.navCtrl.remove(index-1);
        });
      }else{
        this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
    });
  };
  /*
    Called when user click 'no, continue' button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
