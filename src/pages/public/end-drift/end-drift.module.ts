import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EndDriftPage } from './end-drift';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EndDriftPage,
  ],
  imports: [
    IonicPageModule.forChild(EndDriftPage),
    TranslateModule.forChild()
  ],
})
export class EndDriftPageModule {}
