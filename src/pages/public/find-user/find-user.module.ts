import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { FindUserPage } from './find-user';

@NgModule({
  declarations: [
    FindUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FindUserPage),
    TranslateModule.forChild()
  ],
})
export class FindUserPageModule {}
