import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { UtilService } from '../../../providers/utilService';

/**
 * Generated class for the FindUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-find-user',
  templateUrl: 'find-user.html',
})
export class FindUserPage {
  loading: Loading;
  userInfo: any;
  pictureurl: string = 'assets/img/malethumb.png';
  transparam: any;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private userService: UserService,
    private translateService: TranslateService,
    private loadingCtrl: LoadingController,
    private util: UtilService) {
      this.showLoading();
      this.userService.getUserInfo(this.navParams.get('userid')).then((res) => {
        this.loading.dismiss();
        this.userInfo = res;
        this.transparam = {
          username: this.userInfo.nickname,
          usergender: 'him'
        }
        if(this.userInfo.pictureurl && this.userInfo.pictureurl != 'http://test.drift.group/storage'){
          this.pictureurl = this.userInfo.pictureurl;
        }
        if(this.userInfo.gender == 'female'){
          this.transparam.usergender = 'her';
        }
      });
  }
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    Called when user click 'yes'
  */
  yes() {
    this.showLoading();
    this.util.startDrift(this.navParams.get('driftid')).then((res) => {
      this.loading.dismiss();
      if(this.navParams.get('from') == 'destination' && localStorage.getItem('drift_destination_user') == this.navParams.get('userid')){
        localStorage.removeItem('drift_destination_user');
        this.navCtrl.push('DriverGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'}).then((res) => {
          let index = this.navCtrl.getActive().index;
          this.navCtrl.remove(index-1);
        });
      }else{
        this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }
    }, (err)=>{
      this.loading.dismiss();
      this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    });    
  };
  /*
    Called when user click 'not yet'
  */
  notYet() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'continue without'
  */
  continueWithout() {
    this.showLoading();
    this.util.cancelDrift(this.navParams.get('driftid'), 'continue without you').then((res) => {
      this.loading.dismiss();
      this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err)=> {
      this.loading.dismiss();
    });
  };
}
