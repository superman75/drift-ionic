import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { EmergencyChoosePage } from './emergency-choose';

@NgModule({
  declarations: [
    EmergencyChoosePage,
  ],
  imports: [
    IonicPageModule.forChild(EmergencyChoosePage),
    TranslateModule.forChild()
  ],
})
export class EmergencyChoosePageModule {}
