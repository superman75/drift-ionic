import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../../../providers/userService';
import { AuthService } from '../../../providers/authService';

/**
 * Generated class for the EmergencyChoosePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-emergency-choose',
  templateUrl: 'emergency-choose.html',
})
export class EmergencyChoosePage {
  loading: Loading;
  companions: Array<any>=[];
  requests: Array<any>=[];
  query: string = '';
  noCompanion: boolean = false;
  initSearch: boolean = false;//user start search or not
  itemcount: number = 20;
  cPagenumber: number = 0;
  sPagenumber: number = 0;
  noMorePeople: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private auth: AuthService,
    private userService: UserService,
    private translateService:TranslateService,
    private loadingCtrl: LoadingController) {
      this.showLoading();
      this.companions = [];
      this.requests = [];
      this.noCompanion = false;
      this.auth.getCompanionRequests().then((res: Array<any>)=> {
        res.forEach((_request)=> {
          this.userService.getUserInfo(_request.otheruser_id).then((_user: any)=> {
            if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
              _request.pictureurl = 'assets/img/malethumb.png';
            }else{
              _request.pictureurl = _user.pictureurl;
            }
            _request.nickname = _user.nickname;
            this.requests.push(_request);
          });
        });
      },(err) => {
        console.log(err);
      });
      this.getCompanions();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    search people
  */
  search(query: string, mode: string, infiniteScroll?: any){
    if (mode == 'more') {
      if (this.initSearch == false) {
        this.getCompanions(infiniteScroll);
      } else {
        this.searchPeople(query, infiniteScroll);
      }
    } else {
      this.initSearch = true;
      this.cPagenumber = 0;
      this.sPagenumber = 0;
      this.companions = [];
      this.showLoading();
      this.noMorePeople = false;
      this.searchPeople(query);
    }
  };
  /*
    get Companions
  */
  getCompanions(infiniteScroll?: any){
    this.userService.getCompanions(this.auth.getUserInfo().id, this.itemcount, this.cPagenumber).then((res: Array<any>) => {
      if(res.length > 0){
        res.forEach((_item) => {
          if(_item.pictureurl== null || _item.pictureurl == 'http://test.drift.group/storage'){
            _item.pictureurl = 'assets/img/malethumb.png';
          }
        });
        this.companions = this.companions.concat(res);
        if(this.companions.length == 0){
          this.noCompanion = true;
        }else{
          this.noCompanion = false;
        }
        this.cPagenumber++;
        if(infiniteScroll)
          infiniteScroll.complete();
      }else{
        this.noMorePeople = true;
        if(this.companions.length == 0){
          this.noCompanion = true;
        }else{
          this.noCompanion = false;
        }
      }
      if(!infiniteScroll){
        this.loading.dismiss();
      }
    }, (err) => {
      this.noMorePeople = true;
    });
  };
  /*
    search people with query
  */
  searchPeople(query: string, infiniteScroll?: any) {
    this.userService.searchPeople(query, this.itemcount, this.sPagenumber).then((res: Array<any>) => {
      if(res.length > 0){
        res.forEach((_item) => {
          if(_item.pictureurl== null || _item.pictureurl == 'http://test.drift.group/storage'){
            _item.pictureurl = 'assets/img/malethumb.png';
          }
        });
        res = res.filter((_item) => {
          return this.auth.getUserInfo().id != _item.user_id;
        });
        this.companions = this.companions.concat(res);
        this.sPagenumber++;
        if(infiniteScroll)
          infiniteScroll.complete();
      }else{
        this.noMorePeople = true;
      }
      if(!infiniteScroll){
        this.loading.dismiss();
      }
    }, (err) => {
      this.noMorePeople = true;
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click other user
  */
  chooseUser(_user: any){
    let selectedEmergency = {
      number: this.navParams.get('number'),
      id: _user.user_id,
      pictureurl: _user.pictureurl,
      nickname: _user.nickname
    };
    localStorage.setItem('drift_emergency', JSON.stringify(selectedEmergency));
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };

}
