import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, ToastController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
import { UserService } from '../../../providers/userService';
/**
 * Generated class for the EmergencyContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-emergency-contact',
  templateUrl: 'emergency-contact.html',
})
export class EmergencyContactPage {
  loading: Loading;
  userInfo: any;
  emergency1: any;
  emergency2: any;
  constructor(
    private navCtrl: NavController, 
    private loadingCtrl: LoadingController,
    private userService: UserService,
    private auth: AuthService,
    private translateService: TranslateService,
    private toastCtrl: ToastController) {
    this.userInfo = this.auth.getUserInfo();
    this.emergency1 = {
      id: null,
      pictureurl: "assets/img/user-plus-disable.png",
      nickname: null
    };
    this.emergency2 = {
      id: null,
      pictureurl: "assets/img/user-plus-disable.png",
      nickname: null
    };
    //get the user info of emergencycontact1 if exist
    if(this.userInfo.emergencycontact1 != null){
      this.userService.getUserInfo(this.userInfo.emergencycontact1).then((_user: any)=> {
        this.emergency1.id = this.userInfo.emergencycontact1;
        if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
          this.emergency1.pictureurl = 'assets/img/malethumb.png';
        }else{
          this.emergency1.pictureurl = _user.pictureurl;
        }
        this.emergency1.nickname = _user.nickname;
      });
    }
    //get the user info of emergencycontact2 if exist
    if(this.userInfo.emergencycontact2 != null){
      this.userService.getUserInfo(this.userInfo.emergencycontact2).then((_user: any)=> {
        this.emergency2.id = this.userInfo.emergencycontact2;
        if(_user.pictureurl== null || _user.pictureurl == 'http://test.drift.group/storage'){
          this.emergency2.pictureurl = 'assets/img/malethumb.png';
        }else{
          this.emergency2.pictureurl = _user.pictureurl;
        }
        this.emergency2.nickname = _user.nickname;
      });
    }
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    show toast message
  */
  showToast(content) {
    let toast = this.toastCtrl.create({
      message: content,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  };
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter() {
    if(localStorage.getItem('drift_emergency') != null){
      let emergency = JSON.parse(localStorage.getItem('drift_emergency'));
      if(emergency.number == 1){
        this.emergency1.id = emergency.id;
        this.emergency1.pictureurl = emergency.pictureurl;
        this.emergency1.nickname = emergency.nickname;
      }else{
        this.emergency2.id = emergency.id;
        this.emergency2.pictureurl = emergency.pictureurl;
        this.emergency2.nickname = emergency.nickname;
      }
      localStorage.removeItem('drift_emergency');
    }
  };
  /*
    when user click user icon to choose emergency contact
  */
  choose(number){
    this.navCtrl.push('EmergencyChoosePage', {number: number}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click 'noted' button
  */
  noted() {
    this.showLoading();
    this.auth.updateEmergency(this.emergency1.id, this.emergency2.id).then((res) => {
      this.loading.dismiss();
      let toastMessage = this.translateService.instant('Toast.Emergency');
      this.showToast(toastMessage);
      this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
    }, (err)=> {
      this.loading.dismiss();
      console.log(err);
    });
  };
}
