import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { EmergencyContactPage } from './emergency-contact';

@NgModule({
  declarations: [
    EmergencyContactPage,
  ],
  imports: [
    IonicPageModule.forChild(EmergencyContactPage),
    TranslateModule.forChild()
  ]
})
export class EmergencyContactPageModule {}
