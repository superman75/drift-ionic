import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DriverNoDestinationPage } from './driver-no-destination';

@NgModule({
  declarations: [
    DriverNoDestinationPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverNoDestinationPage),
    TranslateModule.forChild()
  ],
})
export class DriverNoDestinationPageModule {}
