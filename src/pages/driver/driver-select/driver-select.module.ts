import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverSelectPage } from './driver-select';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DriverSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverSelectPage),
    TranslateModule.forChild()
  ],
})
export class DriverSelectPageModule {}
