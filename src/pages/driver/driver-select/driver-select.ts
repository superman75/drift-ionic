import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController } from 'ionic-angular';
import { mapboxStyle, mapboxZoom } from '../../../app/app.config';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
import { BadgeService } from '../../../providers/badgeService';

/**
 * Generated class for the DriverSelectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driver-select',
  templateUrl: 'driver-select.html',
})
export class DriverSelectPage {
  loading: Loading;
  mapContainer = {
    map: null,
    currentMarker: null,
    destinationMarker: null,
    geocoder: null,
    destination: null,
  };
  mapId: number;//current page mapid
  showingAlert: boolean = false;
  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    private mapbox: MapboxService,
    private badge: BadgeService,
    private location: LocationService, 
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private translateService: TranslateService) {
      this.auth.updateMode('driver').then((res) => {
        console.log('update mode to drifter...');
      }, (err) => {
        console.log('update mode error...');
      });
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('driver_select_map');
  };
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
  */
  showAlert(content: string){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {}  
        }
      ]
    });
    alert.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      destinationMarker: null,
      geocoder: null,
      destination: null,
    };
    //check whether gps is availalbe or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage);
    });
    this.showMap(false);
  }
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    this.mapContainer.map.remove();
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('driver_select_map');
  }
  /*
    show map in the page
  */
  showMap(_mapUpdate: boolean) {
    let mapOptions = {
      container: 'driver_select_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: mapboxZoom,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false,//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null) {
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showSelectMap(this.mapContainer, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //update intervals
            // this.updateUserInterval = setInterval(() => {
            //   this.showMap(true);
            // }, 5000);
          }, (err) => {
            this.loading.dismiss();
            console.log(err);
          });
        }else{
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showSelectMap(this.mapContainer, true, null, dataOptions).then((res) => {
            console.log('update current user...');
          });
        }
      }
    });
  }
  /*
    Called when user click 'submit' button
  */
  submit(){
    if(this.mapContainer.destination == null){
      let alertMessage = this.translateService.instant('Alert.Message.SelectDestination');
      this.showAlert(alertMessage);
    }else{
      this.showLoading();
      this.auth.updateDestination(this.mapContainer.destination.geometry.coordinates[0], this.mapContainer.destination.geometry.coordinates[1], this.mapContainer.destination.text).then((res) => {
        this.loading.dismiss();
        this.navCtrl.push('DriverMapPage', {destination: this.mapContainer.destination}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }, (err) => {
        this.loading.dismiss();
        console.log('update destination error...');
      });
    }
  }
  /*
    Called when user click 'Just Cruisen' button
  */
  justCruisen(){
    this.showLoading();
    this.auth.updateDestination(null, null, null).then((res) => {
      this.loading.dismiss();
      this.navCtrl.push('DriverNoDestinationPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err) => {
      this.loading.dismiss();
      console.log('update destination error...');
    });
  }
  /*
    Called when user click arrow button
  */
  goMain(){
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
}
