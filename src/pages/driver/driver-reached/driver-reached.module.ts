import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverReachedPage } from './driver-reached';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    DriverReachedPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverReachedPage),
    TranslateModule.forChild()
  ],
})
export class DriverReachedPageModule {}
