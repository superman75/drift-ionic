import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';

/**
 * Generated class for the DriverReachedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driver-reached',
  templateUrl: 'driver-reached.html',
})
export class DriverReachedPage {
  timeoutInstance:any;
  constructor(
    private navCtrl: NavController) {
  }
  /*
    Runs when the page has fully entered and is now the active page
    This event will fire, whether it was the first load or a chached page.
  */
  ionViewDidEnter() {
    this.timeoutInstance = setTimeout(() => {
      this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
    }, 5000);
  };
  /*
    Called when user click screen
  */
  goBack() {
    clearTimeout(this.timeoutInstance);
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}
