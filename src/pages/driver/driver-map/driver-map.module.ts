import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DriverMapPage } from './driver-map';

@NgModule({
  declarations: [
    DriverMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverMapPage),
    TranslateModule.forChild()
  ],
})
export class DriverMapPageModule {}
