import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { mapboxDistance, mapboxStyle, mapboxZoom, intervalTime, routeColor} from '../../../app/app.config';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
import { BadgeService } from '../../../providers/badgeService';

/**
 * Generated class for the DriverMapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driver-map',
  templateUrl: 'driver-map.html',
})
export class DriverMapPage {
  loading: Loading;
  mapContainer = {
    map: null,
    currentMarker: null,
    destinationMarker: null,
    userMarkerArray:[],
    driftpointMarkerArray:[],
    idRoute: 0
  };
  updateUserInterval: any;
  updateOthersInterval: any;
  destination: any;
  mapId: number;//current page mapid
  showingAlert: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private loadingCtrl: LoadingController, 
    private alertCtrl: AlertController, 
    private translateService: TranslateService, 
    private mapbox: MapboxService, 
    private location: LocationService,
    private badge: BadgeService) {
      let destinationParam = this.navParams.get('destination');
      this.destination = {
        text: destinationParam.text,
        lon: destinationParam.center[0],
        lat: destinationParam.center[1]
      };
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('driver_map');
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show Alert
  */
  showAlert(content: string){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {}  
        }
      ]
    });
    alert.present();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    //initialize map container
    this.mapContainer = {
      map: null,
      currentMarker: null,
      destinationMarker: null,
      userMarkerArray:[],
      driftpointMarkerArray:[],
      idRoute: 0
    };
    //check whether gps is available or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage);
    });
    //show map
    this.showMap(false, false, false);
  }
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    clearInterval(this.updateUserInterval);
    clearInterval(this.updateOthersInterval);
    this.mapContainer.map.remove();
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('driver_map');
  }
  /*
    show map in the page
  */
  showMap(_mapUpdate: boolean, _userUpdate: boolean, _otherUpdate: boolean) {
    let mapOptions = {
      container: 'driver_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      zoom: mapboxZoom,//map zoom level from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      trackResize: false//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      destinationlat: this.destination.lat,
      destinationlon: this.destination.lon,
      users: null,
      driftpoints: null,
      mode: 'Driver',
      routeColor: routeColor
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null){
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false, false, false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showDestinationMap(this.mapContainer, false, false, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //add other users to the map
            this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers)=> {
              dataOptions.users = resusers;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {});
            });
            //add driftpoints to the map
            this.location.getDriftPoints(cpos.latitude, cpos.longitude, mapboxDistance).then((resdrifts) => {
              dataOptions.driftpoints = resdrifts;
              this.mapbox.showDestinationMap(this.mapContainer, true, false, false, null, dataOptions).then((res) => {});
            });
            //update intervals
            this.updateUserInterval = setInterval(() => {
              this.showMap(true, true, false);
            }, 5000);
            this.updateOthersInterval = setInterval(() => {
              this.showMap(true, false, true);
            }, intervalTime);
          }, (err) => {
            this.loading.dismiss();
            console.log(err);
          });
        }else if(_userUpdate == true){
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showDestinationMap(this.mapContainer, true, true, false, null, dataOptions).then((res) => {
            // update current user
          });
        }else if(_otherUpdate == true){
          this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers) => {
            dataOptions.users = resusers;
            this.mapbox.showDestinationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {
              // update other users
            });          
          });
        }
      } 
    });
  }
  /*
    Called when user click arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }
  /*
    Called when user click 'start' button
  */
  start() {
    this.navCtrl.push('DriverGoingMapPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
}
