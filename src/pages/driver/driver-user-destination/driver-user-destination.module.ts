import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverUserDestinationPage } from './driver-user-destination';
import { TranslateModule } from '@ngx-translate/core';
import { Insomnia } from '@ionic-native/insomnia';

@NgModule({
  declarations: [
    DriverUserDestinationPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverUserDestinationPage),
    TranslateModule.forChild()
  ],
  providers: [
    Insomnia
  ]
})
export class DriverUserDestinationPageModule {}
