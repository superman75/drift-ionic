import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverGoingMapPage } from './driver-going-map';
import { Insomnia } from '@ionic-native/insomnia';

@NgModule({
  declarations: [
    DriverGoingMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverGoingMapPage),
  ],
  providers: [
    Insomnia
  ]
})
export class DriverGoingMapPageModule {}
