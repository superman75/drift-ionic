import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, Loading, ModalController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Insomnia } from '@ionic-native/insomnia';

import { mapboxCenter, mapboxDistance, mapboxStyle, intervalTime, routeColor} from '../../../app/app.config';
import { MapboxService } from '../../../providers/mapboxService';
import { LocationService } from '../../../providers/locationService';
import { UtilService } from '../../../providers/utilService';
import { BadgeService } from '../../../providers/badgeService';
import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the DriverGoingMapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driver-going-map',
  templateUrl: 'driver-going-map.html',
})
export class DriverGoingMapPage {
  loading: Loading;
  hideFlag = false; 
  actionTimeout = null; 
  mapContainer = {
    map: null,//mapboxgl instance
    activeRoute: 1,//shows which route is actived
    startLonLat: [0, 0],//drift start point [lon, lat]
    destinationLonLat: [0, 0],//drift end point [lon, lat]
    currentLonLat: [0, 0],//current user point[lon, lat]
    currentMarker: null,//current user marker,
    destinationMarker: null,//destination marker
    userMarkerArray: [],//other users marker array
    driftpointMarkerArray: [],//drift points marker array
    idRoute: 0,//route layer id in the map
    directions: [],//directions data of current route
    routeElements: null,//instruction elements on top navigation ui
    wrongWayInterval: 0,//shows interval that user is driving to wrong way 
    drivingStarted: false,//show whether driving is started or not
    drivingFinished: false,//show whether driving is finished or not
    recalculating: false,//show whether recalculating route or not
    nextStepIndex: 0//shows the index that user is driving in the route
  };
  userInfo: any;
  updateUserInterval: any;
  updateOthersInterval: any;
  destination: any;
  mapId: number;//current page mapid
  showSosBadge: boolean = false;
  showingAlert: boolean = false;
  constructor(
    private navCtrl: NavController,
    private loadingCtrl: LoadingController, 
    private alertCtrl: AlertController,
    private translateService: TranslateService,
    private mapbox: MapboxService,
    private location: LocationService,
    private util: UtilService,
    private badge: BadgeService,
    private auth: AuthService,
    private modalCtrl: ModalController,
    private insomnia: Insomnia) {
      this.updateUserInfo();
      this.destination = {
        text: this.userInfo.destinationcaption,
        lon: this.userInfo.destinationlon,
        lat: this.userInfo.destinationlat
      };
      //get map id to avoid duplicate id
      this.mapId = this.mapbox.getMapId('driver_going_map');
  }
  /*
    get open request count
  */
  getTotalRequestCount() {
    return this.badge.getTotalRequestCount();
  }
  /*
    get open rquests
  */
  getTotalRequests() {
    return this.badge.getTotalRequests();
  }
  /*
    track requests
  */
  trackRequestsFn(index, item) {
    return item.userid;
  }
  /*
    update user info
  */
  updateUserInfo() {
    this.userInfo = this.auth.getUserInfo();
  }
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  }
  /*
    show modal
  */
  showConfirmModal() {
    let modal = this.modalCtrl.create('EndNavigationPage', {}, {cssClass: 'inset-modal'});
    modal.present();
    modal.onDidDismiss(data => {
      if(data.mode =='end'){
        if(this.navCtrl.getViews().length > 1){
          this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
        }else{
          this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
        }
      }else{

      }
    });
  }
  /*
    show Alert
  */
  showAlert(content: string){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {}
        }
      ]
    });
    alert.present();
  }
  /*
    show emergency alert
  */
  showEmergencyAlert() {
    let message = this.translateService.instant('Alert.Message.DeclareEmergency');
    let buttonYes = this.translateService.instant('Button.Yes');
    let buttonNo = this.translateService.instant('Button.No');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      message: message,
      buttons: [
        {
          text: buttonYes,
          handler: () => { this.declareEmergency();}
        },
        {
          text: buttonNo,
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }
  /*
    hide header
  */
  hideHeader() {
    this.actionTimeout = setTimeout(()=> {
      this.hideFlag = true;
    }, 5000);
  }
  /*
    touch action
  */
  touchAction() {
    if(this.hideFlag == true){
      this.hideFlag = false;
    }else{
      clearTimeout(this.actionTimeout);
    }
    this.hideHeader();
  }
  /*
    Runs when the page is about to enter and become the active page
  */
  ionViewWillEnter(){
    this.showLoading('Obtaining GPS position');
    this.userInfo = this.auth.getUserInfo();
    this.badge.refreshRequests();
    this.insomnia.keepAwake().then(()=>{
      console.log('keep awake...');
    });
    //initialize map container
    this.mapContainer = {
      map: null,//mapboxgl instance
      activeRoute: 1,//shows which route is actived
      startLonLat: [0, 0],//drift start point [lon, lat]
      destinationLonLat: [0, 0],//drift end point [lon, lat]
      currentLonLat: [0, 0],//current user point[lon, lat]
      currentMarker: null,//current user marker,
      destinationMarker: null,//destination marker
      userMarkerArray: [],//other users marker array
      driftpointMarkerArray: [],//drift points marker array
      idRoute: 0,//route layer id in the map
      directions: [],//directions data of current route
      routeElements: null,//instruction elements on top navigation ui
      wrongWayInterval: 0,//shows interval that user is driving to wrong way 
      drivingStarted: false,//show whether driving is started or not
      drivingFinished: false,//show whether driving is finished or not
      recalculating: false,//show whether recalculating route or not
      nextStepIndex: 0//shows the index that user is driving in the route
    };
    //check whether gps is available or not
    this.location.isLocationAvailable().then((res) => {
      console.log('GPS is available...');
    }, (err) => {
      this.loading.dismiss();
      let alertMessage = this.translateService.instant('Alert.Message.GpsNotAvailable');
      this.showAlert(alertMessage);
    });
    this.showMap(false, false, false);
    this.hideHeader();
    //show sos badge
    if(localStorage.getItem('drift_sosClicked') !== 'true' && (this.userInfo.emergencycontact1 == null && this.userInfo.emergencycontact2 == null)){
      this.showSosBadge = true;
    }else{
      this.showSosBadge = false;
    }
  }
  /*
    runs when the page has finished leaving and is no longer the active page
  */
  ionViewWillLeave() {
    clearInterval(this.updateUserInterval);
    clearInterval(this.updateOthersInterval);
    this.mapContainer.map.remove();
    this.insomnia.allowSleepAgain().then(()=>{
      console.log('allow sleep...');
    });
  }
  /*
    runs when the page is about to be destroyed and have it elements removed
  */
  ionViewWillUnload() {
    this.mapbox.removeMapId('driver_going_map');
  }
  /*
    show map in the page
  */
  showMap(_mapUpdate: boolean, _userUpdate: boolean, _otherUpdate: boolean) {
    let mapOptions = {
      container: 'driver_going_map' + this.mapId,//map container id selector
      style: mapboxStyle,//map style from app.config.ts
      center: [0, 0], // starting position [lng, lat]
      zoom: 16,
      pitch: 60,
      bearing: 0,
      trackResize: false//If  true , the map will automatically resize when the browser window resizes.
    };
    let dataOptions = {
      currentlat: 0,
      currentlon: 0,
      destinationlat: this.destination.lat,
      destinationlon: this.destination.lon,
      users: null,
      driftpoints: null,
      mode: 'Driver',
      routeColor: routeColor
    };
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null){
        if(_mapUpdate == false && this.showingAlert == false){
          let alert = this.alertCtrl.create({
            title: `<img src="assets/img/logo.png"/>`,
            subTitle: 'Failed obtaining your GPS location.',
            buttons: [
              {
                text: 'try again',
                handler: () => {
                  this.showMap(false, false, false);
                  this.showingAlert = false;
                }
              }
            ]
          });
          alert.present();
          this.showingAlert = true;
        }
      }else{
        mapOptions.center = [cpos.longitude, cpos.latitude];
        dataOptions.currentlat = cpos.latitude;
        dataOptions.currentlon = cpos.longitude;
        if(_mapUpdate == false){
          this.mapbox.showNavigationMap(this.mapContainer, false, false, false, mapOptions, dataOptions).then((res) =>{
            this.loading.dismiss();
            //add other users to the map
            this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers)=> {
              dataOptions.users = resusers;
              this.mapbox.showNavigationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {});
            });
            //add driftpoints to the map
            this.location.getDriftPoints(cpos.latitude, cpos.longitude, mapboxDistance).then((resdrifts) => {
              dataOptions.driftpoints = resdrifts;
              this.mapbox.showNavigationMap(this.mapContainer, true, false, false, null, dataOptions).then((res) => {});
            });
            //update intervals
            this.updateUserInterval = setInterval(() => {
              this.showMap(true, true, false);
            }, 500);
            this.updateOthersInterval = setInterval(() => {
              this.showMap(true, false, true);
            }, intervalTime);
          }, (err) => {
            this.loading.dismiss();
            console.log(err);
          });
        }else if(_userUpdate == true){
          dataOptions.currentlat = cpos.latitude;
          dataOptions.currentlon = cpos.longitude;
          this.mapbox.showNavigationMap(this.mapContainer, true, true, false, null, dataOptions).then((res) => {
            // update current user
          });
        }else if(_otherUpdate == true){
          this.location.getUsers(cpos.latitude, cpos.longitude, mapboxDistance).then((resusers) => {
            dataOptions.users = resusers;
            this.mapbox.showNavigationMap(this.mapContainer, true, false, true, null, dataOptions).then((res) => {
              // update other users
            });          
          });
        }
      }
    });
  }
  /*
    Called when user click arrow button
  */
  goBack() {
    this.showConfirmModal();
  }
  /*
    Called when user click 'sos' button
  */
  goSos() {
    localStorage.setItem('drift_sosClicked', 'true');
    if(this.userInfo.emergencycontact1 == null && this.userInfo.emergencycontact2 == null){
      this.navCtrl.push('EmergencyContactPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }else{
      this.showEmergencyAlert();
    }
  }
  /*
    Called when user click request badge area
  */
  goRequest(request){
    if(request.type == 'companionrequest'){
      this.goCompanionRequest();
    }else if(request.type == 'driftinvitation'){
      this.goDriftRequest();
    }else {
      this.goChatMessage(request.userid);
    }
  }
  /*
    Called when user click 'companionrequest' badge area
  */
  goCompanionRequest() {
    this.navCtrl.push('ProfileCompanionsPage', {userid: this.userInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    Called when user click 'driftrequest' badge area
  */
  goDriftRequest() {
    this.navCtrl.push('TravelsRequestsPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
  }
  /*
    Called when user click 'chatmessage' badge area
  */
  goChatMessage(_userid) {
    this.navCtrl.push('ChatsPage', { friendsId: _userid }, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  }
  /*
    declare emergency
  */
  declareEmergency() {
    this.location.getCurrentLocation().then((cpos: any) => {
      if(cpos == null){
        cpos = mapboxCenter;
      }
      this.util.declareEmergency(cpos.latitude, cpos.longitude).then((res)=> {
        // handle success
      }, (err) => {
        // handle error
      });
    });
  }
}
