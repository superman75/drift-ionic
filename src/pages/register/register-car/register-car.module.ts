import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterCarPage } from './register-car';
import { TranslateModule } from '@ngx-translate/core';
import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    RegisterCarPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterCarPage),
    TranslateModule.forChild()
  ],
  providers: [
    Camera
  ]
})
export class RegisterCarPageModule {}
