import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, LoadingController, Loading, AlertController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the RegisterCarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-car',
  templateUrl: 'register-car.html',
})
export class RegisterCarPage {
	vehicles = { platenumber: '', carmodel: '', picture: 'assets/img/car-thumb.jpg'};
  loading: Loading;
  constructor(
    private navCtrl: NavController,
    private camera: Camera, 
    private alertCtrl: AlertController, 
    private actionSheetCtrl: ActionSheetController, 
    private loadingCtrl: LoadingController, 
    private translateService: TranslateService, 
    private auth: AuthService) {
  }
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    show Alert
  */
  showAlert(content: string){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {
            //handler
          }  
        }
      ]
    });
    alert.present();
  };
  /*
  Called when user click image or thumb icon
  */
  showActionSheet() {
    let titleChoosePicture = this.translateService.instant('ActionSheet.Title.ChoosePicture');
    let buttonChooseAlbums = this.translateService.instant('ActionSheet.Button.ChooseAlbums');
    let buttonChooseCamera = this.translateService.instant('ActionSheet.Button.ChooseCamera');
    let actionSheet = this.actionSheetCtrl.create({
      title: titleChoosePicture,
      buttons: [
        {
          text: buttonChooseAlbums,
          icon: 'albums',
          handler: () => {
            let options: CameraOptions = {
              quality: 75,
              destinationType: this.camera.DestinationType.DATA_URL,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: true,
              encodingType: this.camera.EncodingType.JPEG,
              targetWidth: 150,
              targetHeight: 150,
              saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then((imageData) => {
              //imageData is either a base64 encoded string or a file URI
              //If it's base64:
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              this.vehicles.picture = base64Image;
            }, (err) => {
              //Handle error
            });
          }
        },
        {
          text: buttonChooseCamera,
          icon: 'camera',
          handler: () => {
            let options: CameraOptions = {
              quality: 75,
              destinationType: this.camera.DestinationType.DATA_URL,
              sourceType: this.camera.PictureSourceType.CAMERA,
              allowEdit: true,
              encodingType: this.camera.EncodingType.JPEG,
              targetWidth: 150,
              targetHeight: 150,
              saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then((imageData) => {
              //imageData is either a base64 encoded string or a file URI
              //If it's base64:
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              this.vehicles.picture = base64Image;
            }, (err) => {
              //Handle error
            });
          }
        }
      ]
    });
    actionSheet.present();
  };
  /*
	  Called when user click buttons in this page
  */
  carDone(mode:string){
    this.showLoading();
    switch (mode) {
      case "later":
        this.auth.havenoVehicle(0).then((res) => {
          this.loading.dismiss();
          this.goCharacter();
        }, (err) => {
          this.loading.dismiss();
          console.log(err);
        });
        break;
      case "donot":
        this.auth.havenoVehicle(1).then((res) => {
          this.loading.dismiss();
          this.goCharacter();
        }, (err) => {
          this.loading.dismiss();
          console.log(err);
        });
        break;
      case "update":
        if(this.vehicles.platenumber == '' || this.vehicles.carmodel == ''){
          this.loading.dismiss();
          let message = this.translateService.instant('Alert.Message.InsertCar');
          this.showAlert(message);
        }else if(this.vehicles.picture == 'assets/img/car-thumb.jpg'){
          this.auth.updateVehicle(this.vehicles.platenumber, this.vehicles.carmodel).then((res) => {
            this.loading.dismiss();
            this.goCharacter();
          }, (err) => {
            this.loading.dismiss();
            console.log('update vehicle error...');
          });
        }else {
          this.auth.updateVehicle(this.vehicles.platenumber, this.vehicles.carmodel, this.vehicles.picture).then((res) => {
            this.loading.dismiss();
            this.goCharacter();
          }, (err) => {
            this.loading.dismiss();
            console.log('update vehicle error...');
          });
        }
        break;
      default:
        // code...
        break;
    }
  };
  /*
    go register character page;
  */
  goCharacter(){
    this.navCtrl.setRoot('RegisterCharacterPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
}
