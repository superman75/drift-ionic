import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterTreatPage } from './register-treat';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegisterTreatPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterTreatPage),
    TranslateModule.forChild()
  ],
})
export class RegisterTreatPageModule {}
