import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
/**
 * Generated class for the RegisterTreatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-treat',
  templateUrl: 'register-treat.html',
})
export class RegisterTreatPage {

  constructor(
    private navCtrl: NavController) {
  };

  /*
    Called when user click 'nice' button
  */
  treatDone(){
    this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };

}
