import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPerkPage } from './register-perk';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegisterPerkPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPerkPage),
    TranslateModule.forChild()
  ],
})
export class RegisterPerkPageModule {}
