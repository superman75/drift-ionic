import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { Perks } from '../../../models/perks.model';
import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the RegisterPerkPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-perk',
  templateUrl: 'register-perk.html',
})
export class RegisterPerkPage {
  loading: Loading;
  private perks: Array<any>;
  constructor(
    private navCtrl: NavController,
    private loadingCtrl: LoadingController, 
    private translateService: TranslateService, 
    private auth: AuthService) {
      this.perks = Perks.getList(true);
  };
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    Called when user click 'done' button
  */
  perkDone() {
    let perksString = Perks.getString(this.perks);
    if(perksString == ''){
      this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }else{
      this.showLoading();
      this.auth.updatePerk(perksString).then((res) => {
        this.loading.dismiss();
        this.navCtrl.setRoot('MainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }, (err) => {
        this.loading.dismiss();
        console.log('updating perks error...');
      });
    }
  };
}
