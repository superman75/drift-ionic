import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterTosPage } from './register-tos';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegisterTosPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterTosPage),
    TranslateModule.forChild()
  ],
})
export class RegisterTosPageModule {}
