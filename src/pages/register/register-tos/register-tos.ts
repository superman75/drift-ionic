import { Component } from '@angular/core';
import { IonicPage, ViewController, Loading, LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiService } from '../../../providers/apiService';

/**
 * Generated class for the RegisterTosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-tos',
  templateUrl: 'register-tos.html',
})
export class RegisterTosPage {
  loading: Loading;
  terms: any;
  constructor(
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService,
    private api: ApiService) {
      this.showLoading();
      this.api.getTerms().subscribe((response)=>{
        this.terms = response.text();
        this.loading.dismiss();
      });
  }
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    called when user click 'close' button
  */
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
