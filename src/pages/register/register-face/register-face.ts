import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, AlertController, LoadingController, Loading} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the RegisterFacePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-face',
  templateUrl: 'register-face.html',
})
export class RegisterFacePage {
	faceURL;
  loading: Loading;
  constructor(
    private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController, 
    private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController, 
    private translateService: TranslateService, 
    private camera: Camera, 
    private auth: AuthService) {
  	  this.faceURL = 'assets/img/register-face.jpg';
  };
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
	  Called when user click image or thumb icon
  */
  showActionSheet() {
    let titleChoosePicture = this.translateService.instant('ActionSheet.Title.ChoosePicture');
  	let buttonChooseAlbums = this.translateService.instant('ActionSheet.Button.ChooseAlbums');
  	let buttonChooseCamera = this.translateService.instant('ActionSheet.Button.ChooseCamera');
  	let actionSheet = this.actionSheetCtrl.create({
      title: titleChoosePicture,
  		buttons: [
  			{
  				text: buttonChooseAlbums,
  				icon: 'albums',
  				handler: () => {
  					let options: CameraOptions = {
  						quality: 75,
              destinationType: this.camera.DestinationType.DATA_URL,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: true,
              encodingType: this.camera.EncodingType.JPEG,
              targetWidth: 150,
              targetHeight: 150,
              saveToPhotoAlbum: false
  					};
  					this.camera.getPicture(options).then((imageData) => {
  						//imageData is either a base64 encoded string or a file URI
  						//If it's base64:
  						let base64Image = 'data:image/jpeg;base64,' + imageData;
  						this.faceURL = base64Image;
  					}, (err) => {
  						//Handle error
  					});
  				}
  			},
  			{
  				text: buttonChooseCamera,
  				icon: 'camera',
  				handler: () => {
  					let options: CameraOptions = {
  						quality: 75,
              destinationType: this.camera.DestinationType.DATA_URL,
              sourceType: this.camera.PictureSourceType.CAMERA,
              allowEdit: true,
              encodingType: this.camera.EncodingType.JPEG,
              targetWidth: 150,
              targetHeight: 150,
              saveToPhotoAlbum: false
  					};
  					this.camera.getPicture(options).then((imageData) => {
  						//imageData is either a base64 encoded string or a file URI
  						//If it's base64:
  						let base64Image = 'data:image/jpeg;base64,' + imageData;
  						this.faceURL = base64Image;
  					}, (err) => {
  						//Handle error
  					});
  				}
  			}
  		]
  	});
  	actionSheet.present();
  };
  /*
    Called when user click 'that's my chocolate side' button
  */
  faceDone() {
    let messageUploadPicture = this.translateService.instant('Alert.Message.UploadPicture');
    let buttonBack = this.translateService.instant('Alert.Button.Back');
    let buttonSkip = this.translateService.instant('Alert.Button.Skip');
    if(this.faceURL == 'assets/img/register-face.jpg'){
      let alert = this.alertCtrl.create({
        title: `<img src="assets/img/logo.png"/>`,
        message: messageUploadPicture,
        buttons: [
          {
            text: buttonBack,
            role: 'cancel',
            handler: () => {
              //close alert
            }
          },
          {
            text: buttonSkip,
            handler: () => {
              if(this.auth.registerData.hasdriverslicense){
                this.navCtrl.setRoot('RegisterCarPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
              }else{
                this.navCtrl.setRoot('RegisterCharacterPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
              }
            }
          }
        ]
      });
      alert.present();
    }else{
      this.showLoading();
      this.auth.updateProfilePicture(this.faceURL).then((res) => {
        this.loading.dismiss();
        if(this.auth.registerData.hasdriverslicense){
          this.navCtrl.setRoot('RegisterCarPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
        }else{
          this.navCtrl.setRoot('RegisterCharacterPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
        }
      }, (err) => {
        this.loading.dismiss();
        console.log('update profile picture error...');
      });
    }
  };
}
