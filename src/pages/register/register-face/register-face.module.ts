import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterFacePage } from './register-face';
import { Camera } from '@ionic-native/camera';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegisterFacePage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterFacePage),
    TranslateModule.forChild()
  ],
  providers: [
  	Camera
  ]
})
export class RegisterFacePageModule {}
