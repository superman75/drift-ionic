import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterCharacterPage } from './register-character';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegisterCharacterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterCharacterPage),
    TranslateModule.forChild()
  ],
})
export class RegisterCharacterPageModule {}
