import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the RegisterCharacterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-character',
  templateUrl: 'register-character.html',
})
export class RegisterCharacterPage {
	aboutCount: number = 0;
	aboutCharacter: string = '';
  loading: Loading;
  constructor(
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private auth: AuthService,
    private translateService: TranslateService) {
  };
    /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
		Called when user change textarea content
  */
  changeAbout() {
  	var numberOfLineBreaks = (this.aboutCharacter.match(/\n/g) || []).length;
  	this.aboutCount = this.aboutCharacter.length + numberOfLineBreaks;
  };
  /*
		Called when user click 'done' button
  */
  characterDone() {
    this.showLoading();
    this.auth.updateAbout(this.aboutCharacter).then((res) => {
      this.loading.dismiss();
      this.navCtrl.setRoot('RegisterPerkPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
    }, (err) => {
      this.loading.dismiss();
      console.log('update about text error...');
    });
  };
}
