import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, ModalController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { GenderValidator } from '../../../validators/gender.validator';
import { PasswordValidator } from '../../../validators/password.validator';
import { PhoneValidator } from '../../../validators/phone.validator';
import { TranslateService } from '@ngx-translate/core';

import { Country } from '../../../models/country.model';
import { UtilService } from '../../../providers/utilService';
import { AuthService } from '../../../providers/authService';
/**
 * Generated class for the RegisterFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-form',
  templateUrl: 'register-form.html',
})
export class RegisterFormPage {
  loading: Loading;
  registerFormGroup: FormGroup;
  matchingPasswordsGroup: FormGroup;
  countryPhoneGroup: FormGroup;
  genderGroup: FormGroup;
  submitted: boolean = false;
  countries: Array<Country> = [];
  selectOptions: any;
  checkNicknameText: string = '';
  checkEmailText: string = '';
  changingEmail: boolean = false;
  changingPhone: boolean = false;
  changingPassword: boolean = false;
  changingConfirm: boolean = false;
  constructor(
    private navCtrl: NavController, 
    private formBuilder: FormBuilder, 
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private translateService: TranslateService, 
    private util: UtilService, 
    private auth: AuthService) {
  }
  /*
    runs when page is loaded
  */
  ionViewWillLoad() {
    this.countries = this.util.countries;
    /*
      Create formgroup that contains gender list
    */
    this.genderGroup = new FormGroup({
      male: new FormControl(false),
      female: new FormControl(false),
      open: new FormControl(false)
    }, (formGroup: FormGroup) => {
      return GenderValidator.noSelect(formGroup);
    });
    /*
      Create formgroup that contains password and confirm password control
    */
    this.matchingPasswordsGroup = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[^\w\s][ -~]+$')
      ])),
      confirmPassword: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });
    /*
      Create formgroup that contains country and phone number control
    */
    let country = new FormControl(this.countries[0], Validators.required);
    let phone = new FormControl('', Validators.compose([
      Validators.required,
      PhoneValidator.validCountryPhone(country)
    ]));
    /*
      Create register form group
    */
    this.registerFormGroup = this.formBuilder.group({
      genderGroup: this.genderGroup,
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      nickname: new FormControl(''),
      profession: new FormControl(''),
      birthdate: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      country: country,
      phone: phone,
      city: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      zip: new FormControl('', Validators.required),
      matchingPasswords: this.matchingPasswordsGroup,
      hasdriverslicense: new FormControl(false),
      terms: new FormControl(true, Validators.pattern('true'))
    });
  };
  /*
    runs whenever page is active
  */
  ionViewWillEnter() {
    this.translateService.get('Label.Country').subscribe((value) => {
      this.selectOptions = {
        title: value
      };
    });
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    called when user click 'Terms of services'
  */
  showTos(){
    let modal = this.modalCtrl.create('RegisterTosPage', {}, {cssClass: 'inset-modal', enableBackdropDismiss: true});
    modal.present();
    modal.onDidDismiss(data=>{});
  };
  /*
    Called when user click 'done!' button
  */
  onSubmit(){
    this.submitted = true;
    if(this.registerFormGroup.valid && this.checkNicknameText != 'Label.NicknameNotAvailable' && this.checkEmailText != 'Label.EmailNotAvailable'){
      this.showLoading();
      this.auth.register(this.registerFormGroup.value).then((regRes) => {
        this.loading.dismiss();
        this.navCtrl.setRoot('RegisterFacePage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      }, (regErr) => {
        this.loading.dismiss();
        console.log('register error...');
      });
    }
  };

  /*
    Called when user click 'gender' option
  */
  onSelectGender(gender){
    for (let key in this.genderGroup.controls) {
      if (this.genderGroup.controls.hasOwnProperty(key)) {
        let control: FormControl = <FormControl>this.genderGroup.controls[key];
        control.patchValue(false);
      }
    }
    this.genderGroup.controls[gender].patchValue(true);
  };
  /*
     Called when blue email text field
  */
  checkEmail(){
    if(this.registerFormGroup.get('email').valid){
      this.checkEmailText = 'Label.Checking';
      this.auth.checkEmail(this.registerFormGroup.get('email').value).then((res) => {
        this.checkEmailText = 'Label.EmailAvailable';
      }, (err) => {
        this.checkEmailText = 'Label.EmailNotAvailable';
      });
    }
  };
  /*
    Called when user change email text field
  */
  changeEmail() {
    this.changingEmail=true;
    this.checkEmailText = '';
  };
  /*
    Called when user swipe right screen
  */
  goLogin() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  }

}
