import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterFormPage } from './register-form';
import { TranslateModule } from '@ngx-translate/core';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    RegisterFormPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterFormPage),
    TranslateModule.forChild(),
    TextMaskModule
  ],
})
export class RegisterFormPageModule {}
