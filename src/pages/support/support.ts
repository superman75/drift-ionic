import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController, ToastController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from '../../providers/utilService';

/**
 * Generated class for the SupportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {
  loading: Loading;
  categories:Array<any> = [];
  selectedId: string = '';
  selectedCategory: string = '';
  selectOptions: any;
  message: string = '';
  constructor(
    private navCtrl: NavController, 
    private util: UtilService,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private translateService: TranslateService,
    private alertCtrl: AlertController) {
      this.util.getSupportCategories().then((res: Array<any>)=> {
        this.categories = res;
      }, (err)=> {
        console.log(err);
      });
  };
  /*
    show toast message
  */
  showToast(content) {
    let toast = this.toastCtrl.create({
      message: content,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  };
  /*
    show loading modal
  */
  showLoading(content?) {
    var loadingContent = this.translateService.instant('Label.PleaseWait');
    if(content) loadingContent = content;
    this.loading = this.loadingCtrl.create({
      content: loadingContent
    });
    this.loading.present();
  };
  /*
    show Alert
  */
  showAlert(content){
    let buttonOk = this.translateService.instant('Button.Ok');
    let alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: content,
      buttons: [
        {
          text: buttonOk,
          handler: () => {
          }  
        }
      ]
    });
    alert.present();
  };

  ionViewWillEnter() {
    this.translateService.get('Label.WhatsTheMainIssue').subscribe((value) => {
      this.selectOptions = {
        title: value
      };
    });
  };
  /*
    Called when user click back arrow button
  */
  goBack() {
    this.navCtrl.pop({animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
  /*
    Called when user click 'send' button
  */
  send() {
    if(this.selectedId != ''){
      if(this.message != ''){
        this.showLoading();
        this.util.submitSupport(this.selectedId, this.message).then((res) => {
          this.loading.dismiss();
          let toastMessage = this.translateService.instant('Toast.SendMessageSuccess');
          this.showToast(toastMessage);
        }, (err) => {
          this.loading.dismiss();
          console.log(err);
        });
      }else{
        let alertMessage = this.translateService.instant('Alert.Message.TypeMessage');
        this.showAlert(alertMessage);
      }
    }else{
      let alertMessage = this.translateService.instant('Alert.Message.SelectCategory');
      this.showAlert(alertMessage);
    }
  };
  /*
    called when user click issue title
  */
  showSelect() {

  };
  /*
    Called when user select option
  */
  changeSelected() {
    this.categories.forEach((_item) => {
      if(_item.id == this.selectedId){
        this.selectedCategory = _item.text
      }
    });
  };
}
