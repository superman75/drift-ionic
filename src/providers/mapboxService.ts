import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { Events } from 'ionic-angular';

import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import turf from '@turf/turf/turf.min.js';
import { mapboxApiKey, apiEndpoint, systemOptions } from '../app/app.config';
import { ApiService } from './apiService';
import { LocationService } from './locationService';

@Injectable()
export class MapboxService {
  driftPointUrl = apiEndpoint + '/images/driftpoint.png';
  currentUrl = apiEndpoint + '/images/currentposition.png';
  placeholderUrl = apiEndpoint + '/images/portrait_placeholder.png';
  destinationUrl = apiEndpoint + '/images/destination.png';
  offsetY = 120;
  constructor(
    private api: ApiService,
    private location: LocationService,
    private translateService: TranslateService, 
    private events: Events,
    private http: Http) {
      mapboxgl.accessToken = mapboxApiKey;
  };
  /*
    get new map id
  */
  getMapId(_mapName: string){
    var mapId =  parseInt(localStorage.getItem('drift_' + _mapName) || '0');
    mapId = mapId + 1;
    localStorage.setItem('drift_' + _mapName, mapId.toString());
    return mapId;
  };
  /*
    remove map id
  */
  removeMapId(_mapName: string){
    var mapId = parseInt(localStorage.getItem('drift_' + _mapName));
    mapId = mapId - 1;
    localStorage.setItem('drift_' + _mapName, mapId.toString());
  };
  /*
    get directions between two coordinates
    @parameter
      _startLon, _startLat, _endLon, _endLat
  */
  getMapboxDirections(_startLon, _startLat, _endLon, _endLat, _step, _overview) {
    return new Promise((resolve, reject) => {
      this.http.get('https://api.mapbox.com/directions/v5/mapbox/driving/' + _startLon + ',' + _startLat + ';' + _endLon + ',' + _endLat + '?geometries=geojson&overview=' + _overview +'&steps=' + _step + '&access_token=' + mapboxApiKey).subscribe((res) => {
        let response = res.json();
        resolve(response);
      }, (err) => {
        reject('get direction error');
      });
    });
  };
  /*
    Initialize steps in notification element
    @parameter
      _mapContainer: Object(container instance)
  */
  initStep(_mapContainer){
    let _initStep = _mapContainer.directions.routes[0].legs[0].steps[0];
    let _currentPos = _mapContainer.currentLonLat;
    let _nextStep = _mapContainer.directions.routes[0].legs[0].steps[1];
    let distance = this.distanceBetween(_currentPos, _initStep.maneuver.location);
    var _distanceElement = document.getElementById('distance');
    var _directionElement = document.getElementById('direction') as HTMLImageElement;
    var _instructionElement = document.getElementById('instruction');
    var _unitElement = document.getElementById('unit');
    var _nextElement = document.getElementById('ndirection') as HTMLImageElement;
    var _thenElement = document.getElementById('then');
    var _stepsElement = document.getElementById('steps');
    _stepsElement.style.background = 'linear-gradient(#0c9286, #007f5e)';
    _thenElement.style.display = 'block';
    _distanceElement.textContent = (Math.round(distance * 1000)).toString();
    if(_initStep.maneuver.modifier){
      let modifierArray = _initStep.maneuver.modifier.split(' ');
      let modifierString = modifierArray.join('_');
      let typeArray = _initStep.maneuver.type.split(' ');
      let typeString = typeArray.join('_');
      _directionElement.src = "assets/img/directions/" + 'direction_' + typeString + '_' + modifierString + '.png';
    }else{
      let typeArray = _initStep.maneuver.type.split(' ');
      let typeString = typeArray.join('_');
      _directionElement.src = "assets/img/directions/" + 'direction_' + typeString + '.png';
    }
    _instructionElement.textContent = _initStep.maneuver.instruction;
    _unitElement.textContent = 'm';
    if(_nextStep.maneuver.modifier){
      let modifierArray = _nextStep.maneuver.modifier.split(' ');
      let modifierString = modifierArray.join('_');
      let typeArray = _nextStep.maneuver.type.split(' ');
      let typeString = typeArray.join('_');
      _nextElement.src = "assets/img/directions/" + 'direction_' + typeString + '_' + modifierString + '.png';
    }else{
      let typeArray = _nextStep.maneuver.type.split(' ');
      let typeString = typeArray.join('_');
      _nextElement.src = "assets/img/directions/" + 'direction_' + typeString + '.png';
    }
    var routeElements = {
      distanceElement: _distanceElement,
      directionElement: _directionElement,
      instructionElement: _instructionElement,
      unitElement: _unitElement,
      nextElement: _nextElement,
      thenElement: _thenElement,
      stepsElement: _stepsElement
    };
    _mapContainer.routeElements = routeElements;
  };
  /*
    Show current route step's manenver
    @parameter
      _mapContainer: Object(container instance)
  */
  displayManeuver(_mapContainer){
    let nextStep = _mapContainer.directions.routes[0].legs[0].steps[_mapContainer.nextStepIndex];
    let routeNextStartPos = nextStep.geometry.coordinates[0];
    var distance = this.distanceBetween(_mapContainer.currentLonLat, routeNextStartPos) * 1000;
    var thenStep;
    if(_mapContainer.nextStepIndex < _mapContainer.directions.routes[0].legs[0].steps.length-1)
    {
      thenStep = _mapContainer.directions.routes[0].legs[0].steps[_mapContainer.nextStepIndex + 1];
      if(thenStep.maneuver.modifier){
        let modifierArray = thenStep.maneuver.modifier.split(' ');
        let modifierString = modifierArray.join('_');
        let typeArray = thenStep.maneuver.type.split(' ');
        let typeString = typeArray.join('_');
        _mapContainer.routeElements.nextElement.src = "assets/img/directions/" + 'direction_' + typeString + '_' + modifierString + '.png';
      }else{
        let typeArray = thenStep.maneuver.type.split(' ');
        let typeString = typeArray.join('_');
        _mapContainer.routeElements.nextElement.src = "assets/img/directions/" + 'direction_' + typeString + '.png';
      }
    }else{
      _mapContainer.routeElements.thenElement.style.display = 'none';
    }
    if(distance > 1000){
      _mapContainer.routeElements.distanceElement.textContent = (distance/1000).toFixed(1);
      _mapContainer.routeElements.unitElement.textContent = 'km';
    }else{
      _mapContainer.routeElements.distanceElement.textContent = parseInt(distance.toString());
      _mapContainer.routeElements.unitElement.textContent = 'm';
    }
    if(nextStep.maneuver.modifier){
      let modifierArray = nextStep.maneuver.modifier.split(' ');
      let modifierString = modifierArray.join('_');
      let typeArray = nextStep.maneuver.type.split(' ');
      let typeString = typeArray.join('_');
      _mapContainer.routeElements.directionElement.src = "assets/img/directions/" + 'direction_' + typeString + '_' + modifierString + '.png';
    }else{
      let typeArray = nextStep.maneuver.type.split(' ');
      let typeString = typeArray.join('_');
      _mapContainer.routeElements.directionElement.src = "assets/img/directions/" + 'direction_' + typeString + '.png';
    }
    _mapContainer.routeElements.instructionElement.textContent = nextStep.maneuver.instruction;
  };
  /*
    get directions between two coordinates
    @parameter
      _startLon, _startLat, _endLon, _endLat
  */
  getDirections(_startLon, _startLat, _endLon, _endLat, _step) {
    return new Promise((resolve, reject) => {
      this.api.getDirections(_startLon + ',' + _startLat + ';' + _endLon + ',' + _endLat + '?geometries=geojson&overview=full&steps=true&access_token=' + mapboxApiKey).subscribe((res) => {
        let response = res.json();
        resolve(response);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get direction error');
      });
    });
  };
  /*
    Generate new route
    @parameter
      _mapContainer: Object(container instance)
  */
  recalculateRoute(_mapContainer){
    _mapContainer.recalculating = true;
    _mapContainer.drivingStarted = false;
    let _startPos = _mapContainer.currentLonLat;
    let _endPos = _mapContainer.destinationLonLat;
    this.getDirections(_startPos[0],  _startPos[1], _endPos[0], _endPos[1], true).then((res: any) => {
      _mapContainer.directions = res;
      let coordinates = res.routes[0].geometry.coordinates;
      let idRoute = _mapContainer.idRoute + 1;
      this.addRouteCoordinates(idRoute, _mapContainer.map, coordinates, 50, '#32adfe', false);
      _mapContainer.map.removeLayer('route' + _mapContainer.idRoute);
      _mapContainer.idRoute = idRoute;
      //_mapContainer.map.setCenter(_mapContainer.currentLonLat);
      //_mapContainer.map.easeTo({center: _mapContainer.currentLonLat, offset: [0, this.offsetY]});
      this.initStep(_mapContainer);
      _mapContainer.recalculating = false;
    }, (err) => {
      console.log(err);
    });
  };
  /*
    Change route
    @parameter
      _mapContainer: Object(container instance)
      _activeRoute: number(route active number)
  */
  changeRoute(_mapContainer, _activeRoute){
    if(_activeRoute == 1){
      _mapContainer.destinationLonLat = _mapContainer.endLonLat;
    }else if(_activeRoute == 0){
      _mapContainer.destinationLonLat = _mapContainer.otherLonLat;
    }
    _mapContainer.recalculating = true;
    _mapContainer.drivingStarted = false;
    _mapContainer.drivingFinished = false;
    let _startPos = _mapContainer.currentLonLat;
    let _endPos = _mapContainer.destinationLonLat;
    this.getDirections(_startPos[0],  _startPos[1], _endPos[0], _endPos[1], true).then((res: any) => {
      _mapContainer.directions = res;
      let coordinates = res.routes[0].geometry.coordinates;
      let idRoute = _mapContainer.idRoute + 1;
      this.addRouteCoordinates(idRoute, _mapContainer.map, coordinates, 50, '#32adfe', false);
      _mapContainer.map.removeLayer('route' + _mapContainer.idRoute);
      _mapContainer.idRoute = idRoute;
      this.initStep(_mapContainer);
      _mapContainer.recalculating = false;
    },(err) => {
      console.log(err);
    });
  };
  /*
    Check step contains current marker or not within a distance
    @parameter
      _mapContainer: Object(container instance)
      _coordinates: Coordinates Array
      _distance: Distance round to check
  */
  checkContain(_mapContainer, _coordinates, _distance){
    var currentPos = turf.point(_mapContainer.currentLonLat);
    var stepLine = turf.lineString(_coordinates);
    var isContain = turf.booleanContains(stepLine, currentPos);
    if(isContain == true){
      return true;
    }
    var snapped = turf.pointOnLine(stepLine, currentPos, 'kilometers');
    if(snapped.properties.dist < _distance){
      return true;
    }
    return false;
  };
  /*
    check driving is started in route or not
    @parameter
      _mapContainer: Object(container instance)
  */
  checkStarted(_mapContainer) {
    let routeStartPos = _mapContainer.directions.routes[0].geometry.coordinates[0];
    let distance = Math.round(this.distanceBetween(_mapContainer.currentLonLat, routeStartPos) * 1000);
    _mapContainer.routeElements.distanceElement.textContent = distance;
    if(distance > 100){
      this.recalculateRoute(_mapContainer);
    }else if(distance < 10){
      console.log('started...');
      _mapContainer.drivingStarted = true;
      _mapContainer.nextStepIndex = 1;
      this.displayManeuver(_mapContainer);
    }else {
      var firstStepCoordinates = _mapContainer.directions.routes[0].legs[0].steps[0].geometry.coordinates;
      if(this.checkContain(_mapContainer, firstStepCoordinates, 0.01)){
        _mapContainer.drivingStarted = true;
        _mapContainer.nextStepIndex = 1;
        this.displayManeuver(_mapContainer);
      }                          
    }
  };
  /*
    checking driving
    @parameter
      _mapContainer: Object(container instance)
  */
  checkManeuver(_mapContainer){
    let routeNexPos = _mapContainer.directions.routes[0].legs[0].steps[_mapContainer.nextStepIndex].geometry.coordinates[0];
    let distance = this.distanceBetween(_mapContainer.currentLonLat, routeNexPos) * 1000;
    this.displayManeuver(_mapContainer);
    let stepCoordinates = _mapContainer.directions.routes[0].legs[0].steps[_mapContainer.nextStepIndex-1].geometry.coordinates;
    if(!this.checkContain(_mapContainer, stepCoordinates, 0.01)){
      this.recalculateRoute(_mapContainer);
    }
    if(distance < 10){
      if(_mapContainer.nextStepIndex < _mapContainer.directions.routes[0].legs[0].steps.length-1){
        _mapContainer.nextStepIndex++;
      }else{
        if(_mapContainer.activeRoute == 1){
          this.events.publish('reachedDestination');
        }
        _mapContainer.drivingFinished = true;
        _mapContainer.routeElements.stepsElement.style.background = 'linear-gradient(#5cb85c, #5aaf5a)';
        _mapContainer.routeElements.distanceElement.textContent = 0;
      }
    }
  };
  /*
    Check user's navigation while user is moving to new position
    @parameter
      _mapContainer: Object(map container instance)
      _newPos: user's current new position
  */
  checkNavigation(_mapContainer, _newPos){
    if(_mapContainer.recalculating == false){
      if(_mapContainer.drivingStarted == false){//user is not on route yet
        _mapContainer.currentLonLat = _newPos;
        this.checkStarted(_mapContainer);
      }else{//user is driving on the route
        if(_mapContainer.drivingFinished == false){
          let stepCoordinates = _mapContainer.directions.routes[0].legs[0].steps[_mapContainer.nextStepIndex-1].geometry.coordinates
          let drivingBearing = this.bearingBetween(_mapContainer.currentLonLat, _newPos);
          let stepBearing = this.bearingBetween(stepCoordinates[0], stepCoordinates[stepCoordinates.length-1]);
          let diffBearing = Math.abs(drivingBearing) - Math.abs(stepBearing);
          if(diffBearing > 90 || diffBearing < -90){
            console.log('driving to wrong way...');
            _mapContainer.wrongWayInterval++;
            //_mapContainer.routeElements.stepsElement.style.background = 'linear-gradient(#ff9900, #e6b500)';
            if(_mapContainer.wrongWayInterval > 10){
              this.recalculateRoute(_mapContainer);
            }
          }else{
            _mapContainer.wrongWayInterval = 0;
            _mapContainer.routeElements.stepsElement.style.background = 'linear-gradient(#0c9286, #007f5e)';
          }
          _mapContainer.currentLonLat = _newPos;
          this.checkManeuver(_mapContainer);
        }else{
          _mapContainer.currentLonLat = _newPos;
        }
      }
    }  
  };
  /*
    Get Bearing between two coordinates
    @parameter
      from: LngLat([lng, lat])
      to: LngLat([lng, lat])
  */
  bearingBetween(_from:any, _to:any) {
      var from = turf.point(_from);
      var to = turf.point(_to);
      var bearing = turf.bearing(from, to);
      return bearing;
  };
  /*
    Get Distance between two coordinates
    @parameter
      from: LngLat([lng, lat])
      to: LngLat([lng, lat])
      unit: String 'miles', 'kilometers'
  */
  distanceBetween(_from:any, _to:any, _unit?:any) {
    var from = turf.point(_from);
    var to = turf.point(_to);
    var unit = _unit ? _unit : 'kilometers';
    return turf.distance(from, to, unit);
  };
  /*
    get overlap features between coordinates and return coordiantes if two direction are same.
  */
  getOverlap(_coordinates1, _coordinates2){
    let line1 = turf.lineString(_coordinates1);
    let line2 = turf.lineString(_coordinates2);
    let featureCollections = turf.lineOverlap(line1, line2);
    var coordinates = [];
    featureCollections.features.forEach((_feature) => {
      _feature.geometry.coordinates.forEach((coor) => {
        coordinates.push(coor);
      });
    });
    if(coordinates.length > 1) {
      var startIndex1, startIndex2, endIndex1, endIndex2;
      for(let i=0; i< _coordinates1.length; i++){
        if(_coordinates1[i][0] == coordinates[0][0] && _coordinates1[i][1] == coordinates[0][1]){
          startIndex1 = i;
        }
        if(_coordinates1[i][0] == coordinates[coordinates.length-1][0] && _coordinates1[i][1] == coordinates[coordinates.length-1][1]){
          endIndex1 = i;
        }
      }
      for(let j=0; j< _coordinates2.length; j++){
        if(_coordinates2[j][0] == coordinates[0][0] && _coordinates2[j][1] == coordinates[0][1]){
          startIndex2 = j;
        }
        if(_coordinates2[j][0] == coordinates[coordinates.length-1][0] && _coordinates2[j][1] == coordinates[coordinates.length-1][1]){
          endIndex2 = j;
        }
      }
      if((startIndex1 < endIndex1) && (startIndex2 < endIndex2)){
        return {coordinates: coordinates, direction: true};
      }else if((startIndex1 > endIndex1) && (startIndex2 > endIndex2)){
        return {coordinates: coordinates, direction: false};
      }else{
        return null;
      }
    }else{
      return null;
    }
  };
  /*
    Add route layer to the map and fit bound to padding
    @parameter
      _id: number(routeId)
      _map: object(map to add route layer)
      _direction: object(directions data from server)
      _padding: object(fit route map padding size)
      _color: string(routeColor #ffffff)
      _flag: boolean(whether fit bound or not)
  */
  addRouteCoordinates(_id, _map, _coordinates, _padding, _color, _flag){
    var routeId = 'route' + _id;
    var coordinates = _coordinates;
     // Geographic coordinates of the LineString
    _map.addLayer({
          "id": routeId,
          "type": "line",
          "source": {
              "type": "geojson",
              "data": {
                  "type": "Feature",
                  "properties": {},
                  "geometry": {
                      "type": "LineString",
                      "coordinates": _coordinates
                  }
              }
          },
          "layout": {
              "line-join": "round",
              "line-cap": "round"
          },
          "paint": {
              "line-color": _color,
              "line-width": {"base": 5, "stops": [[12, 10], [13, 10], [14, 15], [20, 20]]}
          }
      }, 'road_label');
        // Pass the first coordinates in the LineString to `lngLatBounds` &
        // wrap each coordinate pair in `extend` to include them in the bounds
        // result. A variation of this technique could be applied to zooming
        // to the bounds of multiple Points or Polygon geomteries - it just
        // requires wrapping all the coordinates with the extend method.
        if(_flag == true){
          var bounds = coordinates.reduce(function(bounds, coord) {
              return bounds.extend(coord);
          }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
          _map.fitBounds(bounds, {
              padding: _padding
          });
        }
  };
  /*
    Add user markers to the map
    @parameter
      _map: object(map instance)
      _users: array(lngLat array of users)
      _mode: 'driver', 'drifter'
      _title: 
        null-> show nickname below user icon
        'distance'->show distance below user icon
    @return
      markerarray
  */
  addUserMarkers(_map:any, _users:Array<any>, _mode:string, _position?:Array<any>, _title?: string) {
    var _userMarkerArray = [];
    // add user markers to map
    _users.forEach((_user) =>{
      if(_user.mode != 'Driver' || _mode != 'Driver')
      {
        // create a DOM element for the marker
        var el = document.createElement('div');
        if(_user.mode == 'Drifter' && _mode == 'Drifter'){
          el.className = 'drifter-marker';
        }else{
          el.className = 'user-marker';
        }
        /* add start if user is companion*/
        if(_user.isCompanion == true){
          var starImg = document.createElement('img');
          starImg.className = 'star-img';
          starImg.src = 'assets/img/star.png';
          el.appendChild(starImg);
        }
        /* add user profile picture */
        var img = document.createElement('img');
        img.className = 'user-img';
        if(_user.pictureurl != null)
          img.src = _user.pictureurl;
        else
          img.src = this.placeholderUrl;
        el.appendChild(img);
        /* add user's caption*/
        var title = document.createElement('div');
        // if(_title == 'distance'){
        //   let distance = (this.distanceBetween([parseFloat(_user.lon), parseFloat(_user.lat)], _position)).toFixed(1);
        //   title.innerHTML = distance.toString() + 'km';
        // }else{
        //   title.innerHTML = _user.caption;
        // }
        if(_user.commondistance != null && _user.commondistance > 0){
          let distance = (_user.commondistance/1000).toFixed(1);
          title.innerHTML = distance.toString() + 'km';
        }else{
          title.innerHTML = _user.caption;
        }
        title.className = 'marker-title';
        el.appendChild(title);

        el.addEventListener('click', () => {
          let userId = _user.id;
          this.events.publish('userClicked', userId);
        });
        // add marker to map
        var userMarker = new mapboxgl.Marker(el, {offset: [0, 0]})
            .setLngLat([_user.lon, _user.lat])
            .addTo(_map);
        userMarker.id = _user.id;
        _userMarkerArray.push(userMarker);
      }
    });
    return _userMarkerArray;
  };
  /*
    Add destination marker to the map
    @parameter
      _map: object(map instance)
      _destination: lat lon
    @return
      marker instance
  */
  addDestinationMarker(_map, _destination) {
    // create a DOM element for the marker
    var el = document.createElement('div');
    el.className = 'destination-marker';
    var img = document.createElement('img');
    img.className = 'destination-img';
    img.src = this.destinationUrl;
    el.appendChild(img);
    // add marker to map
    var destinationMarker = new mapboxgl.Marker(el, {offset: [0, 0]})
        .setLngLat(_destination)
        .addTo(_map);
    return destinationMarker;
  };
  /*
    Add driftpoint markers to the map
    @parameter
      _map: object(map instance)
      _driftpoints: array(lngLat array of driftpoints)
    @return
      markerarray
  */
  addDriftpointMarkers(_map, _driftpoints) {
    var _driftpointMarkerArray = [];
    // add driftpoints to map
    _driftpoints.forEach((_driftpoint) =>{
      // create a DOM element for the marker
      var el = document.createElement('div');
      el.className = 'driftpoint-marker';
      var img = document.createElement('img');
      img.className = 'driftpoint-img';
      img.src = this.driftPointUrl;
      el.appendChild(img);
      // add marker to map
      var driftMarker = new mapboxgl.Marker(el, {offset: [0, 0]})
          .setLngLat([_driftpoint.lon, _driftpoint.lat])
          .addTo(_map);
       _driftpointMarkerArray.push(driftMarker);
    });
    return _driftpointMarkerArray;
  };
  /*
    Add simple circle marker to the map
    @parameter
      _map: object(map instance)
      _current: array(lngLat of current user)
    @return
      marker
  */
  addCircleMarker(_map, _current, _radius?: number, _color?: string){
    var radius = 15;
    var color = '#8ca8a9'
    if(_radius)
      radius = _radius;
    if(_color)
      color = _color;
    // create a DOM element for the marker
    var el = document.createElement('div');
    el.className = 'circle-marker';
    el.style.backgroundColor = color;
    el.style.width = (radius * 2) + 'px';
    el.style.height = (radius * 2) + 'px';
    // add marker to map
    var circleMarker = new mapboxgl.Marker(el, {offset: [0, 0]})
        .setLngLat([_current[0], _current[1]])
        .addTo(_map);
    return circleMarker;
  };
  /*
    show map in mainpage
    @parameter
      _mapUpdate: boolean-> map is loaded or not
      _userUpdate: boolean-> update current user's position or not
      _otherUpdate: boolean-> update other users' position or not
      _mapOptions: object-> mapbox options
      _dataOptions: object-> required data to show map
  */
  showMainMap(_mapContainer, _mapUpdate, _userUpdate, _otherUpdate, _mapOptions, _dataOptions){
    return new Promise((resolve, reject) => {
      if(_mapUpdate == false) {
        let map = new mapboxgl.Map(_mapOptions);
        _mapContainer.map = map;
        _mapContainer.map.on('load', () => {
          _mapContainer.currentMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.currentlon, _dataOptions.currentlat]);
          _mapContainer.map.fitBounds([[
            _dataOptions.currentlon - 0.1,
            _dataOptions.currentlat - 0.1
          ], [
            _dataOptions.currentlon + 0.1,
            _dataOptions.currentlat + 0.1
          ]]);
          resolve({message: 'map loaded successfully!'});
        });
        _mapContainer.map.on('moveend', ()=> {
          let _center = _mapContainer.map.getCenter();
          let _bounds = _mapContainer.map.getBounds();
          let _diameter = this.distanceBetween([_bounds._ne.lng, _bounds._ne.lat], [_bounds._sw.lng, _bounds._sw.lat]);
          console.log(_diameter);
          //add other users to the map
          this.location.getUsers(_center.lat, _center.lng, (_diameter/2)*1000).then((resusers:any)=> {
            if(_mapContainer.userMarkerArray)
              _mapContainer.userMarkerArray.forEach((umarker) => {umarker.remove()});
            if(resusers)
              _mapContainer.userMarkerArray = this.addUserMarkers(_mapContainer.map, resusers, _dataOptions.mode);
          });
          //add driftpoints to the map
          this.location.getDriftPoints(_center.lat, _center.lng, (_diameter/2)*1000).then((resdrifts:any) => {
            if(_mapContainer.driftpointMarkerArray)
              _mapContainer.driftpointMarkerArray.forEach((dmarker) => {dmarker.remove()});
            if(resdrifts)
              _mapContainer.driftpointMarkerArray = this.addDriftpointMarkers(_mapContainer.map, resdrifts);
          });
        });
      }else if(_otherUpdate == false && _userUpdate == false){
        if(_dataOptions.driftpoints)
          _mapContainer.driftpointMarkerArray = this.addDriftpointMarkers(_mapContainer.map, _dataOptions.driftpoints);
        resolve({message:'add driftpoints successfully!'});
      }else if(_userUpdate == true){
        _mapContainer.currentMarker.setLngLat([_dataOptions.currentlon, _dataOptions.currentlat]);
        //_mapContainer.map.setCenter([_dataOptions.currentlon, _dataOptions.currentlat]);
        resolve({message: 'update current user successfully!'});
      }else if(_otherUpdate == true){
        if(_mapContainer.userMarkerArray)
          _mapContainer.userMarkerArray.forEach((umarker) => {umarker.remove()});
        if(_dataOptions.users)
          _mapContainer.userMarkerArray = this.addUserMarkers(_mapContainer.map, _dataOptions.users, _dataOptions.mode);
        resolve({message: 'update other users successfully!'});
      }
    });
  };
  /*
    show map to select destination
    @parameter
      _mapUpdate: shows that map is loaded or not
      _mapOptions: object-> mapbox options
      _dataOptions: object-> required data to show map
  */
  showSelectMap(_mapContainer, _mapUpdate, _mapOptions, _dataOptions){
    return new Promise((resolve, reject) => {
      if(_mapUpdate == false){
        let map = new mapboxgl.Map(_mapOptions);
        _mapContainer.map = map;
        _mapContainer.map.on('load', () => {
          _mapContainer.currentMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.currentlon, _dataOptions.currentlat]);
          _mapContainer.map.fitBounds([[
            _dataOptions.currentlon - 0.1,
            _dataOptions.currentlat - 0.1
          ], [
            _dataOptions.currentlon + 0.1,
            _dataOptions.currentlat + 0.1
          ]]);
          resolve({message: 'map loaded successfully...'});
        });
        let placeholder = this.translateService.instant('Placeholder.PickDestination');
        _mapContainer.geocoder = new MapboxGeocoder({
          accessToken: mapboxApiKey,
          placeholder: placeholder,
          flyTo: false,
          proximity: {longitude: _dataOptions.currentlon, latitude: _dataOptions.currentlat},
          language: systemOptions.systemLanguage,
          limit: 8
          //country: this.auth.getUserInfo().countrycode
        });
        /*
          Fired when input is set
        */
        _mapContainer.geocoder.on('result', (res) =>{
          if(_mapContainer.destinationMarker !== null){
            _mapContainer.destinationMarker.setLngLat(res.result.geometry.coordinates);
          }else{
            _mapContainer.destinationMarker = this.addDestinationMarker(_mapContainer.map, res.result.geometry.coordinates);
          }
          _mapContainer.destination = res.result;
          _mapContainer.map.easeTo({center: res.result.geometry.coordinates, offset: [0, -120]});
          setTimeout(()=> {
            var suggestion = document.querySelector('.mapboxgl-ctrl-geocoder .suggestions') as HTMLElement;
            suggestion.style.height = '0px';
          }, 100);
        });
        /*
          Emitted when the geocoder is looking up a query
        */
        _mapContainer.geocoder.on('loading', (res) => {
          _mapContainer.destination = null;
          var suggestion = document.querySelector('.mapboxgl-ctrl-geocoder .suggestions') as HTMLElement;
          suggestion.style.height = '0px';
        });
        /*
          Fired when the geocoder returns a response
        */
        _mapContainer.geocoder.on('results', (res) => {
          var suggestion = document.querySelector('.mapboxgl-ctrl-geocoder .suggestions') as HTMLElement;
          suggestion.style.height = '90px';
        });
        _mapContainer.map.addControl(_mapContainer.geocoder);
      }else{
        _mapContainer.currentMarker.setLngLat([_dataOptions.currentlon, _dataOptions.currentlat]);
        // _mapContainer.map.setCenter([_dataOptions.currentlon, _dataOptions.currentlat]);
        resolve({message:'update user successfully...'});
      }
    });
  };
  /*
    show destination map
    @parameter
      _mapUpdate: boolean-> map is loaded or not
      _userUpdate: boolean-> update current user's position or not
      _otherUpdate: boolean-> update other users' position or not
      _mapOptions: object-> mapbox options
      _dataOptions: object-> required data to show map
  */
  showDestinationMap(_mapContainer, _mapUpdate, _userUpdate, _otherUpdate, _mapOptions, _dataOptions){
    return new Promise((resolve, reject) => {
      if(_mapUpdate == false) {
        let map = new mapboxgl.Map(_mapOptions);
        _mapContainer.map = map;
        _mapContainer.map.on('load', () => {
          _mapContainer.currentMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.currentlon, _dataOptions.currentlat]);
          _mapContainer.map.fitBounds([[
            _dataOptions.currentlon - 0.1,
            _dataOptions.currentlat - 0.1
          ], [
            _dataOptions.currentlon + 0.1,
            _dataOptions.currentlat + 0.1
          ]]);
          if(_dataOptions.destinationlat != null && _dataOptions.destinationlon != null){
            _mapContainer.destinationMarker = this.addDestinationMarker(_mapContainer.map, [_dataOptions.destinationlon, _dataOptions.destinationlat]);
            this.getDirections(_dataOptions.currentlon, _dataOptions.currentlat, _dataOptions.destinationlon, _dataOptions.destinationlat, true).then((res: any)=> {
              let coordinates = res.routes[0].geometry.coordinates;
              this.addRouteCoordinates(_mapContainer.idRoute, _mapContainer.map, coordinates, 80, _dataOptions.routeColor, true)
            });
          }
          resolve({message:'map loaded successfully...'});
        });
      }else if(_otherUpdate == false && _userUpdate == false){
        if(_dataOptions.driftpoints)
          _mapContainer.driftpointMarkerArray = this.addDriftpointMarkers(_mapContainer.map, _dataOptions.driftpoints);
        resolve({message:'add driftpoints successfully...'});
      }else if(_userUpdate == true){
        _mapContainer.currentMarker.setLngLat([_dataOptions.currentlon, _dataOptions.currentlat]);
        //_mapContainer.map.setCenter([_dataOptions.currentlon, _dataOptions.currentlat]);
        resolve({message:'update current user successfully...'});
      }else if(_otherUpdate == true){
        if(_mapContainer.userMarkerArray)
          _mapContainer.userMarkerArray.forEach((umarker) => {umarker.remove()});
        if(_dataOptions.users)
          _mapContainer.userMarkerArray = this.addUserMarkers(_mapContainer.map, _dataOptions.users, _dataOptions.mode);
        resolve({message:'update otherusers successfully...'});
      }
    });
  };
  /*
    show drift map that have two routes
    @parameter
      _mapUpdate: boolean-> map is loaded or not
      _userUpdate: boolean-> update current user's position or not
      _otherUpdate: boolean-> update other users' position or not
      _mapOptions: object-> mapbox options
      _dataOptions: object-> required data to show map
  */
  showDriftersRouteMap(_mapContainer, _mapOptions, _dataOptions){
    return new Promise((resolve, reject) => {
      let map = new mapboxgl.Map(_mapOptions);
      _mapContainer.map = map;
      _mapContainer.map.on('load', () => {
        _mapContainer.currentMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.currentlon, _dataOptions.currentlat]);
        _mapContainer.otherMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.otherlon, _dataOptions.otherlat], 10, _dataOptions.otherRouteColor);
        if(_dataOptions.destinationlat != null && _dataOptions.destinationlon != null){
          _mapContainer.destinationMarker = this.addDestinationMarker(_mapContainer.map, [_dataOptions.destinationlon, _dataOptions.destinationlat]);
          this.getDirections(_dataOptions.currentlon, _dataOptions.currentlat, _dataOptions.destinationlon, _dataOptions.destinationlat, true).then((res:any)=> {
            let userCoordinates = res.routes[0].geometry.coordinates;
            this.addRouteCoordinates(_mapContainer.idCurrentRoute, _mapContainer.map, userCoordinates, 30, _dataOptions.routeColor, true)
            if(_dataOptions.otherdestinationlat != null && _dataOptions.otherdestinationlon != null){
              _mapContainer.otherDestinationMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.otherdestinationlon, _dataOptions.otherdestinationlat], 10, _dataOptions.otherRouteColor);
              this.getMapboxDirections(_dataOptions.otherlon, _dataOptions.otherlat, _dataOptions.otherdestinationlon, _dataOptions.otherdestinationlat, 'true', 'full').then((res:any)=> {
                let otherCoordinates = res.routes[0].geometry.coordinates;
                this.addRouteCoordinates(_mapContainer.idOtherRoute, _mapContainer.map, otherCoordinates, 30, _dataOptions.otherRouteColor, false);
                let overlap = this.getOverlap(userCoordinates, otherCoordinates);
                if(overlap != null){
                  var togetherStartPos;
                  var togetherEndPos;
                  if(overlap.direction == true){
                    togetherStartPos = overlap.coordinates[0];
                    togetherEndPos = overlap.coordinates[overlap.coordinates.length-1];
                  }else{
                    togetherEndPos = overlap.coordinates[0];
                    togetherStartPos = overlap.coordinates[overlap.coordinates.length-1];
                  }
                  this.getMapboxDirections(togetherStartPos[0], togetherStartPos[1], togetherEndPos[0], togetherEndPos[1], 'true', 'full').then((res:any)=> {
                    this.addRouteCoordinates(3, _mapContainer.map, res.routes[0].geometry.coordinates, 30, '#5cb85c', false);
                    resolve({message:'map loaded successfully...', distance: res.routes[0].distance});
                  });
                }else {
                  resolve({message:'map loaded successfully...', distance: 0});
                }
                
              });
            }else {
              resolve({message:'map loaded successfully...', distance: 0});
            }
          });
        }else{
          if(_dataOptions.otherdestinationlat != null && _dataOptions.otherdestinationlon != null){
            _mapContainer.otherDestinationMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.otherdestinationlon, _dataOptions.otherdestinationlat], 10, _dataOptions.otherRouteColor);
            this.getMapboxDirections(_dataOptions.otherlon, _dataOptions.otherlat, _dataOptions.otherdestinationlon, _dataOptions.otherdestinationlat, 'true', 'full').then((res:any)=> {
              let coordinates = res.routes[0].geometry.coordinates;
              this.addRouteCoordinates(_mapContainer.idOtherRoute, _mapContainer.map, coordinates, 30, _dataOptions.otherRouteColor, true);
              resolve({message:'map loaded successfully...', distance: 0});
            });
          }else{
            resolve({message:'map loaded successfully...', distance: 0});
          }
        } 
      });
    });
  };
  /*
    show navigation map
    @parameter
      _mapContainer: object -> map container
      _mapUpdate: boolean-> map is loaded or not
      _userUpdate: boolean-> update current user's position or not
      _otherUpdate: boolean-> update other users' position or not
      _mapOptions: object-> mapbox options
      _dataOptions: object-> required data to show map
  */
  showNavigationMap(_mapContainer, _mapUpdate, _userUpdate, _otherUpdate, _mapOptions, _dataOptions){
    return new Promise((resolve, reject) => {
      if(_mapUpdate == false) {//load map with mapoptions and dataoptions
        let map = new mapboxgl.Map(_mapOptions);
        _mapContainer.map = map;
        _mapContainer.startLonLat = [_dataOptions.currentlon, _dataOptions.currentlat];
        _mapContainer.destinationLonLat = [_dataOptions.destinationlon, _dataOptions.destinationlat];
        _mapContainer.currentLonLat = [_dataOptions.currentlon, _dataOptions.currentlat];
        _mapContainer.map.on('load', () => {
          _mapContainer.currentMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.currentlon, _dataOptions.currentlat]);
          _mapContainer.destinationMarker = this.addDestinationMarker(_mapContainer.map, [_dataOptions.destinationlon, _dataOptions.destinationlat]);
          this.getDirections(_dataOptions.currentlon, _dataOptions.currentlat, _dataOptions.destinationlon, _dataOptions.destinationlat, true).then((res: any)=> {
            _mapContainer.directions = res;
            let coordinates = res.routes[0].geometry.coordinates;        
            this.addRouteCoordinates(_mapContainer.idRoute, _mapContainer.map, coordinates, 50, '#32adfe', false);
            this.initStep(_mapContainer);
            _mapContainer.map.setCenter(_mapContainer.currentLonLat);
            _mapContainer.map.setBearing(this.bearingBetween(_mapContainer.currentLonLat, coordinates[0]));
            resolve({message: 'map loaded successfully...', coordinates:coordinates});
          }, (error)=> {
            reject({message: 'faild...'});
          });
        });  
      }else if(_otherUpdate == false && _userUpdate == false){//add driftpoints to map
        if(_dataOptions.driftpoints)
          _mapContainer.driftpointMarkerArray = this.addDriftpointMarkers(_mapContainer.map, _dataOptions.driftpoints);
        resolve({message:'add driftpoints successfully...'});
      }else if(_userUpdate == true){//update current user
        _mapContainer.currentMarker.setLngLat([_dataOptions.currentlon, _dataOptions.currentlat]);
        _mapContainer.map.setCenter([_dataOptions.currentlon, _dataOptions.currentlat]);
        _mapContainer.map.setBearing(this.bearingBetween(_mapContainer.currentLonLat, [_dataOptions.currentlon, _dataOptions.currentlat]));
        let moveDistance = this.distanceBetween(_mapContainer.currentLonLat, [_dataOptions.currentlon, _dataOptions.currentlat]);
        if(moveDistance > 0.002){
          this.checkNavigation(_mapContainer, [_dataOptions.currentlon, _dataOptions.currentlat]);
        }
        resolve({message:'update user successfully...'});
      }else if(_otherUpdate == true){//update other users
        if(_mapContainer.userMarkerArray)
          _mapContainer.userMarkerArray.forEach((umarker) => {umarker.remove()});
        if(_dataOptions.users)
          _mapContainer.userMarkerArray = this.addUserMarkers(_mapContainer.map, _dataOptions.users, _dataOptions.mode);
        resolve({message:'update otherusers successfully...'});
      }
    });
  };
  /*
    show user destination map
    @parameter
      _mapContainer: object -> map container
      _mapUpdate: boolean-> map is loaded or not
      _userUpdate: boolean-> update current user's position or not
      _otherUpdate: boolean-> update other users' position or not
      _mapOptions: object-> mapbox options
      _dataOptions: object-> required data to show map
  */
  showUserDestinationMap(_mapContainer, _mapUpdate, _userUpdate, _otherUpdate, _mapOptions, _dataOptions){
    return new Promise((resolve, reject) => {
      if(_mapUpdate == false) {//load map with mapoptions and dataoptions
        let map = new mapboxgl.Map(_mapOptions);
        _mapContainer.map = map;
        _mapContainer.startLonLat = [_dataOptions.currentlon, _dataOptions.currentlat];
        _mapContainer.destinationLonLat = [_dataOptions.other.lon, _dataOptions.other.lat];
        _mapContainer.otherLonLat = [_dataOptions.other.lon, _dataOptions.other.lat];
        _mapContainer.endLonLat = [_dataOptions.destinationlon, _dataOptions.destinationlat];
        _mapContainer.currentLonLat = [_dataOptions.currentlon, _dataOptions.currentlat];
        _mapContainer.map.on('load', () => {
          _mapContainer.currentMarker = this.addCircleMarker(_mapContainer.map, [_dataOptions.currentlon, _dataOptions.currentlat]);
          _mapContainer.destinationMarker = this.addDestinationMarker(_mapContainer.map, _mapContainer.endLonLat);
          this.getDirections(_dataOptions.currentlon, _dataOptions.currentlat, _dataOptions.other.lon, _dataOptions.other.lat, true).then((res: any)=> {
            _mapContainer.directions = res;
            let coordinates = res.routes[0].geometry.coordinates;        
            this.addRouteCoordinates(_mapContainer.idRoute, _mapContainer.map, coordinates, 50, '#32adfe', false);
            this.initStep(_mapContainer);
            _mapContainer.map.setCenter(_mapContainer.currentLonLat);
            _mapContainer.map.setBearing(this.bearingBetween(_mapContainer.currentLonLat, coordinates[0]));
            resolve({message: 'map loaded successfully...', coordinates:coordinates});
          }, (error)=> {
            reject({message: 'faild...'});
          });
        });  
      }else if(_otherUpdate == false && _userUpdate == false){//add driftpoints to map
        if(_dataOptions.driftpoints)
          _mapContainer.driftpointMarkerArray = this.addDriftpointMarkers(_mapContainer.map, _dataOptions.driftpoints);
        resolve({message:'add driftpoints successfully...'});
      }else if(_userUpdate == true){//update current user
        _mapContainer.currentMarker.setLngLat([_dataOptions.currentlon, _dataOptions.currentlat]);
        _mapContainer.map.setCenter([_dataOptions.currentlon, _dataOptions.currentlat]);
        _mapContainer.map.setBearing(this.bearingBetween(_mapContainer.currentLonLat, [_dataOptions.currentlon, _dataOptions.currentlat]));
        let moveDistance = this.distanceBetween(_mapContainer.currentLonLat, [_dataOptions.currentlon, _dataOptions.currentlat]);
        if(moveDistance > 0.002){
          this.checkNavigation(_mapContainer, [_dataOptions.currentlon, _dataOptions.currentlat]);
        }
        resolve({message:'update user successfully...'});
      }else if(_otherUpdate == true){//update other users
        if(_mapContainer.userMarkerArray)
          _mapContainer.userMarkerArray.forEach((umarker) => {umarker.remove()});
        if(_dataOptions.users)
          _mapContainer.userMarkerArray = this.addUserMarkers(_mapContainer.map, _dataOptions.users, _dataOptions.mode);
        resolve({message:'update otherusers successfully...'});
      }
    });
  };
}