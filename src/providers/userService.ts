import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { Events } from 'ionic-angular';

@Injectable()
export class UserService {
  constructor(
    private api: ApiService,
    private events: Events){
  };
  /*
    get user's profile information
  */
  getUserInfo(id) {
    return new Promise((resolve, reject) => {
      this.api.users_id(id).subscribe((res) => {
        let response = res.json();
        if(response.message != 'User not found'){
          resolve(response);
        }else{
          resolve(null);
        }
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get user error...');
      });
    });
  };
  /*
    get user's reviews
    @parameter
      id: userid,
      resultsperpage: optional
      pagenumber: optional
  */
  getReviews(id: number, resultsperpage?: number, pagenumber?:number) {
    let request = {
      resultsperpage: resultsperpage,
      pagenumber: pagenumber
    };
    return new Promise((resolve, reject) => {
      this.api.users_id_reviews(id, request).subscribe((res) => {
        let response = res.json();
        if(response.reviews)
          resolve(response.reviews);
        else
          reject([]);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get companions error...');
      });
    });
  };
  /*
    get user's companions
    @parameter
      id: userid,
      itemcount: optional,
      pagenumber: optional
  */
  getCompanions(id: number, itemcount?: number, pagenumber?:number) {
    let request = {
      itemcount: itemcount,
      pagenumber: pagenumber
    };
    return new Promise((resolve, reject) => {
      this.api.users_id_companions(id, request).subscribe((res) => {
        let response = res.json();
        if(response.companions)
          resolve(response.companions);
        else
          resolve([]);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get companions error...');
      });
    });
  };
  /*
    get user's drifts
    @parameter
  */
  getDrifts(id:number, statuses?:string, date_until?:string, itemcount?: number){
    let request = {
      statuses: statuses,
      date_untill: date_until,
      itemcount: itemcount
    };
    return new Promise((resolve, reject) => {
      this.api.users_id_drifts(id, request).subscribe((res) => {
        let response = res.json();
        if(response.drifts)
          resolve(response.drifts);
        else
          resolve([]);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get drifts error...');
      })
    });
  };
  /*
    search people with query
  */
  searchPeople(query: string, itemcount?: number, pagenumber?: number) {
    let request = {
      searchtext: query,
      itemcount: itemcount,
      pagenumber: pagenumber
    };
    return new Promise((resolve, reject) => {
      this.api.users_search(request).subscribe((res) => {
        let response = res.json();
        resolve(response.companions);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('search people error...');
      });
    })
  };
  /*
    send message to other
  */
  sendMessage(id:number, message:string){
    return new Promise((resolve, reject) => {
      this.api.users_id_message(id, message).subscribe((res) => {
        resolve('send message successfully...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('send message error...');
      });
    });
  };
  /*
    send companion request
  */
  sendCompanionRequest(id: number) {
    return new Promise((resolve, reject) => {
      this.api.users_id_companions_request(id).subscribe((res) => {
        resolve('companion request sucessfully...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        console.log('companion request error...');
        let error = err.json();
        reject(error.message);
      });
    });
  };
  /*
    block companion
  */
  blockCompanion(id: number) {
    return new Promise((resolve, reject) => {
      this.api.users_me_companions_id_block(id).subscribe((res) => {
        resolve('companion block sucessfully...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('companion block error...');
      });
    });
  };
  /*
    block companion
  */
  unblockCompanion(id: number) {
    return new Promise((resolve, reject) => {
      this.api.users_me_companions_id_unblock(id).subscribe((res) => {
        resolve('companion unblock sucessfully...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('companion unblock error...');
      });
    });
  };
};