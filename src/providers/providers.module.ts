import { NgModule } from '@angular/core';
import { RequestService } from './requestService';
import { ApiService } from './apiService';
import { AuthService } from './authService';
import { UserService } from './userService';
import { UtilService } from './utilService';
import { MapboxService } from './mapboxService';
import { LocationService } from './locationService';
import { SettingService } from './settingService';
import { ChatService } from './chatService';
import { BadgeService } from './badgeService';
@NgModule({
  declarations: [

  ],
  providers: [
    RequestService,
    ApiService,
    UserService,
    AuthService,
    UtilService,
    MapboxService,
    LocationService,
    SettingService,
    ChatService,
    BadgeService
  ],
})
export class ProvidersModule {}