import { Injectable } from '@angular/core';
import { RequestService } from './requestService';
import { systemOptions } from '../app/app.config';
@Injectable()
export class ApiService {
  token: String;
  constructor(
    private requestService: RequestService) {
      if(localStorage.getItem('drift_token')){
        this.token = localStorage.getItem('drift_token');
      }
  };
  /*
    Set token 
  */
  setToken(tokenString) {
    this.token = tokenString;
  };
  /*
    get token
  */
  getToken() {
    return this.token;
  };
  /*
    POST /login
    Log the user in and returns a session token
    Arguments:  email       E-Mail-Address
                password    The password
    Response:   {"token": "e2gc3UYGbIEz7CRDlWZn"}
    Process:
    - retrieve user from database, return error if not found
    - validate password (SHA1 hash), return error if not found
    - create session token record and return token
  */
  login(credentials){
    let requestData = {
      email: credentials.email,
      password: credentials.password
    };
    return this.requestService.postRequest('/login', requestData);
  };
  /*
    POST /logout
    Logs the user out and invalidates the session token
    Arguments:  token       session token
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - delete the usersession record
  */
  logout() {
    let requestData = {
      token: this.token,
      devicetoken: localStorage.getItem('drift_devicetoken')
    };
    return this.requestService.postRequest('/logout', requestData);
  };
  /*
    GET /countries
    Returns a list of countries
    Arguments:  -
    Response:   {"countries": [{"code":"DE", "text":"Germany"}, {"code":"CH", "text":"Switzerland"}, null, {"code":"AF", "text":"Afghanistan"}, ...]}
    Process:
    - return a list of countries from resources/languages/...
    - the list contains 'frequently used' countries on the top, a null entry as delimiter followed by all other countries
  */
  countries() {
    return this.requestService.getRequest('/countries?lang=' + systemOptions.systemLanguage);
  };
  /*
    POST /login/checknickname
    Check if Nickname is available
    Arguments:  nickname
    Response:   {"available": true}
    - Check whether the given nickname is available (not yet present in users table) and return the result
  */
  login_checknickname(nickname){
    let requestData = {
      nickname: nickname
    };
    return this.requestService.postRequest('/login/checknickname' , requestData);
  };
  /*
    POST /login/checkemail
    Check if E-Mail is available
    Arguments: E-Mail
    Response:  {"available": true}
    Return: boolean true = email available to use
    Process:
    - Check whether the given email address is available (not yet present in users table) and return the result
  */
  login_checkemail(email){
    let requestData = {
      email: email
    };
    return this.requestService.postRequest('/login/checkemail' , requestData);
  };
  
  /*
    POST/register
    Register as a new user
    Arguments:  gender        User's gender "male" | "female" | "open"
                firstname     first name
                lastname      surname
                nickname      nickname or user name (optional)
                email         email address
                mobile        mobile phone number
                address       address
                zip           ZIP code
                countrycode   country code (see /countries)
                birthdate     date of birth
                hasdriverslicense   boolean whether user has a driver's license
                password      The password
    Return: boolean success, message if unsuccessful
    Process:
    - validate columns, all mandatory except nickname
    - create a new entry in users table
  */
  register(registerData) {
    return this.requestService.postRequest('/register', registerData);
  };
  /*
    POST /users/me/profilepicture
    Set Profile Picture
    Arguments:  token     session token
                picture   base64 image
    Response:   success
    - validate the session token, return authentication error if invalid
    - save the image and generate a public url
    - update the profile picture in users table
  */
  users_me_profilepicture(picture){
    let requestData = {
      token: this.token,
      picture: picture
    };
    return this.requestService.postRequest('/users/me/profilepicture', requestData);
  };
  /*
    POST /users/me/userpicture
    Upload additional picture
    Arguments:  token     session token
                picture   base64 image
    Response:   {"picture_id":123}
    - validate the session token, return authentication error if invalid
    - save the image and generate a public url
    - add a new entry in userpictures table
    - return the id of the newly created userpictures record
  */
  post_users_me_userpicture(picture) {
    let requestData = {
      token: this.token,
      picture: picture
    };
    return this.requestService.postRequest('/users/me/userpicture', requestData);
  };
  /*
    DELETE /users/me/userpicture
    Arguments:  token       session token
                picture_id  id of the user picture
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that the userpictures record with id=picture_id exists and it's user_id is the currently logged in user; return error if not
    - delete the userpictures entry
  */
  delete_users_me_userpicture(pictureId) {
    let requestData = {
      token: this.token,
      picture_id: pictureId
    };
    return this.requestService.deleteParamRequest('/users/me/userpicture', requestData);
  };
  /*
    POST /users/me/vehicles/update
    Add vehicle Info. Currently there's no user interface to manage multiple vehicles. Therefore this method adds a vehicle entry if none exists for the user or updates the existing one.
    Arguments:  token         session token
                platenumber   license plate number
                carmodel      car model
                picture       base64 image
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - update the existing vehicles entry or create a new one
    - set users.hasvehicle to true
  */
  users_me_vehicles_update(vehicle){
    let requestData = {
      token: this.token,
      platenumber: vehicle.platenumber,
      carmodel: vehicle.carmodel,
      picture: vehicle.picture
    };
    return this.requestService.postRequest('/users/me/vehicles/update', requestData);
  };
  /*
    POST /users/me/vehicles/havenone
    Declare not having any vehicle
    Arguments:  token, { 'providelater' | 'havenoride' }
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - set users.hasvehicle to false
  */
  users_me_vehicles_havenone(mode){
    var requestData = {};
    if (mode == 0) {
      requestData = {
        token: this.token,
        providelater: true
      };
    } else {
      requestData = {
        token: this.token,
        havenoride: true
      };
    }
    return this.requestService.postRequest('/users/me/vehicles/havenone', requestData);
  };
  /*
    POST /users/me/about
    Set About Text
    Arguments:  token     session token
                text      about text (max. 180 chars)
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - save text in users.about
  */
  users_me_about(text){
    let requestData = {
      token: this.token,
      text: text
    };
    return this.requestService.postRequest('/users/me/about', requestData);
  };
  /*
    POST /users/me/perks
    Set 'perks'
    Arguments:  token      session token
                perks      comma separated string of integers (see users.perks enumeration)
    Response:   -
    Process:
    - validate the session toke, return authentication error if invalid
    - save perks in users.perks
  */
  users_me_perks(perks) {
    let requestData = {
      token: this.token,
      perks: perks
    };
    return this.requestService.postRequest('/users/me/perks', requestData);
  };
  /*
    POST /users/me/emergencycontacts
    Set emergency contacts
    Arguments:  token      session token
                contact1   user id of the first emergency contact
                contact2   user id of the second emergency contact
    Response:   -
    Process:
    - validate the session toke, return authentication error if invalid
    - save emergency contacts in users record
  */
  users_me_emergencycontacts(contacts) {
    let requestData = {
      token: this.token,
      contact1: contacts.contact1,
      contact2: contacts.contact2
    };
    return this.requestService.postRequest('/users/me/emergencycontacts', requestData);
  };
  /*
    GET /users/me
    Read my own profile
    Arguments:  token     session token
    Response:   {"id":1, "gender": "female", "firstname": "Sharon", "lastname": "Stone", "nickname":"sharon1", "email":"s@s.com", "mobile":"123", "address":"", "zip":"XY12345", "country":{"DE", "Germany"}, "birthdate":1234, "hasdriverslicense":true, "hasvehicle":true, "vehicle":{"platenumber":"", "carmodel":"", "pictureurl":""}, "about":"Just me", "pictureurl":"https://cdn.net/1234","invisiblemode":false,"companionmode":true, "upvotes":12, "downvotes":0, ”companions”:15, “destinationlon”:12.34, "destinationlat":12.34, "destinationcaption":"Berlin", "mode":"driver|drifter"}
    Process:
    - validate the session token, return authentication error if invalid
    - return user's profile data including the latest vehicle entry (if present)
  */
  get_users_me(){
    return this.requestService.getRequest('/users/me?token=' + localStorage.getItem('drift_token'));
  };
  /*
    POST /users/me
    Update my profile
    Arguments:  token               session token
                gender              gender (male | femle | open)
                firstname           first name
                lastname            surname
                nickname            user name (optional)
                email               email address
                mobile              mobile phone number
                address             address
                zip                 ZIP code
                countrycode         country code
                birthdate           birth date
                hasDriversLicense   (bool)
    - validate the session token, return authentication error if invalid
    - update user's profile information in users table
  */
  post_users_me(postData){
    return this.requestService.postRequest('/users/me', postData);
  };
  /*
    POST /changepassword
    Change Password
    Arguments:  token         session token
                oldpassword   old password
                newpassword   new password
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - verify old password (hash), return error if invalid
    - set new password (hash)
  */
  changepassword(passwords) {
    let requestData = {
      token: this.token,
      oldpassword: passwords.oldpassword,
      newpassword: passwords.newpassword
    };
    return this.requestService.postRequest('/changepassword', requestData);
  };
  /*
    GET /users/{id}
    Get other user's profile (less info than own profile)
    Arguments:  token     session token
    Result:     {"id":1, "gender": "female", "nickname":"sharon1", "mode":"driver", "vehicle":{"platenumber":"", "carmodel":"", "pictureurl":""}, "about":"Just me", "pictureurl":"https://cdn.net/1234", "pictures":["http://...", "http://..."], "upvotes":23, "downvotes":0, "companions":5, "isCompanion":1, "lat":12.34, "lon":12.34}
    - validate the session token, return authentication error if invalid
    - load the user with {id}, return error if not found
    - return user's profile along with the latest vehicle and number of votes and number of companions
    - for "nickname" response attribute use users.nickname if present, otherwise users.firstname
    - only return response fields 'lat' / 'lon' if
    - users.invisiblemode = false && (!isCompanion || users.companionmode = true) (users referring to the user {id})
  */
  users_id(id) {
    return this.requestService.getRequest('/users/' + id + '?token=' + this.token);
  };
  /*
    GET /users/{id}/location
    Get user's location
    Arguments: Token, User ID
    Return: { user id, lon, lat }
    - validate the session token, return authentication error if invalid
    - if users.invisiblemode = true return no data (403 Forbidden)
    - return latest location
  */
  users_id_location(id) {
    return this.requestService.getRequest('/users/' + id + '/location?token=' + this.token);
  };
  /*
    POST /users/me/mode
    Set current mode
    Arguments:  token     session token
                mode      'driver' or 'drifter'
    Response:   -
    - validate the session token, return authentication error if invalid
    - if mode argument is 'driver' verify that a vehicle is present, return error if not
    - update users.mode
  */
  users_me_mode(mode){
    let requestData = {
      token: this.token,
      mode: mode
    };
    return this.requestService.postRequest('/users/me/mode', requestData);
  };
  /*
    POST /users/me/companionmode
    Set companion mode
    Arguments:  token     session token
                value     true | false
    Response:   -
    - validate the session token, return authentication error if invalid
    - update users.companionmode
  */
  users_me_companionmode(value) {
    let requestData = {
      token: this.token,
      value: value
    };
    return this.requestService.postRequest('/users/me/companionmode', requestData);
  };
  /*
    POST /users/me/invisiblemode
    Set invisible mode
    Arguments:  token     session token
                value     true | false
    Response:   -
    - validate the session token, return authentication error if invalid
    - update users.invisiblemode
  */
  users_me_invisiblemode(value) {
    let requestData = {
      token: this.token,
      value: value
    };
    return this.requestService.postRequest('/users/me/invisiblemode', requestData);
  };
  /*
    GET /users/{id}/companions
    Get list of companions
    Arguments:  token           session token
                itemcount       result item count (optional default 10)
                pagenumber      a page number (optional)
    Response:   {"companions": [{"user_id":2, "nickname":"fred", "pictureurl":"https://cdn.net/2"}, ...]}
    - validate the session token, return authentication error if invalid
    - return all companions of given user
    - for "nickname" response attribute use users.nickname if present, otherwise users.firstname
    - do pagination
  */
  users_id_companions(id, request) {
    let requestData = {
      token: this.token,
      itemcount: request.itemcount,
      pagenumber: request.pagenumber
    };
    return this.requestService.getParamRequest('/users/' + id + '/companions', requestData);
  };
  /*
    POST /users/{id}/message
    Send a text message to a user
    Arguments:  token           session token
                message         message text
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that no record exists in 'userblock' table for user_id = {id} and blockuser_id = logged in user, if it does return error
    - send the message using Guapp service
  */
  users_id_message(id, message) {
    let requestData = {
      token: this.token,
      message: message
    };
    return this.requestService.postRequest('/users/' + id + '/message', requestData);
  };
  /*
    GET /users/search
    Search other users
    Arguments:  token           session token
                searchtext      search text
                itemcount       result item count (optional default 10)
                pagenumber      a page number (optional)
    Response:   {"companions": [{"user_id":2, "nickname":"fred", "pictureurl":"https://cdn.net/2"}, ...]}
    - validate the session token, return authentication error if invalid
    - filter users by searchtext (maching in nickname, firstname or lastname)
    - for "nickname" response attribute use users.nickname if present, otherwise users.firstname
    - do pagination
  */
  users_search(request) {
    let requestData = {
      token: this.token,
      searchtext: request.searchtext,
      itemcount: request.itemcount,
      pagenumber: request.pagenumber
    };
    return this.requestService.getParamRequest('/users/search', requestData);
  };
  /*
    GET /users/{id}/reviews
    Get a user's reviews
    Arguments: token            session token
               resultsperpage   number of reviews to return
               pagenumber       result page to return
    Response:  {"reviews": [ { datetime:123456, "drift_id":123, "reviewby":345, updown:"up", text:"We had a great time, thanks!" }, ... ]}
    Process:
    - validate the session token, return authentication error if invalid
    - validate that the given user exists, return error if not
    - return list of reviews according to paging arguments
  */
  users_id_reviews(id, request) {
    let requestData = {
      token: this.token,
      resultsperpage: request.resultsperpage,
      pagenumber: request.pagenumber
    };
    return this.requestService.getParamRequest('/users/' + id + '/reviews', requestData);
  };
  /*
    POST /drifts/{drift_id}/review
    Post a review on a drift/user
    Arguments:  token           session token
                updown          "up" or "down" upvote or downvote
                text            review text
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - validate that the given drift exists, return error if not
    - validate that the current user is participant of the given drift, return error if not
    - as user_id for the review choose the other party of the drift
    - make sure no review for that drift and user about to be reviewed has been stored yet, return error otherwise
    - save new review
  */
  drifts_id_review(id, request) {
    let requestData = {
      token: this.token,
      updown: request.updown,
      text: request.text
    };
    return this.requestService.postRequest('/drifts/' + id + '/review', requestData);
  };
  /*
    GET /users/{user_id}/drifts
    Get drifts (journeys)
    Arguments:  token             session token
                statuses          optional comma-separated list of statuses to filter: "agreed", running, "completed" (if absent, all statuses are returned)
                date-until        returns results <=date-until descending
                itemcount         number of items to return
    Response:   {"drifts": [ { "drift_id":12345, "driver_id":123, "drifter_id":124, "status":"agreed", "created_at":date, "startdate":datetime, "startlat":lat, "startlon":lon, "startcaption":"New York", "enddate":datetime, "endlat":lat, "endlon":lon, endcaption:"California", "coverpictureurl":"http://test.com/photos/123" }, ... ] }
    - validate the session token, return authentication error if invalid
    - validate that the given user exists, return error if not
    - return all drifts for the given user, add the first drift photo as cover picture (if available)
  */
  users_id_drifts(id, request){
    let requestData = {
      token: this.token,
      statuses: request.statuses,
      date_until: request.date_until,
      itemcount: request.itemcount
    };
    return this.requestService.getParamRequest('/users/' + id + '/drifts', requestData);
  };
  /*
    GET /users/{id}/photos
    Get user's photos (related to drifts)
    Arguments:  token             session token
                itemcount         result item count (optional default 10)
                pagenumber        a page number (optional)
    Response:   { "user_id":123, "photos": [{"id":1234, "timestamp":1234, "lon":1, "lat":1, "url": "https://cdn.org/1234", "caption":""}, ...] }
    Process:
    - validate the session token, return authentication error if invalid
    - validate that the given user exists, return error if not
    - return a list of photos for given user, that is for all photos on whose drift the user is participant either as driver or drifter (sort by timestamp descending, do pagination)
  */
  users_id_photos(id, request){
    let requestData = {
      token: this.token,
      itemcount: request.itemcount,
      pagenumber: request.pagenumber
    };
    return this.requestService.getParamRequest('/users/' + id + '/photos', requestData);
  };
  /*
    GET /drifts/{drift_id}
    Get information on a single drift (journey)
    Arguments:  token             session token
    Response:   { "drift_id":12345, "driver_id":123, "drifter_id":124, "status":"agreed", "created_at":date, "startdate":datetime, "startlat":lat, "startlon":lon, "startcaption":"New York", "enddate":datetime, "endlat":lat, "endlon":lon, endcaption:"California", "coverpictureurl":"http://test.com/photos/123" }
    - validate the session token, return authentication error if invalid
    - validate that the given user exists, return error if not
    - return the drift with the given id, add the first drift photo as cover picture (if available)
  */
  drifts_id(id){
    return this.requestService.getRequest('/drifts/' + id + '?token=' + this.token);
  }
  /*
    GET /drifts/{drift_id}/photos
    Get photos for a drift
    Arguments:  token             session token
                itemcount         result item count (optional default 10)
                pagenumber        a page number (optional)
    Response:   { "drift_id":123, "photos": [{"id":1234, "timestamp":1234, "lon":1, "lat":1, "url": "https://cdn.org/1234", "caption":""}, ...] }
    Process:
    - validate the session token, return authentication error if invalid
    - validate that the given drift exists, return error if not
    - return a list of photos for given drift (sort by timestamp descending, do pagination)
  */
  drifts_id_photos(id, request) {
    let requestData = {
      token: this.token,
      itemcount: request.itemcount,
      pagenumber: request.pagenumber
    };
    return this.requestService.getParamRequest('/drifts/' + id + '/photos', requestData);
  };
  /*
    GET /users/me/driftinvitations
    Get drift invitations
    Arguments:  token             session token
                statuses          optional comma-separated list of these: "open", "accepted", "rejected" (if absent all are returned)
                dateuntil         returns results <=dateuntil descending
                itemcount         number of items to return
    Response:   { "driftinvitations": [ { "id":12, "invitationdate":datetime, "invitinguser_id":124, "inviteduser_id":125, "status":"open|accepted|rejected", "distance":1500 }, ... ] }
    Process:
    - validate the session token, return authentication error if invalid
    - return a list of all driftinvitations where the user is either invitinguser_id or inviteduser_id
    - filter and limit according to arguments
  */
  users_me_driftinvitations(request){
    let requestData = {
      token: this.token,
      statuses: request.statuses,
      dateuntil: request.dateuntil,
      itemcount: request.itemcount
    };
    return this.requestService.getParamRequest('/users/me/driftinvitations', requestData);
  };
  /*
    GET /users/me/driftsandinvitations
    Get information about agreed and running drifts and sent invitations (for drift tab)
    Arguments:  token             session token
                date-until        returns results <=date-until descending
                itemcount         number of items to return
    Response:   {"items": [
                    {"type": "drift",           "id": 1, "date": 12.34, "otheruser_id": 3, "status":"running",  
                                                "text":"Drifting with Marcus"}, 
                    {"type": "drift",           "id": 2, "date": 12.34, "otheruser_id": 4, "status":"agreed",   
                                                "text":"May your journey with Cloe begin"},
                    {"type": "driftinvitation", "id": 1, "date": 12.34, "otheruser_id": 5, "status":"open",     
                                                "text":"You sent Mohammed a drift request"},
                    {"type": "driftinvitation", "id": 2, "date": 12.34, "otheruser_id": 6, "status":"rejected", 
                                                "text":"Sorry mate Nila has denied your request"},
                    ...
                  ]}
    - validate the session token, return authentication error if invalid
    - Combine tables 'drifts' and 'driftinvitations' in a single list, ordered by date descending ('drift.created_at' or 'driftinvitations.invitationdate')
      1. All drifts with status 'running' and 'agreed' in which the current user is either driver or drifter
      2. All driftinvitations in which the current user is the invitinguser (only) with status 'open' or 'rejected'
    - Texts are generated from string resources according to response samples depending on type/status (using user's display name)
    - Combine and paginate the two queries (using date-until and itemcount)
  */
  users_me_driftsandinvitations(request){
    let requestData = {
      token: this.token,
      'date-until': request.date_until,
      itemcount: request.itemcount
    };
    return this.requestService.getParamRequest('/users/me/driftsandinvitations', requestData);
  };
  /*
    GET /users/me/openrequests
    Get information about open received drift invitations, companion requests and open reviews (for requests tab)
    Arguments:  token             session token
                date-until        returns results <=date-until descending
                itemcount         number of items to return
    Response:   {"items": [
                    {"type": "driftinvitation",   "id": 1, "date": 12.34, "otheruser_id": 3, "status":"running",  
                                                  "text":"Emma wants to drift with you"}, 
                    {"type": "companionrequest",  "id": 1, "date": 12.34, "otheruser_id": 4, "status":"agreed",   
                                                  "text":"Cloe wants to be your companion"},
                    {"type": "openreview",        "id": 1, "date": 12.34, "otheruser_id": 4, "status":"agreed",   
                                                  "text":"Felix is waiting for your review"},
                    ...
                  ]}
    - validate the session token, return authentication error if invalid
    - Combine tables 'driftinvitations', 'companionrequests' and 'drifts' in a single list, ordered by date descending ('driftinvitations.invitationdate' or 'companionrequests.invitationdate' or 'drift.created_at')
      1. All driftinvitations with status 'open' in which the current user is the inviteduser (only)
      2. All companionrequests with status 'open' in which the current user is the inviteduser (only)
      3. All drifts with status 'complete' in which the current user is either driver or drifter for which the current user has not left a review
    - Texts are generated from string resources according to response samples depending on type (using user's display name)
    - Combine and paginate the two queries (using date-until and itemcount)
  */
  users_me_openrequests(request) {
    let requestData = {
      token: this.token,
      'date-until': request['date_until'],
      itemcount: request.itemcount
    };
    return this.requestService.getParamRequest('/users/me/openrequests', requestData);
  };
  /*
    GET: /map/users
    Get People for Map by Position
    Arguments:  token           session token
                lat             center point latitude
                lon             center point longitude
                distance        radius in meters to return results for
                types           one or multiple of these: "companion", "driver", "drifter"
    Response:   { "users": [ { "id":123, "lat":lat, "lon":lon, "caption":"Nickname or first name", "pictureurl":"https://cdn.net/123", "destinationlat":lat, "destinationlon":lon, "destinationcaption":"Berlin" }, ...] }
    - validate the session token, return authentication error if invalid
    - return a list of users by geospacial search and apply the filter by 'types' argument
      - generally exclude users with invisiblemode = true
      - for type='companion' results: exclude users with companionmode = false
  */
  map_users(request) {
    let requestData = {
      token: this.token,
      lat: request.lat,
      lon: request.lon,
      types: request.types,
      distance: request.distance
    };
    return this.requestService.getParamRequest('/map/users', requestData);
  };
  /*
    GET: /map/driftpoints
    Get drift points on the map by position
    Arguments:  token           session token
                lat             center point latitude
                lon             center point longitude
                distance        radius in meters to return results for
    Response:   { "driftpoints": [ { "id":1, "lat":lat, "lon":lon, "caption":"Highway Crossing" }, ...] }
    Process:
    - validate the session token, return authentication error if invalid
    - return a list of driftpoints by geospacial search
  */
  map_driftpoints(request) {
    let requestData = {
      token: this.token,
      lat: request.lat,
      lon: request.lon,
      distance: request.distance
    };
    return this.requestService.getParamRequest('/map/driftpoints', requestData);
  };
  /*
    GET: /support/categories
    Get a list of support categories
    Arguments:  -
    Response:   { "supportcategories": [ { "id":1, "text":"Technical Support" } }
    Process:
    - return a fixed set of support categories (to be defined)
  */
  support_categories() {
    return this.requestService.getRequest('/support/categories');
  };
  /*
    POST: /support/submit
    Submit a message to support
    Arguments:  token               session token
                supportcategory_id  selected support category
                message             message body
    Response:   -
    - validate the session token, return authentication error if invalid
    - send an email message to the designated support mailbox (configured in .env)
  */
  support_submit(request) {
    let requestData = {
      token: this.token,
      supportcategory_id: request.supportcategory_id,
      message: request.message
    };
    return this.requestService.postRequest('/support/submit', requestData);
  };
  /*
    POST: /users/me/location
    Update user's current location
    Arguments:  token             session token
                lat               latitude
                long              longitude  
    Response:   -
    - validate the session token, return authentication error if invalid
    - adds an entry to the userlocations table containing the passed location
  */
  users_me_location(position){
    let requestData = {
      token: this.token,
      lat: position.latitude,
      lon: position.longitude
    };
    return this.requestService.postRequest('/users/me/location', requestData);
  };
  /*
    POST: /users/me/destination
    Update my destination
    Arguments:  token, lat, long, caption
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - updates the destination* fields in users table
  */
  users_me_destination(destination) {
    let requestData = {
      token: this.token,
      lat: destination.lat,
      lon: destination.lon,
      caption: destination.caption
    };
    return this.requestService.postRequest('/users/me/destination', requestData);
  };
  /*
    POST: /users/me/route
    Update my planned route
    Arguments:  token
                route       is a list of all route points in json format
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - save the value of argument 'route' to a file in the route files directory
      - route files directory is somewhere below 'storage/'
      - 'route' argument is already json and can be directly written to a file (overwrite, not append)
      - filename is [userid].json for example '1.json'
    - run a python script to import the route in route database (command will be like /path/to/run.py -i [userid])
  */
  users_me_route(route){
    let requestData = {
      token: this.token,
      route: route
    };
    return this.requestService.postRequest('/users/me/route', requestData);
  };
  /*
    POST: /driftinvitations/send
    Send drift invitation
    Arguments:  token             session token
                user_id           user to invite for drift
    Result:     { "driftinvitation_id":123 }
    - validate the session token, return authentication error if invalid
    - validate that invited user's mode is opposite than current user "driver" / "drifter", return error if not
    - check whether a driftinvitation with the same user pair (inviting+invited) and status 'open' is already present; return error if so
    - add entry in 'driftinvitations' table with status 'open'
  */
  driftinvitations_send(userId) {
    let requestData = {
      token: this.token,
      user_id: userId
    };
    return this.requestService.postRequest('/driftinvitations/send', requestData);
  };
  /*
    POST: /driftinvitations/{id}/accept
    Accept drift invitation
    Arguments:  token                 session token
    Response:   { "drift_id":123 }
    - validate the session token, return authentication error if invalid
    - verify that given driftinvitation exists and is yet status 'open', return error if not
    - verify that current user is the invited user in given driftinvitation, return error if not
    - mark drift invitation as 'accepted'
    - create a new entry in 'drifts' table (set driver_id, drifter_id and status to 'agreed')
    - return the id of the new drift
    - send a push notification to the inviting user
    - get all driftinvitations where invitinguser?id is the drifter of this new drift and status 'open' and delete them
  */
  driftinvitations_id_accept(id) {
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/driftinvitations/' + id + '/accept', requestData);
  };
  /*
    POST: /driftinvitations/{id}/reject
    Reject drift invitation
    Arguments:  token                 session token
                driftinvitation_id    invitation id
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given driftinvitation exists and is yet status 'open', return error if not
    - verify that current user is the invited user in given driftinvitation, return error if not
    - mark drift invitation as 'rejected'
    - send a push notification to the inviting user
  */
  driftinvitations_id_reject(id) {
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/driftinvitations/' + id + '/reject', requestData);
  };
  /*
    POST: /drifts/{id}/start
    Start drift
    Arguments:  token             session token
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given drift exists, is of status 'agreed' and current user is participant, return error if not
    - set drift's status to 'running'
    - set drift's start* values to current user's current location
    - set drift's startdate to current date/time
  */
  drifts_id_start(id) {
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/drifts/' + id + '/start', requestData);
  };
  /*
    POST: /hitchhike/start
    Start a drift in hitchhiker mode (with someone who is not a drift app user); this is for security purposes to register the plate number and provide SOS button functionality
    Arguments:  token                 session token
                platenumber           plate number string
    Response:   { "drift_id":123 }
    - validate the session token, return authentication error if invalid
    - create a new entry in 'drifts' table
      - set driver_id to null, drifter_id to current user and status to 'running'
      - set start* values to current user's current location
      - set startdate to current date/time
      - set platenumber from argument
    - return the id of the new drift
  */
  hitchhike_start(platenumber) {
    let requestData = {
      token: this.token,
      platenumber: platenumber
    };
    return this.requestService.postRequest('/hitchhike/start', requestData);
  };
  /*
    POST: /drifts/{id}/end
    End Drift
    Arguments:  token             session token
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given drift exists, is of status 'running' and current user is participant, return error if not
    - set drift's status to 'completed'
    - set drift's end* values to current user's current location
    - set drift's enddate to current date/time
  */
  drifts_id_end(id) {
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/drifts/' + id + '/end', requestData);
  };
  /*
    POST: /drifts/{id}/cancel
    Cancel drift
    Arguments:  token             session token
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given drift exists, is of status 'agreed' or 'running' and current user is participant, return error if not
    - set drift's status to 'cancelled'
    - set drift's end* values to current user's current location
    - set drift's enddate to current date/time
  */
  drifts_id_cancel(id, message) {
    let requestData = {
      token: this.token,
      message: message
    };
    return this.requestService.postRequest('/drifts/' + id + '/cancel', requestData);
  };
  /*
    POST: /drifts/{id}/uploadphoto
    Upload Picture
    Arguments:  token             session token
                picture           (base64)
                lad               (optional) latitude
                lon               (optional) longitude
                caption           (optional) text caption  
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - verify that given drift exists and current user is participant, return error if not
    - save picture to photos table
  */
  drifts_id_uploadphoto(id, photoData){
    let requestData = {
      token: this.token,
      picture: photoData.picture,
      lat: photoData.lat,
      lon: photoData.lon,
      caption: photoData.caption
    };
    return this.requestService.postRequest('/drifts/' + id + '/uploadphoto', requestData);
  };
  /*
    POST: /users/{id}/companions/request
    Send companion invitation
    Arguments:  token             session token
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - verify that current user is not yet companion of given user, return error if is already
    - verify that no entry in table 'companionrequests' with status "open" for user pair exists (in both directions), return error if it does
    - add a new entry in table 'companionrequests'
    - send a push notitification to invited user
  */
  users_id_companions_request(id) {
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/users/' + id + '/companions/request', requestData);
  };
  /*
    GET: /users/me/companions/requests
    Get companion invitations
    Arguments:  token                 session token
                statuses              optional comma-separated list of statuses: "open", "accepted", "rejected" (if absent all statuses are returned)
                invitationdate-until  (optional) returns results <=date-until descending
                itemcount             (optional) limit result count
    Response:   { "companionrequests": [ "id":1, "invitationdate":date, "invitinguser_id":123, "inviteduser_id":124, "status":"open"}, ...] }
    Process:
    - validate the session token, return authentication error if invalid
    - find all 'companionrequests' entries where current user is one of either invitinguser or inviteduser
    - filter by given status(es), date and item count
  */
  users_me_companions_requests(request){
    let requestData = {
      token: this.token,
      statuses: request.statuses,
      'invitationdate-until': request['invitationdate-until'],
      itemcount: request.itemcount
    };
    return this.requestService.getParamRequest('/users/me/companions/requests', requestData);
  };
  /*
    POST: /companionrequests/{id}/accept
    Accept companion invitation
    Arguments:  token                 session token
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given companionrequest exists and is yet status 'open', return error if not
    - verify that current user is the invited user in given companionrequest, return error if not
    - mark companion request as 'accepted'
    - create two new entries in 'companions' table (normal and inverse users pair)
    - send a push notification to the requesting user
  */
  companionrequests_id_accept(id){
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/companionrequests/' + id + '/accept', requestData);
  };
  /*
    POST: /companionrequests/{id}/reject
    Reject companion invitation
    Arguments:  token                 session token
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given companionrequest exists and is yet status 'open', return error if not
    - verify that current user is the invited user in given companionrequest, return error if not
    - mark companion request as 'rejected'
    - send a push notification to the requesting user
  */
  companionrequests_id_reject(id){
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/companionrequests/' + id + '/reject', requestData);
  };
  /*
    POST: /users/me/companions/{user_id}/remove
    Remove companion
    Arguments:  token                 session token
    Response:   -
    - validate the session token, return authentication error if invalid
    - verify that given a companionship between current user and given user id exists, return error if not
    - remove companionship (two recirds, normal and inverse)
  */
  users_me_companions_id_remove(id){
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/users/me/companions/' + id + '/remove', requestData);
  };
  /*
    POST: /users/me/companions/{user_id}/block
    Block another user
    Arguments:  token                 session token
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - insert a record to userblock if it does not yet exist
  */
  users_me_companions_id_block(id){
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/users/me/companions/' + id + '/block', requestData);
  };
  /*
    POST: /users/me/companions/{user_id}/unblock
    Unblock another user
    Arguments:  token                 session token
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - remove the entry in userblock if it does exist
  */
  users_me_companions_id_unblock(id){
    let requestData = {
      token: this.token
    };
    return this.requestService.postRequest('/users/me/companions/' + id + '/unblock', requestData);
  };
  /*
    POST: /users/me/devicetoken
    Send push device token
    Arguments:  token                 session token
                client                the app or client used 'android', 'ios' etc
                devicetoken           the device token used to send push notifications
    Response:   -
    Process:
    - validate the session token, return authentication error if invalid
    - add an entry to devicetoken table
  */
  users_me_devicetoken(tokenData){
    let requestData = {
      token: this.token,
      client: tokenData.client,
      devicetoken: tokenData.devicetoken
    };
    return this.requestService.postRequest('/users/me/devicetoken', requestData);
  };

   /*
    GET /users/me/chattoken?token=
    Returns the session token of chatserver
    Arguments:  token                 session token of drift app                    
  */  
  users_me_chattoken() {
    return this.requestService.getRequest('/users/me/chattoken?token=' + this.token);
  }

  /*
    GET /users/me/chatclienturl
    Returns the URL of the chat client interface
    Arguments:  token                 session token
                otheruser_id          optional: other user to chat with
    Returns:    {"url": "http://chat.example.com/client?token=abc"}
    Process:
    - validate the session token, return authentication error if invalid
    - login to Guapp Chat for that user (if necessary)
    - if otheruser_id is present obtain the guapp user id of given user (if possible) and add it as url argument 'otheruser_id'
    - return the chat client UI url including Guapp session token
  */
  users_me_chatclienturl(otherId){
    let requestData = {
      token: this.token,
      otheruser_id: otherId
    };
    return this.requestService.getParamRequest('/users/me/chatclienturl', requestData);
  };
  /*
    POST /communitymessages/send
    Send a community message
    Arguments:  token         session token
                message       text message
    Response:   -
    - validate the session token, return authentication error if invalid
    - save a new entry in 'communitymessages' table (with users current location)
    - send a push notification ("Community chat message") to all users whose distance is up to X meters away (X is configurable, default 3000)
  */
  communitymessages_send(message) {
    let requestData = {
      token: this.token,
      message: message
    };
    return this.requestService.postRequest('/communitymessages/send', requestData);
  };
  /*
    POST /sos
    Declare an emergency
    Arguments:  token         session token
                lat           current latitude
                lon           current longitude
    Response:   -
    - validate the session token, return authentication error if invalid
    - save a new entry in 'emergencies' table (with location)
    - send a text chat message (using Guapp library) to users.emergencycontact1 and users.emergencycontact2
    - if both emergency contacts are not set or an error happened while sending, send an email to the administrator
  */
  sos(request){
    let requestData = {
      token: this.token,
      lat: request.lat,
      lon: request.lon
    };
    return this.requestService.postRequest('/sos', requestData);
  };
 /*
   get directions and save route
 */
 getDirections(request) {
   return this.requestService.getRequest('/users/me/directions/' + request + '&token=' + this.token);
 };
 /*
    POST /driftinvitations/{id}/cancel
 */
 driftinvitations_id_cancel(id){
   let requestData = {
     token: this.token
   };
   return this.requestService.postRequest('/driftinvitations/' + id + '/cancel', requestData);
 };
 /*
   Get /content/terms.htm
 */
 getTerms(){
   return this.requestService.getRequest('/content/terms.htm');
 };
}