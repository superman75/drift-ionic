import { Injectable } from '@angular/core';

import {appSettings} from '../app/app.config';
@Injectable()
export class SettingService {
  setting: any = null;
  constructor(){
    if(localStorage.getItem('drift_settings') == null){
      this.setting = appSettings;
    }else{
      this.setting = JSON.parse(localStorage.getItem('drift_settings'));
    }
  };
  /*
    get settings data
  */
  getSettings(){
    return this.setting;
  }
  /*
    set settings data
  */
  setSettings(settings){
    this.setting = settings;
    localStorage.setItem('drift_settings', JSON.stringify(settings));
  }
  /*
    get display community chat
  */
  getDisplayCommunityChat(){
    return this.setting.displayCommunityChat;
  }
};