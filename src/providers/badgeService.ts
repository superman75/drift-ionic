import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import { AuthService } from './authService';
import { ChatService } from './chatService'; 
import { UserService } from './userService';
@Injectable()
export class BadgeService {
    private openRequests: Array<any> = [];
    private defaultPictureurl: string = 'assets/img/malethumb.png';
    constructor(
      private auth: AuthService,
      private chat: ChatService,
      private events: Events,
      private userService: UserService) {
      this.openRequests = [];
      this.events.subscribe('getChatInfo', () => {
        this.getNewOpenRequests();
      });
      /*
        listen for push notification received event
      */
      this.events.subscribe('notificationReceived', (_notification) => {
        let notification = JSON.parse(_notification);
        if(notification.type == 'driftinvitation_received' || notification.type == 'companioninvitation_received'){
          var type = notification.type == 'driftinvitation_received' ? 'driftinvitation' : 'companionrequest';
          let invitingUser = JSON.parse(notification.invitinguser);
          if(!invitingUser.pictureurl || invitingUser.pictureurl == 'http://test.drift.group/storage'){
            invitingUser.pictureurl = this.defaultPictureurl;
          }
          let request = {
            type: type,
            userid: invitingUser.id,
            pictureurl: invitingUser.pictureurl,
            companionCount: type == 'companionrequest' ? 1 : 0,
            driftCount: type == 'driftinvitation' ? 1 : 0,
            chatCount: 0,
            left: 10
          };
          if(this.openRequests.length > 0){
            var existUser = false;
            this.openRequests.forEach((_item)=> {
              if(_item.userid == invitingUser.id){
                if(type == 'companionrequest') _item.companionCount = 1;
                if(type == 'driftinvitation') _item.driftCount = 1;
                _item.type = type;
                existUser = true;
              }
            });
            if(existUser == false){
              this.openRequests.forEach((_item) =>{
                _item.left = _item.left + 50;
              });
              this.openRequests.push(request);
            }
          }else{
            this.openRequests.push(request);
          }
        }else if(notification.type == 'chatmessage'){
          let fromUser = JSON.parse(notification.fromuser);
          if(!fromUser.pictureurl || fromUser.pictureurl == 'http://test.drift.group/storage'){
            fromUser.pictureurl = this.defaultPictureurl;
          }
          let request = {
            type: 'chatmessage',
            userid: fromUser.id,
            pictureurl: fromUser.pictureurl,
            companionCount: 0,
            driftCount: 0,
            chatCount: 1,
            left: 10
          };
          if(this.openRequests.length > 0){
            var existUser = false;
            this.openRequests.forEach((_item)=> {
              if(_item.userid == fromUser.id){
                _item.chatCount++;
                _item.type = request.type;
                existUser = true;
              }
            });
            if(existUser == false){
              this.openRequests.forEach((_item) =>{
                _item.left = _item.left + 50;
              });
              this.openRequests.push(request);
            }
          }else{
            this.openRequests.push(request);
          }
        }
      });
    }
    /*
      refresh requests
    */
    public refreshRequests() {
      this.openRequests = [];
      this.getNewOpenRequests();
    };
    /*
      get open total request count
    */
    public getTotalRequestCount() {
      return this.auth.getOpenRequestCount() + this.chat.get_total_unread_count();
    }
    /*
      get open total requests
    */
    public getTotalRequests() {
      return this.openRequests;
    }
    /*
      get open total request
    */
    private getNewOpenRequests() {
      this.auth.getOpenRequests().then((res: Array<any>)=> {
      if(res.length > 0){
        res.forEach((_request)=>{
          this.userService.getUserInfo(_request.otheruser_id).then((_user: any)=> {
            if(_user !== null){
              if(!_user.pictureurl || _user.pictureurl == 'http://test.drift.group/storage'){
                _user.pictureurl = this.defaultPictureurl;
              }
              let request = {
                type: _request.type,
                userid: _request.otheruser_id,
                pictureurl: _user.pictureurl,
                companionCount: _request.type == 'companionrequest' ? 1 : 0,
                driftCount: _request.type == 'driftinvitation' ? 1 : 0,
                chatCount: 0,
                left: 10
              };
              if(this.openRequests.length > 0){
                var existUser = false;
                this.openRequests.forEach((_item)=> {
                  if(_item.userid == _request.otheruser_id){
                    if(_request.type == 'companionrequest') _item.companionCount = 1;
                    if(_request.type == 'driftinvitation') _item.driftCount = 1;
                    _item.type = _request.type;
                    existUser = true;
                  }
                });
                if(existUser == false){
                  this.openRequests.forEach((_item) =>{
                    _item.left = _item.left + 50;
                  });
                  this.openRequests.push(request);
                }
              }else{
                this.openRequests.push(request);
              }
            }
          });
        });
      }
      let unreadChat = this.chat.get_unread_count_per_member();
      if(unreadChat.length > 0){
        unreadChat.forEach((_chatmessage)=>{
          this.userService.getUserInfo(_chatmessage.nativeid).then((_user: any)=> {
            if(_user !== null){
              if(!_user.pictureurl || _user.pictureurl == 'http://test.drift.group/storage'){
                _user.pictureurl = this.defaultPictureurl;
              }
              let request = {
                type: 'chatmessage',
                userid: _chatmessage.nativeid,
                pictureurl: _user.pictureurl,
                companionCount: 0,
                driftCount: 0,
                chatCount: _chatmessage.unreadCount,
                left: 10
              };
              if(this.openRequests.length > 0){
                var existUser = false;
                this.openRequests.forEach((_item)=> {
                  if(_item.userid == _chatmessage.nativeid){
                    _item.chatCount = _chatmessage.unreadCount;
                    _item.type = request.type;
                    existUser = true;
                  }
                });
                if(existUser == false){
                  this.openRequests.forEach((_item) =>{
                    _item.left = _item.left + 50;
                  });
                  this.openRequests.push(request);
                }
              }else{
                this.openRequests.push(request);
              }
            }
          });
        });
      }
    },(err) => {
      console.log(err);
    });
  }
}
