import { Injectable } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { Events } from 'ionic-angular';
import turf from '@turf/turf/turf.min.js';

import { ApiService } from './apiService';
import { AuthService } from './authService';
import { intervalTime, mapboxDistance, findDistance} from '../app/app.config';

@Injectable()
export class LocationService {
  locationInterval: any;//interval instance that update current location
  findInterval: any;//interval instance that find guys
  geoOptions = {
    maximumAge: 15000,
    enableHighAccuracy: true,
    timeout: 5000
  };
  constructor(
    private api: ApiService,
    private auth: AuthService,
    private diagnostic: Diagnostic,
    private geolocation: Geolocation,
    private events: Events){
  };
  /*
    Get Distance between two coordinates
    @parameter
      from: LngLat([lng, lat])
      to: LngLat([lng, lat])
      unit: String 'miles', 'kilometers'
  */
  distanceBetween(_from:any, _to:any, _unit?:any) {
    var from = turf.point(_from);
    var to = turf.point(_to);
    var unit = _unit ? _unit : 'kilometers';
    return turf.distance(from, to, unit);
  };
  /*
    check device's location is available or not.
  */
  isLocationAvailable(){
    return new Promise((resolve, reject) => {
      this.diagnostic.isLocationAvailable().then((available) => {
        console.log("GPS location is " + (available ? "available" : "not available"));
        if(!available){
          this.diagnostic.isLocationAuthorized().then((authorized) => {
            console.log("Location is " + (authorized ? "authorized" : "unauthorized"));
            if(authorized){
              this.diagnostic.isGpsLocationEnabled().then((enabled) => {
                console.log("GPS location setting is " + (enabled ? "enabled" : "disabled"));
                if(!enabled){
                  this.diagnostic.switchToLocationSettings();
                }else{
                  resolve('location is available');
                }
              });
            }else{
              this.diagnostic.requestLocationAuthorization().then((status) => {
                switch(status){
                  case this.diagnostic.permissionStatus.GRANTED:
                    console.log("Permission granted");
                    this.diagnostic.isGpsLocationEnabled().then((enabled) => {
                      console.log("GPS location setting is " + (enabled ? "enabled" : "disabled"));
                      if(!enabled){
                        this.diagnostic.switchToLocationSettings();
                      }else{
                        resolve('location is available');
                      }
                    });
                  break;
                  case this.diagnostic.permissionStatus.DENIED:
                    console.log("Permission denied");
                    reject('Permission denied');
                  break;
                  case this.diagnostic.permissionStatus.DENIED_ALWAYS:
                    console.log("Permission permanently denied");
                    reject("Permission permanently denied");
                  break;
                }
              });
            }
          });
        }else{
          resolve('location is available');
        }
      }).catch((err) => {
        reject('gps location is not available');
      });
    });
  };

  /*
    get current location
  */
  getCurrentLocation(options?:any) {
    var geoOptions: any;
    if(options){
      geoOptions = options;
    }else{
      geoOptions = this.geoOptions;
    }
    return new Promise((resolve, reject) => {
      this.geolocation.getCurrentPosition(geoOptions).then((resp) =>{
        resolve(resp.coords);
      }).catch((err) =>{
        resolve(null);
      });
    });
  };
  /*
    update current location to server
  */
  updateCurrentLocation(){
    console.log('update location');
    clearInterval(this.locationInterval);
    this.locationInterval = setInterval(()=> {
      if(localStorage.getItem('drift_token') !== null){
        this.getCurrentLocation().then((position) => {
          if(position != null){
            this.api.users_me_location(position).subscribe((res) =>{
              // update location
            }, (err)=> {
              if(err.status == 403){
                this.events.publish('tokenExpired');
              }
            });
          }
        });
      }else{
        clearInterval(this.locationInterval);
      }
    }, intervalTime);
  };
  /*
    find guys that has same agreed drift that is within findDistance around current user
  */
  findGuys() {
    console.log('find guys');
    clearInterval(this.findInterval);
    this.findInterval = setInterval(()=> {
      if(localStorage.getItem('drift_token') !== null){
        this.getCurrentLocation().then((position: any) => {
          if(position != null){
            this.auth.getDrifts().then((drifts: any)=>{
              if(drifts.agreed.length > 0){
                this.getUsers(position.latitude, position.longitude, mapboxDistance).then((users: Array<any>)=> {
                  drifts.agreed.forEach((_drift)=> {
                    users.forEach((_user) => {
                      if(_drift.otheruser_id == _user.id){
                        let distance = this.distanceBetween([parseFloat(_user.lon), parseFloat(_user.lat)], [position.longitude, position.latitude]);
                        console.log('guys distance: ' + distance);
                        if(distance < findDistance){
                          this.events.publish('findGuys', _user.id, _drift.id);
                        }
                      }
                    });
                  });
                });
              }
            });
          }
        });
      }else{
        clearInterval(this.findInterval);
      }
    }, intervalTime);
  };
  /*
    get drivers list with current position
  */
  getDrivers(lat: number, lon: number, distance?: number){
    let request = {
      lat: lat,
      lon: lon,
      types: 'driver',
      distance: distance
    };
    return new Promise((resolve, reject) => {
      this.api.map_users(request).subscribe((res) =>{
        let response = res.json();
        resolve(response.users);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('getting drivers error...');
      });
    });
  };
  /*
    get users list with current position
  */
  getUsers(lat: number, lon: number, distance?: number, types?:string){
    let request = {
      lat: lat,
      lon: lon,
      types: types,
      distance: distance
    };
    return new Promise((resolve, reject) => {
      this.api.map_users(request).subscribe((res) =>{
        let response = res.json();
        resolve(response.users);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('getting users error...');
      });
    });
  };
  /*
    get driftpoints list with current position within surround
  */
  getDriftPoints(lat, lon, distance){
    let request = {
      lat: lat,
      lon: lon,
      distance: distance
    };
    return new Promise((resolve, reject) => {
      this.api.map_driftpoints(request).subscribe((res) => {
        let response = res.json();
        resolve(response.driftpoints);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('getting driftpoints error...');
      });
    });
  };
}
