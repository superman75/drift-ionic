import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { Events } from 'ionic-angular';

@Injectable()
export class AuthService {
  registerData: any = null;
  openRequestCount: number = 0;
 	constructor(
    private api: ApiService, 
    private events: Events){

    if(localStorage.getItem('drift_user') != null){
      let userInfo = JSON.parse(localStorage.getItem('drift_user'));
      this.openRequestCount = userInfo.opendriftrequestcount + userInfo.opencompanionrequestcount;
    } 
 	}
  /*
    call 'GET /user/me' and return promise
  */
  public getUser(){
    return new Promise((resolve, reject) => {
      this.api.get_users_me().subscribe((res) => {
        let response = res.json();
        localStorage.setItem('drift_user', JSON.stringify(response));
        this.openRequestCount = response.opendriftrequestcount + response.opencompanionrequestcount;
        this.events.publish('getUserInfo');
        resolve(response);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    call 'POST /login' and return promise
    @parameter
      credentials: Object {email:string, password: string}
  */
  public login(credentials) {
  	return new Promise((resolve, reject) => {
  		this.api.login(credentials).subscribe((res) => {
        let response = res.json();
        localStorage.setItem('drift_token', response.token);
        localStorage.setItem('drift_credentials', JSON.stringify(credentials));
        this.api.setToken(response.token);
        this.events.publish('loggedIn');
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
  		}, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
  			reject('error');
  		});
  	});  
  };
  /*
    call 'GET /logout' and return promise
  */
  public logout(){
    return new Promise((resolve, reject) => {
      this.api.logout().subscribe((res) => {
        localStorage.removeItem('drift_token');
        localStorage.removeItem('drift_user');
        localStorage.removeItem('drift_credentials');
        localStorage.removeItem('drift_main_map');
        localStorage.removeItem('drift_drifter_going_map');
        localStorage.removeItem('drift_drifter_hitch_map');
        localStorage.removeItem('drift_drifter_map');
        localStorage.removeItem('drift_drifter_select_map');
        localStorage.removeItem('drift_driver_going_map');
        localStorage.removeItem('drift_driver_user_destination_map');
        localStorage.removeItem('drift_driver_map');
        localStorage.removeItem('drift_driver_no_destination_map');
        localStorage.removeItem('drift_driver_select_map');
        localStorage.removeItem('drift_travels_drift_map');
        localStorage.removeItem('drift_user_drift_map');
        resolve('success');
      }, (err) => {
        reject('error');
      });
    });
  };
  /*
    register new account
    call 'POST /register' and return promise
    @paramert
      data: Object
  */
  public register(data) {
    this.registerData = {
      gender: '',
      firstname: data.firstname,
      nickname: data.nickname,
      lastname: data.lastname,
      profession: data.profession,
      email: data.email,
      mobile: data.phone,
      address: data.address,
      zip: data.zip,
      city: data.city,
      countrycode: data.country.iso,
      birthdate: data.birthdate,
      password: data.matchingPasswords.password,
      hasdriverslicense: data.hasdriverslicense
    };
    for(let item of Object.keys(data.genderGroup)){
      if(data.genderGroup[item] == true){
        this.registerData.gender = item;
      }
    }
    return new Promise((resolve, reject) => {
      this.api.register(this.registerData).subscribe((res) => {
        let credentials = {
          email: this.registerData.email,
          password: this.registerData.password
        };
        this.login(credentials).then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    check nick name
    call 'POST /login/checknickname' and return promise
    @parameter
      nickname: String
  */
  public checkNickname(nickname) {
    return new Promise((resolve, reject) => {
      this.api.login_checknickname(nickname).subscribe((res) => {
        let response = res.json();
        if(response.available == true){
          resolve('available');
        }else{
          reject('not available');
        }
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    check email
    call 'POST /login/checkemail' and return promise
    @parameter
      email: String
  */
  public checkEmail(email) {
    return new Promise((resolve, reject) => {
      this.api.login_checkemail(email).subscribe((res) => {
        let response = res.json();
        if(response.available == true){
          resolve('available');
        }else{
          reject('not available');
        }
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update profile picture
    call POST /users/me/profilepicture and return promise
    @parameter
      picture: String
  */
  public updateProfilePicture(picture){
    return new Promise((resolve, reject) => {
      this.api.users_me_profilepicture(picture).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update vehicle data
    call POST /users/me/vehicles/update and return promise
    @parameter
      vehicle: Object
  */
  public updateVehicle(platnumber: string, carmodel: string, picture?: string){
    return new Promise((resolve, reject) => {
      let vehicle = {
        platenumber: platnumber,
        carmodel: carmodel,
        picture: picture
      };
      this.api.users_me_vehicles_update(vehicle).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    call POST /users/me/vehicles/havenone and return promise
    @parameter
      mode: Number
  */
  public havenoVehicle(mode){
    return new Promise((resolve, reject) => {
      this.api.users_me_vehicles_havenone(mode).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update About text
    call POST /users/me/about and return promise
    @parameter
      text: String
  */
  public updateAbout(text){
    return new Promise((resolve, reject) => {
      this.api.users_me_about(text).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update perks
  */
  public updatePerk(text){
    return new Promise((resolve, reject) => {
      this.api.users_me_perks(text).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update user's mode
  */
  public updateMode(mode){
    return new Promise((resolve, reject) => {
      this.api.users_me_mode(mode).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update user's destination
    @parameter
      destination information which is selected by user in the selectMap
  */
  public updateDestination(lon, lat, caption){
    let request = {
      lon: lon,
      lat: lat,
      caption: caption
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_destination(request).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update emergencycontacts
  */
  public updateEmergency(emergency1, emergency2){
    let request = {
      contact1: emergency1,
      contact2: emergency2
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_emergencycontacts(request).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    update user's picture
  */
  public changeUserPicture(picture){
    return new Promise((resolve, reject) => {
      this.api.delete_users_me_userpicture(picture.id).subscribe((res) => {
        this.api.post_users_me_userpicture(picture.url).subscribe((res) => {
          resolve('success');
        }, (err) => {
          reject('upload user picture error...');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('delete user picture error...');
      });
    });
  };
  /*
    upload user's picture
  */
  public uploadUserPicture(picture){
    return new Promise((resolve, reject) => {
      this.api.post_users_me_userpicture(picture.url).subscribe((res) => {
        resolve('success');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('upload user picture error...');
      });
    });
  };
  /*
    register new account
    call 'POST /register' and return promise
    @paramert
      data: Object
  */
  public update(data) {
    var oldInfo = this.getUserInfo()
    this.registerData = {
      token: this.api.token,
      firstname: oldInfo.firstname,
      lastname: oldInfo.lastname,
      gender: oldInfo.gender,
      nickname: data.nickname,
      profession: data.profession,
      email: data.email,
      mobile: data.countryPhone.phone,
      address: data.address,
      zip: data.zip,
      city: data.city,
      countrycode: data.countryPhone.country.iso,
      birthdate: data.birthdate,
      hasdriverslicense: data.hasdriverslicense
    };
    return new Promise((resolve, reject) => {
      this.api.post_users_me(this.registerData).subscribe((res) => {
        this.getUser().then((res) => {
          resolve('success');
        }, (err) => {
          reject('error');
        });
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('error');
      });
    });
  };
  /*
    get all open requests
  */
  public getOpenRequests(itemcount?: number, date_until?: string){
    let request = {
      date_until: date_until,
      itemcount: itemcount
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_openrequests(request).subscribe((res) => {
        let response = res.json();
        if(response.items){
          let openRequests = response.items.filter((_item) => {
            return _item.type != 'openreview';
          });
          resolve(openRequests);
        }else{
          resolve([]);
        }
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get open requests error...');
      })
    });
  };
  /*
    get user's companion requests
  */
  public getCompanionRequests(itemcount?: number, date_until?: string){
    let request = {
      date_until: date_until,
      itemcount: itemcount
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_openrequests(request).subscribe((res) => {
        let response = res.json();
        if(response.items){
          let companionRequests = response.items.filter((_item) => {
            return _item.type == 'companionrequest';
          });
          resolve(companionRequests);
        }else{
          resolve([]);
        }
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get open requests error...');
      })
    });
  };
  /*
    get running and agreed drifts
  */
  public getDrifts(itemcount?:number, date_until?:string) {
    let request = {
      date_until: date_until,
      itemcount: itemcount
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_driftsandinvitations(request).subscribe((res) => {
        let response = res.json();
        if(response.items){
          let runningDrifts = response.items.filter((_item) => {
            return _item.type == 'drift' && _item.status == 'running';
          });
          let agreedDrifts = response.items.filter((_item) => {
            return _item.type == 'drift' && _item.status == 'agreed';
          });
          resolve({running: runningDrifts, agreed: agreedDrifts});
        }else{
          resolve({running: [], agreed: []});
        }
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get open requests error...');
      });
    });
  };
  /*
    get the requests of driftinvitations
  */
  public getDriftRequests(itemcount?: number, date_until?: string){
    let request = {
      date_until: date_until,
      itemcount: itemcount
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_openrequests(request).subscribe((res) => {
        let response = res.json();
        if(response.items){
          let driftRequests = response.items.filter((_item) => {
            return _item.type == 'driftinvitation';
          });
          resolve(driftRequests);
        }else{
          resolve([]);
        }
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get open requests error...');
      });
    });
  };
  /*
    get the sent driftinvitations 
  */
  public getSentDriftRequests(itemcount?:number, date_until?:string) {
    let request = {
      date_until: date_until,
      itemcount: itemcount
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_driftsandinvitations(request).subscribe((res) => {
        let response = res.json();
        if(response.items){
          let sentDriftRequests = response.items.filter((_item) => {
            return _item.type == 'driftinvitation';
          });
          resolve(sentDriftRequests);
        }else{
          resolve([]);
        }
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get open requests error...');
      });
    });
  };
  /*
    set devicetoken
  */
  public setDeviceToken(client:string, devicetoken: string){
    let request = {
      client: client,
      devicetoken: devicetoken
    };
    return new Promise((resolve, reject) => {
      this.api.users_me_devicetoken(request).subscribe((res) => {
        resolve('save devicetoken successfully...');
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('save devicetoken error...');
      });
    });
  };
  /*
    change password
  */
  public changePassword(oldPassword: string, newPassword: string) {
    let request = {
      oldpassword: oldPassword,
      newpassword: newPassword
    };
    return new Promise((resolve, reject) => {
      this.api.changepassword(request).subscribe((res) => {
        let credentials = this.getUserCredentials();
        credentials.password = newPassword;
        localStorage.setItem('drift_credentials', JSON.stringify(credentials));
        resolve('change password successfully...');
      }, (err)=> {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('change password error...');
      });
    });
  };
  /*
    return user's account data
  */
  public getUserInfo() {
    let userInfo = JSON.parse(localStorage.getItem('drift_user'));
    return userInfo;
  };
  /*
    return total open request count
  */
  public getOpenRequestCount() {
    return this.openRequestCount;
  };
  /*
    return user's credentials data
  */
  public getUserCredentials() {
    let credentials = JSON.parse(localStorage.getItem('drift_credentials'));
    return credentials;
  }

  public getChatToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      //resolve('n60R6oGSB9cAOxlloPOo');
      this.api.users_me_chattoken().subscribe((res) => {
        let chattoken = <string>res.json()['chattoken'];
        console.log(res);
        resolve(chattoken);
      }, (error) => {
        reject(error);
      });
    });
  }
}