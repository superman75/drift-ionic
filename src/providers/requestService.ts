import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import {apiEndpoint} from '../app/app.config';

@Injectable()
export class RequestService {
	constructor(
		private http: Http){
	};

	getRequest(endpoint) {
		var options: RequestOptions;
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		options = new RequestOptions({headers: headers});
		return this.http.get(apiEndpoint + endpoint, options);
	};

	deleteRequest(endpoint) {
		var options: RequestOptions;
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		options = new RequestOptions({headers: headers});
		return this.http.delete(apiEndpoint + endpoint);
	};

	postRequest(endpoint, params) {
		var options: RequestOptions;
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		options = new RequestOptions({headers: headers});
		return this.http.post(apiEndpoint + endpoint, params, options);
	};

	getParamRequest(endpoint, params) {
		var options: RequestOptions;
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		options = new RequestOptions({headers: headers});
		options.params = params;
		return this.http.get(apiEndpoint + endpoint, options);
	};

	deleteParamRequest(endpoint, params){
		var options: RequestOptions;
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		options = new RequestOptions({headers: headers});
		options.params = params;
		return this.http.delete(apiEndpoint + endpoint, options);
	}
}