import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { Country } from '../models/country.model';
import { Events } from 'ionic-angular';

@Injectable()
export class UtilService {
  countries: Array<Country> = [];
  constructor(
    private api: ApiService,
    private events: Events){
  };
  /*
    get countries list from server
  */
  getCountries() {
    return new Promise((resolve, reject) => {
      if(this.countries.length == 0){
        this.api.countries().subscribe(res => {
          let response = res.json();
          response.countries.forEach((item) => {
            //filter some country code to avoid 'vendor.js:1378 ERROR TypeError: Cannot read property 'getNationalNumber' of null' error.
            if(item != null && item.code != 'AQ' && item.code != 'BV' && item.code != 'UM' && item.code != 'TF' && item.code != 'HM' && item.code !='PN' && item.code != 'XK' && item.code != 'GS'){
              this.countries.push(new Country(item.code, item.text));
            }
          });
          resolve('get countries success...');
        }, (err) => {
          if(err.status == 403){
            this.events.publish('tokenExpired');
          }
          reject('get countries error...');
        });
      }else{
        resolve('get countries success...');
      }
    });
  }
  /*
    get support categories
  */
  public getSupportCategories() {
    return new Promise((resolve, reject) => {
      this.api.support_categories().subscribe((res) => {
        let response = res.json();
        resolve(response.supportcategories);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get support categries error...');
      });
    });
  };
  /*
    submit support categories
    @parameter
      categoryId: category id
      message: message content
  */
  public submitSupport(categoryId: string, message: string) {
    let request = {
      supportcategory_id: categoryId,
      message: message
    };
    return new Promise((resolve, reject) => {
      this.api.support_submit(request).subscribe((res) => {
        resolve('submit support success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('submit support error...');
      });
    });
  };
  /*
    send community message
    @parameter
      message: message content
  */
  public sendCommunityMessage(message: string) {
    return new Promise((resolve, reject) => {
      this.api.communitymessages_send(message).subscribe((res) => {
        resolve('send community message success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('send community message error...');
      });
    });
  };
  /*
    start hitchhike
    @parameter
      platenumber: hitchhike car platenumber
  */
  public startHitchhike(platenumber: string){
    return new Promise((resolve, reject) => {
      this.api.hitchhike_start(platenumber).subscribe((res) => {
        resolve('start hitchhiker success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('start hitchhiker error...');
      });
    });
  };
  /*
    accept companion request
    id: companion request id
  */
  public acceptCompanionRequest(id: number) {
    return new Promise((resolve, reject) => {
      this.api.companionrequests_id_accept(id).subscribe((res) => {
        resolve('accept companion request success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('accept companion request error...');
      });
    });
  };
  /*
    reject companion request
    id: companion request id
  */
  public rejectCompanionRequest(id: number) {
    return new Promise((resolve, reject) => {
      this.api.companionrequests_id_reject(id).subscribe((res) => {
        resolve('reject companion request success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('reject companion request error...');
      });
    });
  };
  /*
    accept drift invitation
    @parameter
      id: drift invitation id
  */
  public acceptDriftRequest(id: number) {
    return new Promise((resolve, reject) => {
      this.api.driftinvitations_id_accept(id).subscribe((res) => {
        resolve('accept drift invitation success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('accept drift invitation error...');
      });
    });
  };
  /*
    reject drift invitation
    @parameter
      id: drift invitation id
  */
  public rejectDriftRequest(id: number) {
    return new Promise((resolve, reject) => {
      this.api.driftinvitations_id_reject(id).subscribe((res) => {
        resolve('reject drift invitation success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('reject drift invitation error...');
      });
    });
  };
  /*
    start drift
    @parameter
      id: drift id
  */
  public startDrift(id: number){
    return new Promise((resolve, reject) => {
      this.api.drifts_id_start(id).subscribe((res) => {
        resolve('start drift success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('start drift error...');
      });
    });
  };
  /*
    end drift
    @parameter
      id: drift id
  */
  public endDrift(id: number){
    return new Promise((resolve, reject) => {
      this.api.drifts_id_end(id).subscribe((res) => {
        resolve('end drift success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('end drift error...');
      });
    });
  };
  /*
    cancel drift
    @parameter
      id: drift id
  */
  public cancelDrift(id: number, message: string){
    return new Promise((resolve, reject) => {
      this.api.drifts_id_cancel(id, message).subscribe((res) => {
        resolve('cancel drift success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('cancel drift error...');
      });
    });
  };
  /*
    send review
    @parameter
      id: drift id
      updown: 'up', 'down'
      text: review text
  */
  public sendReview(id: number, updown: string, text: string){
    let request = {
      updown: updown,
      text: text
    };
    return new Promise((resolve, reject) => {
      this.api.drifts_id_review(id, request).subscribe((res) => {
        resolve('send review success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('send review error...');
      });
    });
  };
  /*
    get drift photos
    @parameter
      id: drift id,
      itemcount: itemcount per page
      pagenumber: page number to get photo
  */
  public getDriftPhotos(id: number, itemcount?: number, pagenumber?: number){
    let request = {
      itemcount: itemcount,
      pagenumber: pagenumber
    };
    return new Promise((resolve, reject) => {
      this.api.drifts_id_photos(id, request).subscribe((res: any) => {
        let response = res.json();
        resolve(response.photos);
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('get drift photos error...');
      });
    });
  };
  /*
    upload drift photo
    @parameter
      id: drift id
      picture: base64string
      lat: latitude of position uploading photo
      lon: longitude of position uploading photo
      caption: photo's caption
  */
  public uploadDriftPhoto(id: number, picture: string, lat?: number, lon?: number, caption?: string){
    let request = {
      picture: picture,
      lat: lat,
      lon: lon,
      caption: caption
    };
    return new Promise((resolve, reject) => {
      this.api.drifts_id_uploadphoto(id, request).subscribe((res: any) => {
        resolve('upload drift photo success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('upload drift photo error...');
      });
    });
  };
  /*
    send drift request
    @parameter
      userid: user id
  */
  public sendDriftRequest(id: number){
    return new Promise((resolve, reject) => {
      this.api.driftinvitations_send(id).subscribe((res) => {
        resolve('send driftinvitation success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('send driftinvitation error...');
      });
    });
  };
  /*
    cancel drift request
  */
  public cancelDriftRequest(id: number){
    return new Promise((resolve, reject) => {
      this.api.driftinvitations_id_cancel(id).subscribe((res) => {
        resolve('cancel driftinvitation success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('cancel driftinvitation error...');
      });
    });
  };
  /*
    declare emergency
  */
  public declareEmergency(lat, lon) {
    let request = {
      lat: lat,
      lon: lon
    };
    return new Promise((resolve, reject) => {
      this.api.sos(request).subscribe((res) => {
        resolve('declare emergency success...');
      }, (err) => {
        if(err.status == 403){
          this.events.publish('tokenExpired');
        }
        reject('declare emergency error...');
      });
    });
  };
};