import { Injectable } from "@angular/core";
import { ChatClient, UnreadCountInfo, UnreadCountPerMember } from 'guapp-chat';
import { AuthService } from "./authService";
import { Observable } from "rxjs/Observable";
import { Events } from 'ionic-angular';

@Injectable()
export class ChatService {
    public listen_first: boolean;
    private total_unread_count: number;
    private unread_count_per_member: UnreadCountPerMember[];
    constructor(
        private auth: AuthService,
        private events: Events) {
        this.total_unread_count = 0;
        this.unread_count_per_member = [];
        this.listen_first = false;
    }

    private listen_to_unread_count() {
        ChatClient.getNewMessageCount_$().subscribe((unread_count_info: UnreadCountInfo) => {
            this.total_unread_count = unread_count_info.totalUnreadCount;
            this.unread_count_per_member = unread_count_info.unreadCountsPerMember.filter(u => u.unreadCount > 0);
            if (this.listen_first == false) {
                this.listen_first = true;
                this.events.publish('getChatInfo');//event publish at once
            }
        });
    }

    public guapp_chat_unread_count_$(): Observable<UnreadCountInfo> {
        return ChatClient.getNewMessageCount_$();
    }

    public init_chat() {
        console.log('--- chat init ---');
        this.auth.getChatToken()
            .then((chatToken: string) => {
                let chatConfig = {
                    iFrame: 'guapp-chat',
                    sessionToken: chatToken,
                    communityName: 'drift',
                    showfriendstab: false
                };
                return ChatClient.initialize(chatConfig);
            })
            .then(() => {
                this.listen_first = false;
                this.listen_to_unread_count();
            })
            .catch(error => {
                console.log(error);
            });
    }

    public get_total_unread_count(): number {
        return this.total_unread_count;
    }

    public get_unread_count_per_member(): UnreadCountPerMember[] {
        return this.unread_count_per_member;
    }

    public start_chat(): Promise<any> {
        return ChatClient.startChat();
    }

    public enter_room(friend_native_id) {
        ChatClient.enterRoom(friend_native_id);
    }

    public end_chat() {
        ChatClient.endChat();
    }

    public leave_chat_room() {
        ChatClient.leaveChatRoom();
    }
}