import { Input } from '@angular/core';
import { Validator, AbstractControl} from '@angular/forms';

export class CountryValidator implements Validator {
  @Input() validateCountry: string;

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    // let country = c.value;
    let phone = c.root.get(this.validateCountry);
    phone.updateValueAndValidity();
    return null;
  }
}
