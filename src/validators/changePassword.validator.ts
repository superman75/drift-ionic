import { FormControl } from '@angular/forms';

export class OldPasswordValidator {

  static validPassword(fc: FormControl){
    let credentials = JSON.parse(localStorage.getItem('drift_credentials'));
    if(fc.value !== credentials.password){
      return {
        validPassword: true
      };
    } else {
      return null;
    }
  }
}