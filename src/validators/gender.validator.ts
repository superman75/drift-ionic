import { FormControl, FormGroup } from '@angular/forms';

export class GenderValidator {

  // Inspired on: http://plnkr.co/edit/Zcbg2T3tOxYmhxs7vaAm?p=preview
  static noSelect(formGroup: FormGroup) {
    let valid = true;

    for (let key in formGroup.controls) {
      if (formGroup.controls.hasOwnProperty(key)) {
        let control: FormControl = <FormControl>formGroup.controls[key];
        if(control.value == true){
          valid = true;
          break;
        }
      }
      valid = false;
    }

    if (valid) {
      return null;
    }

    return {
      noSelect: true
    };
  }
}