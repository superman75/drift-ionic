/* api endpoint url */
export const apiEndpoint = 'http://test.drift.group';
/* public server url to get media files */
export const publicEndpoint = 'http://test.drift.group';
/* app name */
export const appName = 'drift';
/* app id */
export const appId = 'group.drift.app';
/* google api key */
export const googleApiKey = 'AIzaSyCYHbwHEXc7hF_K1ANTNUUg2dbnkdZj6Zs';
/* interval time that update users location to server */
export const intervalTime = 30000;
/* mapbox-gl api key*/
export const mapboxApiKey = 'pk.eyJ1Ijoic2FueWE1MDIiLCJhIjoiY2o1OWgzZXJsMXN2ejJxb2FndjY3MjBsaSJ9.s02bGyxk8W7yHvdL_EXR2w';
/* mapbox-gl map style*/
export const mapboxStyle = 'mapbox://styles/mapbox/bright-v9?optimize=true';
/* mapbox-gl map zoom*/
export const mapboxZoom = 10;
/* mapbox-gl surround this is used in map/users, map/driftpoints to get users who is within this value in the map*/
export const mapboxDistance = 10000;//10km
/* update interva time to move up community messages*/
export const communityMesssageInterval = 60000;//1 minute
/* find distance*/
export const findDistance = 0.1;//100m display find user question screen if distance is less than this value
/* mapbox-gl default center*/
export const mapboxCenter = {
  latitude: 52.520008,
  longitude: 13.404954
};
/* mapbox-gl route color */
export const routeColor = '#a4cbd1';
export const otherRouteColor = '#d1dcd8';
/* available languages array */
export const availableLanguages = [{
	code: 'en',
	name: 'English'
},
{
	code: 'de',
	name: 'German'
}];
/* app default language */
export const defaultLanguage = 'en';
/* device current setting's language */
export const systemOptions = {
	systemLanguage: defaultLanguage
};

/*app default settings*/
export const appSettings = {
  displayCommunityChat: true
};
/*
  app customize events
*/
export const eventNames = {
  getUserInfo: 'triggered when user profile is updated. published in authService.ts->getUser()',
  loggedIn: 'triggered when user login app. published in authService.ts->login()',
  userClicked: 'triggered when user click other user in map. published in mapboxService.ts -> addUserMarkers()',
  reachedDestination: 'triggered when user reach destination published in mapboxService.ts -> checkManeuver()',
  findGuys: 'triggered when agreed drifter is within arroud findDistance. published in locationService.ts -> findGuys()',
  notificationReceived: 'triggered when device receive push notification. published in app.component.ts -> platform.ready()',
  tokenExpired: 'triggered when user token is expired in server'
};