import { Component, ViewChild, NgZone } from '@angular/core';
import { Platform, Alert, AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Events } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { TranslateService } from '@ngx-translate/core';
import { NavController } from 'ionic-angular';

import { defaultLanguage, availableLanguages, systemOptions } from './app.config';
import { AuthService } from '../providers/authService';
import { UserService } from '../providers/userService';
import { LocationService } from '../providers/locationService';
import { ChatService } from '../providers/chatService';
@Component({
  templateUrl: 'app.html'
})
export class DriftApp{
  @ViewChild('appNav') nav: NavController;
  rootPage:any;
  alert: Alert;
  alertCtrl: AlertController;
  platform: Platform;
  userImg: string = 'assets/img/malethumb.png';
  userInfo: any;
  auth: AuthService;
  location: LocationService;
  fcm: FCM;
  localNotification: LocalNotifications;
  notificationId: number = 0;
  swipeEnabled: boolean = false;
  opendriftrequestcount: number = 0;
  opencompanionrequestcount: number = 0;
  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    alertCtrl: AlertController,
    auth: AuthService,
    location: LocationService,
    userService: UserService,
    private chatService: ChatService,
    events: Events,
    screenOrientation: ScreenOrientation,
    fcm: FCM,
    localNotification: LocalNotifications,
    translate: TranslateService,
    zone: NgZone) {
      this.auth = auth;
      this.location =location;
      this.fcm = fcm;
      this.alertCtrl = alertCtrl;
      this.platform = platform;
      this.localNotification = localNotification;
      if(localStorage.getItem('drift_token') !== null){        
        this.swipeEnabled = true;
        this.userInfo = auth.getUserInfo();
        if(this.userInfo.pictureurl)
          this.userImg = this.userInfo.pictureurl;
        this.opencompanionrequestcount = this.userInfo.opencompanionrequestcount;
        this.opendriftrequestcount = this.userInfo.opendriftrequestcount;
        this.auth.getDrifts().then((res: any) => {//check whether user is drifting now or not
          if(res.running.length > 0){
            if(this.userInfo.mode == 'Driver'){
              this.rootPage = 'DriverGoingMapPage';
            }else{
              this.rootPage = 'DrifterGoingMapPage';
            }
          }else{
            this.rootPage = 'MainPage';
          }
        });
        this.location.updateCurrentLocation();
        this.location.findGuys();
        this.chatService.init_chat();
      }else{
        this.rootPage = 'LoginPage';
      }
      this.localNotification.on('click', (notification, state) => {
        let notificationData = JSON.parse(notification.data);
        if(notificationData.type == 'driftinvitation_received'){
          let invitingUser = JSON.parse(notificationData.invitinguser);
          let request = {
            id: notificationData.driftinvitation_id
          };
          userService.getUserInfo(invitingUser.id).then((res) => {
            this.nav.push('UserDriftPage', {from:'travels-requests', user: res, request: request}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'})
          });
        }
      });
      /*
        Called when token is expired
      */
      events.subscribe('tokenExpired', () => {
        this.goLogin();
      });
      /*
        Called when get user's info
      */
      events.subscribe('getUserInfo', () => {
        zone.run(() => {
          this.userInfo = auth.getUserInfo();
          if(this.userInfo.pictureurl)
            this.userImg = this.userInfo.pictureurl;
          this.opencompanionrequestcount = this.userInfo.opencompanionrequestcount;
          this.opendriftrequestcount = this.userInfo.opendriftrequestcount;
        });
      });
      /*
        Called when user click other user in the map
      */
      events.subscribe('userClicked', (userId) => {
        this.nav.push('UserProfilePage', {userid: userId}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      });
      /*
        Called when user reach destination
      */
      events.subscribe('reachedDestination', () => {
        this.nav.push('DriverReachedPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
      });
      /*
        Called when agreed drifter is within findDistance
      */
      events.subscribe('findGuys', (userId, driftId) => {
        if(this.nav.getActive().name != 'FindUserPage'){
          if(this.nav.getActive().name == 'DriverUserDestinationPage'){
            this.nav.push('FindUserPage', {userid: userId, driftid: driftId, from: 'destination'}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          }else{
            this.nav.push('FindUserPage', {userid: userId, driftid: driftId}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          }
        }
      });
      /*
        Called when user log in
      */
      events.subscribe('loggedIn', () => {
        zone.run(() => {
          this.swipeEnabled = true;
          if(platform.is('ios')){
            this.auth.setDeviceToken('ios', localStorage.getItem('drift_devicetoken')).then((res) =>{
              // handle success
            },(err) =>{
              // handle error
            });
          }
          if(platform.is('android')){
            this.auth.setDeviceToken('android', localStorage.getItem('drift_devicetoken')).then((res)=> {
              // handle success
            }, (err) => {
              // handle error
            });
          }
        }); 
        this.chatService.init_chat();
      });
      /*
        Called when device receive notification
      */
      events.subscribe('notificationReceived', (_notification) => {
        let notification = JSON.parse(_notification);
        auth.getUser().then((res) => {});
        switch (notification.type) {
          /*
              - Plain text
              A plain text message to display
              {"type":"text", "message":"Hi!"}
          */
          case 'text':
            console.log('plain text received...');
          break;
          /*
            - Chat message received
            You received a chat message
            {"type":"chatmessage", "message":"Hi!", "fromuser": {"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12"}
          */
          case 'chatmessage':
            console.log('chat message received...');
            var fromUser = JSON.parse(notification.fromuser);
            this.showLocalNotification(fromUser.displayname, notification.message, _notification);
          break;
          /*
            - Drift invitation received
            Another user invited you to drift
            {"type":"driftinvitation_received", "message":"Hi!", "driftinvitation_id":12, "invitationdate":datetime, "invitinguser":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12" } }
          */
          case 'driftinvitation_received':
            console.log('driftinvitation received...');
            var invitingUser = JSON.parse(notification.invitinguser);
            this.showLocalNotification(invitingUser.displayname, notification.message, _notification);
          break;
          /*
            - Drift invitation accepted
            Another user invited your drift invitation
            {"type":"driftinvitation_accepted", "message":"Hi!", "driftinvitation_id":12, "invitationdate":datetime, "inviteduser":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12" } }
          */
          case 'driftinvitation_accepted':
            console.log('driftinvitation accepted...');
            var invitedUser = JSON.parse(notification.inviteduser);
            this.nav.push('DriftMatchPage', {user: invitedUser}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
          break;
          /*
            - Drift invitation rejected
            Another user rejected your drift invitation
            {"type":"driftinvitation_rejected", "message":"Hi!", "driftinvitation_id":12, "invitationdate":datetime, "inviteduser":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12" } }
          */
          case 'driftinvitation_rejected':
            console.log('driftinvitation rejected...');
            var invitedUser = JSON.parse(notification.inviteduser);
            this.showLocalNotification(invitedUser.displayname, notification.message, _notification);
          break;
          /*
            - Companion invitation received
            Another user invited you to be a companion
            {"type":"companioninvitation_received", "message":"Hi!", "companionrequest_id":12, "invitationdate":datetime, "invitinguser":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12" } }
          */
          case 'companioninvitation_received':
            console.log('companion invitation received...');
            var invitingUser = JSON.parse(notification.invitinguser);
            this.showLocalNotification(invitingUser.displayname, notification.message, _notification);
          break;
          /*
            - Companion invitation accepted
            Another user accepted your companion invitation
            {"type":"companioninvitation_accepted", "message":"Hi!", "companionrequest_id":12, "invitationdate":datetime, "inviteduser":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12" } }
          */
          case 'companioninvitation_accepted':
            console.log('companion invitation accepted...');
            var invitedUser = JSON.parse(notification.inviteduser);
            this.showLocalNotification(invitedUser.displayname, notification.message, _notification);
          break;
          /*
            - Companion invitation rejected
            Another user rejected your companion invitation
            {"type":"companioninvitation_rejected", "message":"Hi!", "companionrequest_id":12, "invitationdate":datetime, "inviteduser":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12" } }
          */
          case 'companioninvitation_rejected':
            console.log('companion invitation rejected...');
            var invitedUser = JSON.parse(notification.inviteduser);
            this.showLocalNotification(invitedUser.displayname, notification.message, _notification);
          break;
          /*
            - Community chat message
            A community message is sent nearby (can be displayed on map screen)
            {"title":"New message from test2","body":"Monday","type":"communitymessage","message":"Monday","fromuser":{"id":2,"email":"test2@guapp.com","displayname":"test2"}}
          */
          case 'communitymessage':
            console.log('communitymessage received...');
            //var fromUser = JSON.parse(notification.fromuser);
            //this.showLocalNotification(fromUser.displayname, notification.message, _notification);
          break;
          /*
            -Drift cancelled
            Another user cancelled drift
            {"type":"driftcancelled", "message": "[nickname] cancelled the drift with the message: [cancellationmessage]", "drift_id":12, "driftpartner":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12"}
          */
          case 'driftcancelled':
            console.log('drift cancelled...');
            var driftPartner = JSON.parse(notification.driftpartner);
            this.showLocalNotification(driftPartner.displayname, notification.message, _notification);
          break;
          /*
            -Drift started
            {"type":"driftstarted", "message": "[nickname] cancelled the drift with the message: [cancellationmessage]", "drift_id":12, "driftpartner":{"id":12, "displayname":"Jack", "pictureurl":"http://test.com/users/12"}
          */
          case 'driftstarted':
            console.log('drift started...');
            this.showLocalNotification('Hi', 'Drift has been started!', _notification);
          break;
          /*
            Cancel Drift invitation
            Another user cancel his drift invitation
            
          */
          case 'driftinvitation_cancelled':
            console.log('drift invitation cancelled...');
            this.showLocalNotification('drift invitation is cancelled', notification.message, _notification);
          break;
          /*
            A notification of type 'driftinvitation_expired' is now sent to both users when a drift invitation is deleted by the server 
            {"type":"driftinvitation_expired", "message":"", "driftinvitation_id":12 } }
          */  
          case 'driftinvitation_expired':
            console.log('drift invitation expired...');
            this.showLocalNotification('drift invitation is expired', notification.message, _notification);
          break;

        }
      });
    
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        statusBar.styleDefault();
        splashScreen.hide();
        screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);

        //this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang(defaultLanguage);
        var navigatorLang = navigator.language || defaultLanguage;
        var userLang = this.getSuitableLanguage(navigatorLang);
        translate.use(userLang);
        systemOptions.systemLanguage = userLang;
        console.log('current language is: ' + systemOptions.systemLanguage);

        //get device token
        this.getDeviceToken();
        //save token when refresh
        fcm.onTokenRefresh().subscribe(token => {
          localStorage.setItem('drift_devicetoken', token);
        });
        //called when receive notification
        fcm.onNotification().subscribe(data => {
          if (data.wasTapped) {
            //Notification was received on device tray and tapped by the user.
            console.log("Tapped: " + JSON.stringify(data));
          } else {
            //Notification was received in foreground. Maybe the user needs to be notified.
            console.log("Not tapped: " + JSON.stringify(data));
            events.publish('notificationReceived', JSON.stringify(data));
          }
        });
        /*
          register back button action
        */
        platform.registerBackButtonAction(() => {
          if(this.nav.getActive().name == 'DriverGoingMapPage' || this.nav.getActive().name == 'DriverUserDestinationPage' || this.nav.getActive().name == 'DrifterGoingMapPage'){
            this.nav.getActive().instance.showConfirmModal();
          }
          else if(this.nav.getActive().name == 'ChatsPage'){
            this.nav.getActive().instance.goByNativeBackButton();
          }
          else{
            if(this.nav.canGoBack()){
              this.nav.pop();
            }else{
              if(this.alert){
                this.alert.dismiss();
                this.alert = null;
              }else{
                this.showAlert();
              }
            }
          }
        });
      });
    };
  /*
    show alert
  */
  showAlert(){
    this.alert = this.alertCtrl.create({
      title: `<img src="assets/img/logo.png"/>`,
      subTitle: 'Do you want to exit the app?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.platform.exitApp();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            this.alert = null;
          }
        }
      ]
    });
    this.alert.present();
  };
  /*
    get language code
  */
  getSuitableLanguage(language) {
    language = language.substr(0,2).toLowerCase();
    return availableLanguages.some(x => x.code == language) ? language : defaultLanguage;
  };
  /*
    get Devicetoken
  */
  getDeviceToken() {
    this.fcm.getToken().then((token) => {
       if(token == null){
         setTimeout(this.getDeviceToken, 1000);
       }else{
         localStorage.setItem('drift_devicetoken', token);
       }
    }).catch((err) => {
      console.log('get token error...');
    });
  };
  /*
    unread chat count
  */
  getTotal_Unread_Chat_Count() {
    return this.chatService.get_total_unread_count();
  }
  /*
    show local notification
  */
  showLocalNotification(title: string, text: string, data: any) {
    let now = new Date();
    this.notificationId++;
    this.localNotification.schedule({
      id: this.notificationId,
      title: title,
      text: text,
      data: data,
      at: now,
      icon: 'res://notification.png'
    });
  };
  /*
    Called when user click 'profile' menu item
  */
  goProfile() {
    if(this.nav.getActive().name != 'ProfileMainPage')
      this.nav.push('ProfileMainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'settings' menu item
  */
  goSettings() {
    if(this.nav.getActive().name != 'SettingsMainPage')
      this.nav.push('SettingsMainPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'chats' menu item
  */
  goChats() {
    if(this.nav.getActive().name != 'ChatsPage')
      this.nav.push('ChatsPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'travels' menu item
  */
  goTravels() {
    if(this.nav.getActive().name != 'TravelsDriftPage')
      this.nav.push('TravelsDriftPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'friends' menu item
  */
  goCompanions() {
    if(this.nav.getActive().name != '')
      this.nav.push('ProfileCompanionsPage', {userid: this.userInfo.id}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'support' menu item
  */
  goSupport() {
    if(this.nav.getActive().name != 'SupportPage')
      this.nav.push('SupportPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'forward'});
  };
  /*
    Called when user click 'logout' menu item
  */
  goLogout() {
    this.swipeEnabled = false;
    this.userImg = 'assets/img/malethumb.png';
    this.nav.setRoot('LoginPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
    this.auth.logout().then((res) => {
      console.log('logout success...');
    }, (err) => {
      console.log('logout error...');
    });
  };
  /*
    Go login page when token is expired
  */
  goLogin() {
    localStorage.removeItem('drift_token');
    localStorage.removeItem('drift_user');
    localStorage.removeItem('drift_credentials');
    localStorage.removeItem('drift_main_map');
    localStorage.removeItem('drift_drifter_going_map');
    localStorage.removeItem('drift_drifter_hitch_map');
    localStorage.removeItem('drift_drifter_map');
    localStorage.removeItem('drift_drifter_select_map');
    localStorage.removeItem('drift_driver_going_map');
    localStorage.removeItem('drift_driver_user_destination_map');
    localStorage.removeItem('drift_driver_map');
    localStorage.removeItem('drift_driver_no_destination_map');
    localStorage.removeItem('drift_driver_select_map');
    localStorage.removeItem('drift_travels_drift_map');
    localStorage.removeItem('drift_user_drift_map');
    this.swipeEnabled = false;
    this.userImg = 'assets/img/malethumb.png';
    this.nav.setRoot('LoginPage', {}, {animate: true, animation: 'ios-transition', easing: 'ease-in-out', direction: 'back'});
  };
}

